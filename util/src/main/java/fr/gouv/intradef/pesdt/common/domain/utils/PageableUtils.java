package fr.gouv.intradef.pesdt.common.domain.utils;

import fr.gouv.intradef.pesdt.common.domain.exceptions.BadSizePageableException;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;

public final class PageableUtils
{
	private PageableUtils()
	{
		throw new UnsupportedOperationException("This is a utility class and cannot be instantiated");
	}

	public static Pageable translatePageable(Integer page, Integer size, String sorts)
	{
		page = translatePage(page);
		size = translateSize(size);
		Sort sortObject = translateSort(sorts);
		return PageRequest.of(page, size, sortObject);
	}

	/**
	 * TODO: tester si la refacto de la methode n'a pas altéré son fonctionnement
	 *
	 * @param sorts
	 * @return l'entrée trié dans un objet Sort
	 */
	public static Sort translateSort(String sorts)
	{
		if (StringUtils.isBlank(sorts))
		{
			return Sort.unsorted();
		}
		else
		{
			List<Sort.Order> orders_array = new ArrayList();
			String[] result_sort_comma = sorts.split(",");
			int result_length = result_sort_comma.length;

			for (int i = 0; i < result_length; ++i)
			{
				String element = result_sort_comma[i];
				Sort.Order order;
				String element_without_first_element;
				element_without_first_element = StringUtils.substring(element, 1);
				if (StringUtils.isBlank(element_without_first_element))
				{
					return Sort.unsorted();
				}
				if (StringUtils.startsWith(element, "-"))
				{
					order = Order.desc(element_without_first_element);
				}
				else if (StringUtils.startsWith(element, "+"))
				{
					order = Order.asc(element_without_first_element);
				}
				else
				{
					order = Order.asc(element);
				}
				orders_array.add(order);
			}

			return Sort.by(orders_array);
		}
	}

	/**
	 * @param page
	 * @return page - 1 si page n'est pas null, 0 sinon
	 */
	private static Integer translatePage(Integer page)
	{
		return (Integer) Optional.ofNullable(page).orElse(1) - 1;
	}

	/**
	 * @param size
	 * @return Valeur de size si size n'est pas null, 20 sinon
	 */
	private static Integer translateSize(Integer size)
	{
		// La valeur par défaut d'une page est 20 elements
		Integer pageSize = (Integer) Optional.ofNullable(size).orElse(20);
		if (pageSize == 0)
		{
			throw new BadSizePageableException();
		}
		return pageSize;
	}
}
