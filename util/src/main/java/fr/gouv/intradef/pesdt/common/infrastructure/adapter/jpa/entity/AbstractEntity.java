package fr.gouv.intradef.pesdt.common.infrastructure.adapter.jpa.entity;


import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import fr.gouv.intradef.pesdt.common.api.utils.CustomLocalDateTimeDeserializer;
import fr.gouv.intradef.pesdt.common.api.utils.CustomLocalDateTimeSerializer;
import fr.gouv.intradef.pesdt.common.infrastructure.adapter.secondary.KafkaEntityListener;
import fr.gouv.intradef.pesdt.common.infrastructure.adapter.secondary.PesdtEntityListener;
import org.springframework.data.annotation.*;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;
import java.time.LocalDateTime;

@MappedSuperclass
@EntityListeners({AuditingEntityListener.class, PesdtEntityListener.class, KafkaEntityListener.class})
public abstract class AbstractEntity<K> implements Serializable
{
	@CreatedBy
	@Column(
			name = "createur",
			length = 120,
			updatable = false
	)
	private String createur;
	@CreatedDate
	@Column(
			name = "creation",
			updatable = false
	)
	@JsonDeserialize(
			using = CustomLocalDateTimeDeserializer.class
	)
	@JsonSerialize(
			using = CustomLocalDateTimeSerializer.class
	)
	private LocalDateTime creation;
	@LastModifiedBy
	@Column(
			name = "modificateur",
			length = 120
	)
	private String modificateur;
	@LastModifiedDate
	@Column(
			name = "modification"
	)
	@JsonDeserialize(
			using = CustomLocalDateTimeDeserializer.class
	)
	@JsonSerialize(
			using = CustomLocalDateTimeSerializer.class
	)
	private LocalDateTime modification;
	@Version
	@Column(name = "version")
	private Long version;

	/**
	 * Constructeur
	 *
	 * @param createur
	 * @param creation
	 * @param modificateur
	 * @param modification
	 * @param version
	 */
	public AbstractEntity(String createur, LocalDateTime creation, String modificateur, LocalDateTime modification,
						  Long version)
	{
		this.createur = createur;
		this.creation = creation;
		this.modificateur = modificateur;
		this.modification = modification;
		this.version = version;
	}

	public AbstractEntity()
	{
	}

	public abstract K getId();

	public abstract void setId(K var1);

	/**
	 * Getter
	 *
	 * @return le créateur
	 */
	public String getCreateur()
	{
		return this.createur;
	}

	public void setCreateur(String createur)
	{
		this.createur = createur;
	}

	public LocalDateTime getCreation()
	{
		return this.creation;
	}

	public void setCreation(LocalDateTime creation)
	{
		this.creation = creation;
	}

	public String getModificateur()
	{
		return this.modificateur;
	}

	public void setModificateur(String modificateur)
	{
		this.modificateur = modificateur;
	}

	public LocalDateTime getModification()
	{
		return this.modification;
	}

	public void setModification(LocalDateTime modification)
	{
		this.modification = modification;
	}

	public Long getVersion()
	{
		return this.version;
	}

	public void setVersion(Long version)
	{
		this.version = version;
	}

	@Override
	public boolean equals(Object o)
	{
		boolean result = true;
		if (o != this)
		{
			if (!(o instanceof AbstractEntity))
			{
				result = false;
			}
			else
			{
				AbstractEntity<?> other = (AbstractEntity) o;
				result = this.isEqualForAttribute(this.getCreateur(), other.getCreateur())
						&& this.isEqualForAttribute(this.getCreation(), other.getCreation())
						&& this.isEqualForAttribute(this.getModificateur(), other.getModificateur())
						&& this.isEqualForAttribute(this.getModification(), other.getModification())
						&& this.isEqualForAttribute(this.getVersion(), other.getVersion());
			}
		}
		return result;
	}

	@Override
	public int hashCode()
	{
		int result = 1;
		Object createur = this.getCreateur();
		result = result * 59 + (createur == null ? 43 : createur.hashCode());
		Object creation = this.getCreation();
		result = result * 59 + (creation == null ? 43 : creation.hashCode());
		Object modificateur = this.getModificateur();
		result = result * 59 + (modificateur == null ? 43 : modificateur.hashCode());
		Object modification = this.getModification();
		result = result * 59 + (modification == null ? 43 : modification.hashCode());
		Object version = this.getVersion();
		result = result * 59 + (version == null ? 43 : version.hashCode());
		return result;
	}

	/**
	 * Comparaison des attributs
	 *
	 * @param attribute1 attribut de référence
	 * @param attribute2 attribut à comparer
	 * @return true si les attributs sont identiques
	 */
	private boolean isEqualForAttribute(Object attribute1, Object attribute2)
	{
		return (attribute1 != null && !attribute1.equals(attribute2))
				|| (attribute1 == null && attribute2 != null);
	}

}
