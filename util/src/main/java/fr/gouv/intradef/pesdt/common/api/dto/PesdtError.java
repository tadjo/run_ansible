package fr.gouv.intradef.pesdt.common.api.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import io.swagger.v3.oas.annotations.media.Schema;

@JsonInclude(Include.NON_NULL)
@Schema(
		description = "Erreur applicative"
)
public class PesdtError
{
	private String message;
	private String code;
	private String detail;

	public PesdtError()
	{
	}

	public String getMessage()
	{
		return this.message;
	}

	public void setMessage(String message)
	{
		this.message = message;
	}

	public String getCode()
	{
		return this.code;
	}

	public void setCode(String code)
	{
		this.code = code;
	}

	public String getDetail()
	{
		return this.detail;
	}

	public void setDetail(String detail)
	{
		this.detail = detail;
	}

	public String toString()
	{
		return "PesdtError(message=" + this.getMessage() + ", code=" + this.getCode() + ", detail=" + this.getDetail() + ")";
	}
}

