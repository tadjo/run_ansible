package fr.gouv.intradef.pesdt.common.api;

import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.RSAKeyProvider;
import fr.gouv.intradef.pesdt.common.infrastructure.configuration.security.KeycloakRSAPublicKeyProvider;
import fr.gouv.intradef.pesdt.common.infrastructure.configuration.security.NoneRSAPublicKeyProvider;
import fr.gouv.intradef.pesdt.common.infrastructure.configuration.swagger.ApiInfos;
import fr.gouv.intradef.pesdt.common.infrastructure.configuration.swagger.CustomOpenApiResource;
import fr.gouv.intradef.pesdt.common.infrastructure.configuration.swagger.GroupCustomiser;
import fr.gouv.intradef.pesdt.common.infrastructure.configuration.swagger.ServerInfos;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeIn;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeType;
import io.swagger.v3.oas.annotations.extensions.Extension;
import io.swagger.v3.oas.annotations.extensions.ExtensionProperty;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.security.SecurityScheme;
import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.Paths;
import lombok.NonNull;
import org.springdoc.core.GroupedOpenApi;
import org.springdoc.core.SwaggerUiConfigParameters;
import org.springdoc.core.customizers.OpenApiCustomiser;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.web.ErrorProperties;
import org.springframework.boot.autoconfigure.web.servlet.error.BasicErrorController;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.info.BuildProperties;
import org.springframework.boot.web.error.ErrorAttributeOptions;
import org.springframework.boot.web.servlet.error.DefaultErrorAttributes;
import org.springframework.boot.web.servlet.error.ErrorAttributes;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import org.springframework.core.env.PropertySource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.ResourceUtils;
import org.springframework.web.context.request.WebRequest;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.function.UnaryOperator;

/**
 * Interface des application pesdt
 */
@OpenAPIDefinition(
		info = @Info(
				title = "${info.app.name}",
				version = "${info.app.version:v1}",
				description = "${info.app.description}",
				contact = @Contact(
						name = "${info.app.contact.name}",
						email = "${info.app.contact.email}"
				),
				extensions = {@Extension(
						properties = {@ExtensionProperty(
								name = "x-data-access-network",
								value = "intradef"
						), @ExtensionProperty(
								name = "x-data-access-authorization",
								value = "nécessitant une autorisation du fournisseur API"
						), @ExtensionProperty(
								name = "x-data-security-classification",
								value = "dr"
						), @ExtensionProperty(
								name = "x-data-security-mention",
								value = "personnel"
						), @ExtensionProperty(
								name = "x-data-use-constraint",
								value = "rgpd"
						), @ExtensionProperty(
								name = "x-source",
								value = "intradef"
						)}
				)}
		),
		security = {@SecurityRequirement(
				name = "mindefconnect_authorization"
		)}
)
@SecurityScheme(
		name = "mindefconnect_authorization",
		type = SecuritySchemeType.APIKEY,
		in = SecuritySchemeIn.HEADER,
		paramName = "x-pesdt-intradef-authorization"
)
public interface PesdtApplication
{
	/**
	 * Lancement de l'application
	 *
	 * @param clazz Classe
	 * @param props propriété
	 * @param args  Arguments
	 * @return une instacnce de ConfigurableApplicationContext
	 */
	static ConfigurableApplicationContext runApp(Class<? extends PesdtApplication> clazz,
												 Map<String, Object> props,
												 String... args)
	{
		return (new SpringApplicationBuilder()).sources(clazz).initializers(context ->
				context.getEnvironment().getPropertySources().addFirst(new AppRuntimePropertySource(props))).run(args);
	}

	/**
	 * @param version
	 * @return Propriétés
	 */
	static Map<String, Object> customProperties(String version)
	{
		Map<String, Object> props = new HashMap();
		props.put("info.app.version", version);
		return props;
	}

	@Bean
	default RSAKeyProvider keyProvider(
			@Value("${pesdt.custom.header.authentication.public-key.path:#{null}}") String publicKeyPath,
			@Value("${pesdt.custom.header.authentication.public-key.kid:#{null}}") String publicKeyId,
			@Value("${pesdt.custom.header.authentication.public-key.kty:#{null}}") String publicKeyType)
			throws IOException
	{
		if (publicKeyPath != null && publicKeyId != null && publicKeyType != null)
		{
			HashMap<String, String> keys = new HashMap();
			String publicKey = String.join("\n",
					Files.readAllLines(ResourceUtils.getFile(publicKeyPath).toPath(),
							StandardCharsets.UTF_8));
			keys.put(publicKeyId, publicKey);
			return new KeycloakRSAPublicKeyProvider(publicKeyType, keys);
		}
		else
		{
			return new NoneRSAPublicKeyProvider();
		}
	}

	@Bean
	default OpenApiCustomiser customerGlobalOpenApiCustomiser(
			@Value("${pesdt.custom.header.swagger:false}") boolean appendHeaders,
			@Value("${pesdt.custom.header.authentication.key:" +
					"#{T(fr.gouv.intradef.pesdt.common.api.CustomHeaders).X_PESDT_INTRADEF_AUTHORIZATION_NAME}}")
			String authenticationKey,
			@Value("${pesdt.custom.overridden-version:#{null}}") String overriddenVersion)
	{
		return (openApi) -> {
			Optional<String> oldVersion = Optional.ofNullable(overriddenVersion);
			Objects.requireNonNull(openApi);
			oldVersion.ifPresent(openApi::setOpenapi);
			Paths paths = openApi.getPaths();
			Components components = openApi.getComponents();
			Map<String, io.swagger.v3.oas.models.security.SecurityScheme> map =
					Optional.ofNullable(components.getSecuritySchemes()).orElse(Collections.emptyMap());
			map.forEach((securityName, securityScheme) -> {
				if ("mindefconnect_authorization".equals(securityName) && securityScheme.getName() != null)
				{
					securityScheme.setName(authenticationKey);
				}

			});
			UnaryOperator<Components> componentFeeding;
			UnaryOperator<Paths> pathsFeeding;
			if (appendHeaders)
			{
				componentFeeding = GroupCustomiser::componentsWithCustomHeaders;
				pathsFeeding = GroupCustomiser::pathsWithCustomHeaders;
			}
			else
			{
				componentFeeding = (c) -> {
					return c;
				};
				pathsFeeding = (p) -> {
					return p;
				};
			}

			openApi.setPaths(
					GroupCustomiser.pathsWithUpdatedOperationDescs(
							GroupCustomiser.pathsWithMergedExtensions(pathsFeeding.apply(paths))));
			openApi.setComponents(GroupCustomiser.updateWrapperDescs(componentFeeding.apply(components)));
		};
	}

	/**
	 * @param environment
	 * @param buildProperties
	 * @param apiDocUrl
	 * @return les informations de l'API
	 */
	@Bean
	default ApiInfos serverInfos(Environment environment,
								 BuildProperties buildProperties,
								 @Value("${springdoc.api-docs.path:#{T(org.springdoc.core.Constants).DEFAULT_API_DOCS_URL}}")
								 String apiDocUrl)
	{
		String serverUrl = this.getServerUrl(environment.getProperty("eureka.instance.homePageUrl"),
				environment.getProperty("tomcat.hostname"));
		String scheme = this.getScheme(serverUrl);
		String hostname = this.getHostname(serverUrl);
		int port = this.getPort(serverUrl)
				.orElse(environment.getProperty("server.port", Integer.class, 8080));
		String contextPath = this.getContextPath(serverUrl)
				.orElse(environment.getProperty("server.servlet.context-path",
						environment.getProperty("tomcat.context-path",
								"")));
		List<String> versions = this.listOfStringsFromEnv(environment, "spring.application.versions")
				.orElse(Collections.singletonList("v1"));
		String appName = environment.getProperty("spring.application.name", buildProperties.getName());
		String now = ZonedDateTime.now().format(DateTimeFormatter.ISO_OFFSET_DATE_TIME);
		ApiInfos.ApiInfosBuilder builder = ApiInfos.builder().swaggerVersion(
						this.getSwaggerVersion())
				.apiName(appName)
				.buildVersion(buildProperties.getVersion());
		Optional<Instant> time = Optional.ofNullable(buildProperties.getTime());
		DateTimeFormatter formatter = DateTimeFormatter.ISO_OFFSET_DATE_TIME.withZone(ZoneId.systemDefault());
		Objects.requireNonNull(formatter);
		ServerInfos serverInfos = ServerInfos.builder().scheme(scheme).hostName(hostname).port(port)
				.apiDocUrl(apiDocUrl).applicationContextPath(contextPath).build();

		return builder.buildTime(time.map(formatter::format).orElse(now))
				.startUpTime(now)
				.apiVersions(versions)
				.serverInfos(serverInfos).build();
	}

	/**
	 * @param swaggerUiConfigParameters
	 * @param apiInfos
	 * @param actuatorPath
	 * @return Liste des roussources de l'api
	 */
	@Bean
	@Primary
	default CustomOpenApiResource customOpenApiResource(
			SwaggerUiConfigParameters swaggerUiConfigParameters,
			ApiInfos apiInfos,
			@Value("${management.endpoints.web.base-path:/actuator}") String actuatorPath)
	{
		CustomOpenApiResource customOpenApiResource =
				new CustomOpenApiResource(swaggerUiConfigParameters, apiInfos, actuatorPath);
		for (String version : apiInfos.getApiVersions())
		{
			customOpenApiResource.addOpenApiDefinition(apiInfos.getApiName(), version);
		}
		return customOpenApiResource;
	}

	/**
	 * @param customOpenApiResource Ressources des apis
	 * @return la liste des apis
	 */
	@Bean
	@Primary
	default List<GroupedOpenApi> getOpenApis(CustomOpenApiResource customOpenApiResource)
	{
		return customOpenApiResource.getOpenApiDefinitions();
	}

	@Bean
	default ErrorAttributes errorAttributes()
	{
		return new DefaultErrorAttributes()
		{
			/**
			 * @param webRequest
			 * @param includeStackTrace Non utilisé TODO
			 * @return Liste des attributs des erreurs
			 */
			public Map<String, Object> getErrorAttributes(WebRequest webRequest, boolean includeStackTrace)
			{
				ErrorAttributeOptions options = ErrorAttributeOptions.defaults();
				Map<String, Object> errorAttributes = super.getErrorAttributes(webRequest, options);
				Throwable error = this.getError(webRequest);
				if (error instanceof JWTVerificationException)
				{
					errorAttributes.put("status", HttpStatus.FORBIDDEN.value());
					errorAttributes.put("error", HttpStatus.FORBIDDEN.getReasonPhrase());
					webRequest.setAttribute("javax.servlet.error.status_code", 403, 0);
				}

				return errorAttributes;
			}
		};
	}

	/**
	 * @return Les propriété d'une erreur
	 */
	@Bean
	default ErrorProperties errorProperties()
	{
		ErrorProperties errorProperties = new ErrorProperties();
		errorProperties.setIncludeException(false);
		errorProperties.setIncludeStacktrace(ErrorProperties.IncludeAttribute.ALWAYS);
		return errorProperties;
	}

	/**
	 * @param errorAttributes
	 * @param errorProperties
	 * @return un controleur d'erreurs
	 */
	@Bean
	default ErrorController errorController(ErrorAttributes errorAttributes, ErrorProperties errorProperties)
	{
		return new BasicErrorController(errorAttributes, errorProperties)
		{
			public ResponseEntity<Map<String, Object>> error(HttpServletRequest request)
			{
				HttpStatus status = this.getStatus(request);
				if (status == HttpStatus.NO_CONTENT)
				{
					return new ResponseEntity(status);
				}
				else
				{
					Map<String, Object> body = this.getErrorAttributes(request, ErrorAttributeOptions.defaults());
					status = this.getStatus(request);
					return new ResponseEntity(body, status);
				}
			}
		};
	}

	/**
	 * @param environment
	 * @param propertyName
	 * @return La liste des valeurs des propriétés
	 */
	default Optional<List<String>> listOfStringsFromEnv(Environment environment, String propertyName)
	{
		List<String> result = new ArrayList();

		String value;
		for (int index = 0; (value = environment.getProperty(propertyName.concat(".")
				.concat(String.valueOf(index)))) != null; ++index)
		{
			result.add(value);
		}

		return Optional.of(result).filter(c -> !c.isEmpty());
	}

	/**
	 * @param serverUrl        url du serveur
	 * @param providedHostname hostname
	 * @return url du serveur
	 */
	default String getServerUrl(String serverUrl, String providedHostname)
	{
		String requestUrl;
		if (serverUrl == null)
		{
			String hostname = "localhost";
			try
			{
				hostname = Optional.ofNullable(providedHostname).orElse(InetAddress.getLocalHost().getHostName());
			}
			catch (UnknownHostException e)
			{
			}
			requestUrl = String.format("%s://%s", "http", hostname);
		}
		else
		{
			try
			{
				requestUrl = URLDecoder.decode(serverUrl, StandardCharsets.UTF_8.toString());
			}
			catch (UnsupportedEncodingException e)
			{
				requestUrl = serverUrl;
			}
		}

		return requestUrl;
	}

	/**
	 * Nom de protocole
	 *
	 * @param serverUrl url du serveur
	 * @return protocole associé au serveur
	 */
	default String getScheme(String serverUrl)
	{
		String protocol = "http";
		;
		try
		{
			protocol = (new URL(serverUrl)).getProtocol();
		}
		catch (MalformedURLException e)
		{
		}

		return protocol;
	}

	/**
	 * Nom de l'host
	 *
	 * @param serverUrl url du serveur
	 * @return le nom de l'host associé au serveur
	 */
	default String getHostname(String serverUrl)
	{
		String hostname = "localhost";
		try
		{
			hostname = (new URL(serverUrl)).getHost();
		}
		catch (MalformedURLException e)
		{
		}

		return hostname;
	}

	/**
	 * Port associé au serveur
	 *
	 * @param serverUrl url du serveur
	 * @return le port du serveur s'il existe
	 */
	default Optional<Integer> getPort(String serverUrl)
	{
		Optional port = Optional.empty();
		try
		{
			port = Optional.of((new URL(serverUrl)).getPort()).filter((p) -> {
				return p > 0;
			});
		}
		catch (MalformedURLException e)
		{
		}

		return port;
	}

	/**
	 * @param serverUrl url du serveur
	 * @return le chemin du context
	 */
	default Optional<String> getContextPath(String serverUrl)
	{
		Optional contextPath = Optional.empty();
		try
		{
			contextPath = Optional.of((new URL(serverUrl)).getPath()).filter(s -> !s.isEmpty());
		}
		catch (MalformedURLException var4)
		{
		}

		return contextPath;
	}

	/**
	 * @return la version du swagger
	 */
	String getSwaggerVersion();

	/**
	 * Source des propriétés
	 */
	class AppRuntimePropertySource extends PropertySource<String>
	{
		/**
		 * Map des propriétés par leur nom
		 */
		private final Map<String, Object> properties;

		/**
		 * Constructeur
		 *
		 * @param props liste des propriétés
		 */
		AppRuntimePropertySource(Map<String, Object> props)
		{
			super(AppRuntimePropertySource.class.getSimpleName());
			this.properties = new TreeMap(String.CASE_INSENSITIVE_ORDER);
			this.properties.putAll(props);
		}

		@Override
		public Object getProperty(@NonNull String name)
		{
			if (name == null)
			{
				throw new NullPointerException("name is marked non-null but is null");
			}
			else
			{
				return this.properties.get(name);
			}
		}

		@Override
		public boolean equals(Object o)
		{
			boolean result = true;
			if (o != this)
			{
				if (!(o instanceof AppRuntimePropertySource))
				{
					result = false;
				}
				else
				{
					AppRuntimePropertySource other = (AppRuntimePropertySource) o;
					if (!super.equals(o))
					{
						result = false;
					}
					else if (this.properties == null)
					{
						result = other.properties == null;
					}
					else
					{
						result = this.properties.equals(other.properties);
					}
				}
			}
			return result;
		}

		@Override
		public int hashCode()
		{
			int result = super.hashCode();
			Object $properties = this.properties;
			result = result * 59 + ($properties == null ? 43 : $properties.hashCode());
			return result;
		}
	}
}
