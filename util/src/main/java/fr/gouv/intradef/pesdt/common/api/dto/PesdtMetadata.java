/**
 * Données complémentaires à la réponse des services (version, taille de la page, nombre total de réponse, nombre de réponse de la page...)
 */
package fr.gouv.intradef.pesdt.common.api.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import io.swagger.v3.oas.annotations.media.Schema;

@JsonInclude(Include.NON_NULL)
@Schema(
		description = "Données complémentaires à la réponse"
)
public class PesdtMetadata
{
	private String version;
	private String environment;
	private Integer size;
	private Integer page;
	private Long total;

	public PesdtMetadata()
	{
	}

	public PesdtMetadata(String version, String environment, Integer size, Integer page, Long total)
	{
		this.version = version;
		this.environment = environment;
		this.size = size;
		this.page = page;
		this.total = total;
	}

	public static PesdtMetadataBuilder builder()
	{
		return new PesdtMetadataBuilder();
	}

	public String getVersion()
	{
		return this.version;
	}

	public void setVersion(String version)
	{
		this.version = version;
	}

	public String getEnvironment()
	{
		return this.environment;
	}

	public void setEnvironment(String environment)
	{
		this.environment = environment;
	}

	public Integer getSize()
	{
		return this.size;
	}

	public void setSize(Integer size)
	{
		this.size = size;
	}

	public Integer getPage()
	{
		return this.page;
	}

	public void setPage(Integer page)
	{
		this.page = page;
	}

	public Long getTotal()
	{
		return this.total;
	}

	public void setTotal(Long total)
	{
		this.total = total;
	}

	public String toString()
	{
		return "PesdtMetadata(version=" + this.getVersion() + ", environment=" + this.getEnvironment() + ", size=" + this.getSize() + ", page=" + this.getPage() + ", total=" + this.getTotal() + ")";
	}

	/**
	 * Builder de la classe PesdtMetadata
	 */
	public static class PesdtMetadataBuilder
	{
		private String version;
		private String environment;
		private Integer size;
		private Integer page;
		private Long total;

		PesdtMetadataBuilder()
		{
		}

		public PesdtMetadataBuilder version(String version)
		{
			this.version = version;
			return this;
		}

		public PesdtMetadataBuilder environment(String environment)
		{
			this.environment = environment;
			return this;
		}

		public PesdtMetadataBuilder size(Integer size)
		{
			this.size = size;
			return this;
		}

		public PesdtMetadataBuilder page(Integer page)
		{
			this.page = page;
			return this;
		}

		public PesdtMetadataBuilder total(Long total)
		{
			this.total = total;
			return this;
		}

		public PesdtMetadata build()
		{
			return new PesdtMetadata(this.version, this.environment, this.size, this.page, this.total);
		}

		public String toString()
		{
			return "PesdtMetadata.PesdtMetadataBuilder(version=" + this.version + ", environment=" + this.environment + ", size=" + this.size + ", page=" + this.page + ", total=" + this.total + ")";
		}
	}
}
