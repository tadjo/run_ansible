package fr.gouv.intradef.pesdt.common.infrastructure.configuration.swagger;

import fr.gouv.intradef.pesdt.common.api.CustomHeaderLocation;
import fr.gouv.intradef.pesdt.common.api.CustomHeaders;
import fr.gouv.intradef.pesdt.common.api.dto.PesdtBody;
import fr.gouv.intradef.pesdt.common.domain.exceptions.CustomiserException;
import io.swagger.v3.core.filter.SpecFilter;
import io.swagger.v3.oas.models.*;
import io.swagger.v3.oas.models.PathItem.HttpMethod;
import io.swagger.v3.oas.models.headers.Header;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.media.Schema;
import io.swagger.v3.oas.models.parameters.HeaderParameter;
import io.swagger.v3.oas.models.servers.Server;
import io.swagger.v3.parser.OpenAPIV3Parser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springdoc.core.customizers.OpenApiCustomiser;
import org.springframework.util.AntPathMatcher;
import org.springframework.util.StringUtils;

import java.util.*;
import java.util.Map.Entry;
import java.util.function.Function;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Custumiser
 */
public class GroupCustomiser extends SpecFilter implements OpenApiCustomiser
{
	private static final Logger LOGGER = LoggerFactory.getLogger(GroupCustomiser.class);
	private final String[] pathsToMatch;
	private final String apiName;
	private final String apiVersion;
	private final AntPathMatcher antPathMatcher;
	private String swaggerVersion;
	private OpenAPI openApi;
	private ServerInfos serverInfos;

	/**
	 * Constructeur
	 *
	 * @param apiName
	 * @param apiVersion
	 * @param pathsToMatch
	 */
	public GroupCustomiser(String apiName, String apiVersion, String... pathsToMatch)
	{
		this.apiName = apiName;
		this.apiVersion = apiVersion;
		this.pathsToMatch = pathsToMatch;
		this.antPathMatcher = new AntPathMatcher();
		this.openApi = null;
		this.swaggerVersion = null;
		this.serverInfos = null;
	}

	/**
	 * Constructeur
	 *
	 * @param pathsToMatch
	 * @param apiName
	 * @param apiVersion
	 * @param antPathMatcher
	 * @param swaggerVersion
	 * @param openApi
	 * @param serverInfos
	 */
	public GroupCustomiser(String[] pathsToMatch, String apiName, String apiVersion, AntPathMatcher antPathMatcher,
						   String swaggerVersion, OpenAPI openApi, ServerInfos serverInfos)
	{
		this.pathsToMatch = pathsToMatch;
		this.apiName = apiName;
		this.apiVersion = apiVersion;
		this.antPathMatcher = antPathMatcher;
		this.swaggerVersion = swaggerVersion;
		this.openApi = openApi;
		this.serverInfos = serverInfos;
	}

	/**
	 * Update des informations des paths
	 *
	 * @param paths Chemins à parcourir
	 * @return les paths mis à jour
	 */
	public static Paths pathsWithUpdatedOperationDescs(Paths paths)
	{
		paths.values().forEach((p) -> {
			p.readOperations().forEach(GroupCustomiser::updateOperationDesc);
		});
		return paths;
	}

	/**
	 * @param components Composants à mettre à jour
	 * @return les composants sans les référence à l'actuator
	 */
	public static Components removeActuatorRootObject(Components components)
	{
		Map<String, Schema> schemas = new HashMap(components.getSchemas());
		schemas.remove("Link");
		components.setSchemas(schemas);
		return components;
	}

	/**
	 * Mise à jour des descriptions associées aux composants
	 *
	 * @param components à mettre à jour
	 * @return components mis à jour
	 */
	public static Components updateWrapperDescs(Components components)
	{
		Map<String, Schema> schemas = components.getSchemas();
		Map<String, Schema> newSchemas = new HashMap(schemas);
		String wrapperName = PesdtBody.class.getSimpleName();
		String wrapperListName = wrapperName.concat("List");
		Iterator iterator = schemas.entrySet().iterator();

		while (iterator.hasNext())
		{
			Map.Entry<String, Schema> entry = (Map.Entry) iterator.next();
			String schemaName = entry.getKey();
			Schema<?> schema = (Schema) entry.getValue();
			if (schemaName.startsWith(wrapperName))
			{
				if (schemaName.startsWith(wrapperListName))
				{
					if (wrapperListName.concat("Long").equalsIgnoreCase(schemaName))
					{
						schema.description("Enveloppe de réponse".concat(" pour liste d'identifiants"));
					}
					else
					{
						schema.description(
								"Enveloppe de réponse".concat(String.format(" pour une liste d'objets de type %s",
										schemaName.replaceAll("^".concat(wrapperListName).concat("(.+)$"),
												"$1"))));
					}
				}
				else
				{
					schema.description(
							"Enveloppe de réponse".concat(String.format(" pour un objet de type %s",
									schemaName.replaceAll("^".concat(wrapperName).concat("(.+)$"),
											"$1"))));
				}
			}
		}

		components.setSchemas(newSchemas);
		return components;
	}

	/**
	 * Suppression de l'actuator
	 *
	 * @param paths        à modifier
	 * @param actuatorPath Chemin de l'actuator
	 * @return paths mis à jour
	 */
	public static Paths removeActuatorRootEndpoint(Paths paths, String actuatorPath)
	{
		Objects.requireNonNull(paths);
		Map map = paths.entrySet().stream().filter((pathEntry) ->
						Pattern.compile("^(/v\\d+)?".concat(actuatorPath).concat("$")).matcher(pathEntry.getKey()).matches())
				.filter((pathEntry) ->
						(pathEntry.getValue()).readOperationsMap().entrySet().stream().allMatch((opeEntry) ->
								HttpMethod.GET.equals(opeEntry.getKey())))
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
		map.forEach(paths::remove);
		return paths;
	}

	/**
	 * Mise à jour des paths en mergeant les extensions
	 *
	 * @param paths Chemins
	 * @return paths mis à jour
	 */
	public static Paths pathsWithMergedExtensions(Paths paths)
	{
		for (PathItem pathItem : paths.values())
		{
			Stream<Map<String, Object>> operationsStream =
					pathItem.readOperations().stream().map(Operation::getExtensions).filter(Objects::nonNull);
			Stream<Map.Entry<String, Object>> mapStream = operationsStream.flatMap((e) -> e.entrySet().stream());
			List<java.util.Map.Entry<String, Object>> list = mapStream.collect(Collectors.toList());

			Function<Entry<String, Object>, List<Operation>> entryListFunction = extension -> {
				Stream<Operation> operationStream = pathItem.readOperations().stream().filter((o) -> {
					Map<String, Object> map1 = Optional.ofNullable(o.getExtensions()).orElse(new HashMap<>());
					return map1.entrySet().stream().anyMatch((e) ->
							extension.getKey().equals(e.getKey()) && extension.getValue().equals(e.getValue()));
				});

				return (List<Operation>) operationStream.collect(Collectors.toList());
			};
			Map<Map.Entry<String, Object>, List<Operation>> operationsByExtension =
					list.stream().distinct().collect(Collectors.toMap(extension -> extension,
							entryListFunction));

			Map<String, Object> pathExtensions = Optional.ofNullable(pathItem.getExtensions()).orElse(new HashMap<>());
			operationsByExtension.forEach((extension, operations) -> {
				if (operations.size() == pathItem.readOperations().size())
				{
					operations.forEach((o) -> {
						o.getExtensions().remove(extension.getKey());
					});
					pathExtensions.put(extension.getKey(), extension.getValue());
				}

			});
			pathItem.setExtensions(pathExtensions);
		}
		return paths;
	}

	/**
	 * Update du header
	 *
	 * @param pathItem   chemin
	 * @param header     header
	 * @param headerName nom du chemin
	 */
	public static void appendHeader(PathItem pathItem, CustomHeaders header, String headerName)
	{
		if (CustomHeaderLocation.BOTH.equals(header.getLocation())
				|| CustomHeaderLocation.REQUEST.equals(header.getLocation()))
		{
			pathItem.addParametersItem((new HeaderParameter()).name(headerName)
					.$ref("#/components/parameters/".concat(headerName)));
		}

		if (CustomHeaderLocation.BOTH.equals(header.getLocation())
				|| CustomHeaderLocation.RESPONSE.equals(header.getLocation()))
		{
			pathItem.readOperations().stream().map(Operation::getResponses)
					.flatMap((responses) -> responses.values().stream())
					.forEach((response) -> response.addHeaderObject(headerName,
							(new Header()).$ref("#/components/headers/".concat(headerName))));
		}

	}

	/**
	 * Mise des description d'une operation
	 *
	 * @param operation operation a mettre à jour
	 */
	public static void updateOperationDesc(Operation operation)
	{
		if (StringUtils.hasLength(operation.getDescription()) && !StringUtils.hasLength(operation.getSummary()))
		{
			operation.setDescription(operation.getSummary());
		}
	}

	/**
	 * Mise à jour des headers des paths
	 *
	 * @param paths a mettre à jour
	 */
	public static Paths pathsWithCustomHeaders(Paths paths)
	{
		paths.values().forEach((pathItem) -> {
			Arrays.stream(CustomHeaders.values()).forEach((header) -> {
				appendHeader(pathItem, header, header.getName());
			});
		});
		return paths;
	}

	/**
	 * Mise à jour des headers des paths
	 *
	 * @param paths             a mettre à jour
	 * @param authenticationKey clé d'authentification
	 * @param pathsToExclude    paths à ignorer
	 * @return paths mis à jour
	 */
	public static Paths pathsWithAuthorizationHeaders(Paths paths, String authenticationKey, List<String> pathsToExclude)
	{
		paths.entrySet().stream().filter((entry) ->
						pathsToExclude.stream().noneMatch(exclusion -> entry.getKey().matches(exclusion)))
				.forEach(entry ->
						appendHeader(entry.getValue(), CustomHeaders.X_PESDT_INTRADEF_AUTHORIZATION, authenticationKey));
		return paths;
	}

	/**
	 * Ajout de paramètres aux components
	 *
	 * @param components à mettre à jour
	 * @param header     header de référence
	 */
	public static void appendParameter(Components components, CustomHeaders header)
	{
		appendParameter(components, header, header.getName());
	}

	/**
	 * Ajout de paramètres aux components
	 *
	 * @param components à mettre à jour
	 * @param header     header de référence
	 * @param headerName nom du header
	 */
	public static void appendParameter(Components components, CustomHeaders header, String headerName)
	{
		components.addParameters(headerName, (new HeaderParameter()).required(header.isRequired()).name(headerName)
				.description(header.getDescription()).schema(header.getType()));
		components.addHeaders(headerName, (new Header()).required(header.isRequired())
				.description(header.getDescription()).schema(header.getType()));
	}

	/**
	 * @param components à mettre à jour
	 * @return components mis à jour
	 */
	public static Components componentsWithCustomHeaders(Components components)
	{
		Arrays.stream(CustomHeaders.values()).forEach((header) -> appendParameter(components, header));
		return components;
	}

	/**
	 * @param components        à mettre à jour
	 * @param authenticationKey de référence
	 * @return mis à jour
	 */
	public static Components componentsWithAuthorizationHeaders(Components components, String authenticationKey)
	{
		appendParameter(components, CustomHeaders.X_PESDT_INTRADEF_AUTHORIZATION, authenticationKey);
		return components;
	}

	/**
	 * @param swaggerVersion de référence
	 * @return Customiser mis à jour
	 */
	public GroupCustomiser withSwaggerVersion(String swaggerVersion)
	{
		this.swaggerVersion = swaggerVersion;
		return this;
	}

	/**
	 * @param serverInfos de référence
	 * @return Custimser mis à jour
	 */
	public GroupCustomiser withServerInfos(ServerInfos serverInfos)
	{
		this.serverInfos = serverInfos;
		return this;
	}

	/**
	 * Mise à jour d'une api
	 *
	 * @param openApi à mettre à jour
	 */
	public void customise(OpenAPI openApi)
	{
		if (Objects.isNull(this.serverInfos))
		{
			throw new CustomiserException("Required serverInfos is missing");
		}
		else
		{
			if (this.openApi == null || this.openApi.getPaths().size() < 1)
			{
				String defaultDocUrl = String.format("%s://%s:%d%s%s",
						this.serverInfos.getScheme(),
						this.serverInfos.getHostName(),
						this.serverInfos.getPort(),
						this.serverInfos.getApplicationContextPath(),
						this.serverInfos.getApiDocUrl());
				LOGGER.debug("Default OpenAPI doc is not initialized, calling default API doc URL {}", defaultDocUrl);

				try
				{
					this.openApi = (new OpenAPIV3Parser()).read(defaultDocUrl);
				}
				catch (Exception var4)
				{
					throw new CustomiserException("Error while calling default API doc URL", var4);
				}
			}

			openApi.setComponents(this.openApi.getComponents());
			openApi.setExtensions(this.openApi.getExtensions());
			openApi.setInfo((Info) Optional.ofNullable(this.openApi.getInfo()).orElse(new Info()));
			openApi.getInfo().setTitle(this.apiName);
			openApi.getInfo().setVersion((String) Optional.ofNullable(this.swaggerVersion).orElse(this.apiVersion));
			openApi.setExternalDocs(this.openApi.getExternalDocs());
			openApi.setOpenapi(this.openApi.getOpenapi());
			openApi.setSecurity(this.openApi.getSecurity());
			openApi.setTags(this.openApi.getTags());
			List<Server> servers = Optional.ofNullable(this.openApi.getServers()).orElse(Collections.emptyList());
			servers = servers.stream().map((s) -> (new Server()).url(s.getUrl().concat("/").concat(this.apiVersion))
							.description(this.apiName.concat(" - ").concat(this.apiVersion))
							.extensions(s.getExtensions()).variables(s.getVariables()))
					.collect(Collectors.toList());
			openApi.setServers(servers);
			Paths paths = new Paths();
			(Optional.ofNullable(this.openApi.getPaths()).orElse(new Paths())).forEach((key, value) -> {
				if (this.isPathToMatch(key))
				{
					String nKey = key.replaceAll("^".concat("/").concat(this.apiVersion).concat("/"),
							"/");
					paths.addPathItem(nKey, value);
				}
			});
			openApi.setPaths(paths);
			super.removeBrokenReferenceDefinitions(openApi);
		}
	}

	/**
	 * @param operationPath path
	 * @return true s'il y a match
	 */
	private boolean isPathToMatch(String operationPath)
	{
		return Arrays.stream(this.pathsToMatch).anyMatch(pattern -> this.antPathMatcher.match(pattern, operationPath));
	}

	/**
	 * Getter
	 *
	 * @return paths to match
	 */
	public String[] getPathsToMatch()
	{
		return this.pathsToMatch;
	}

}
