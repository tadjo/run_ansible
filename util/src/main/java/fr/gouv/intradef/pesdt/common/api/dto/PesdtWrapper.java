/**
 * Type de retour customisé qui étend la classe ResponseEntity
 */

package fr.gouv.intradef.pesdt.common.api.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import fr.gouv.intradef.pesdt.common.domain.utils.PesdtResponseUtils;

import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static fr.gouv.intradef.pesdt.common.api.dto.PesdtBody.builder;

@JsonInclude(Include.NON_NULL)
public class PesdtWrapper<T> extends ResponseEntity<PesdtBody<T>>
{
	public PesdtWrapper(HttpStatus status)
	{
		super(status);
	}

/*
 TODO: Note, vérifier s'il faut instancier un PesdtBody pour que le builder ait le même type que celui en argument des
  fonctions (commentaire sur la ligne ci dessous et dans chaque fonction à décommenter)
*/
//    private static PesdtBody pesdtBody = new PesdtBody();


	/**
	 * Renvoi une liste contenant la réponse de la requête, et l'indication si c'est une réponse partielle ou complete
	 *
	 * @param Page<T> contenant les éléments de la réponse
	 * @return retourne le builder avec le body de la liste des élements de la Page et le status correspondant
	 * (PARTIAL_CONTENT OU OK)
	 */
	public static <T extends AbstractDto<?>> ResponseEntity<PesdtBody<List<T>>> partial(Page<T> page)
	{
		HttpStatus responseStatus = PesdtResponseUtils.getPageableStatusCode(page.getPageable().getPageNumber(),
				page.getTotalPages());
		PesdtBody.PesdtBodyBuilder<List<T>> builder = builder();
		return status(responseStatus).body(builder.data(page.getContent()).metadata(PesdtMetadata.builder()
				.total(page.getTotalElements()).page(page.getPageable().getPageNumber() + 1)
				.size(page.getNumberOfElements()).build()).build());
	}

	/**
	 * @param entity
	 * @param <T>
	 * @return Retourne le builder avec  le body de l'entity et le status OK
	 */
	public static <T extends AbstractDto<?>> ResponseEntity<PesdtBody<T>> ok(T entity)
	{
//        pesdtBody = new PesdtBody<T>(entity);
		PesdtBody.PesdtBodyBuilder<T> builder = builder();
		return ok(builder.data(entity).metadata(PesdtMetadata.builder().build()).build());
	}

	/**
	 * @param wrapper
	 * @param <T>
	 * @return Retourne le builder avec  le body du wrapper et le status OK
	 */
	public static <T extends AbstractDtoWrapper<?>> ResponseEntity<PesdtBody<T>> ok(T wrapper)
	{
//        pesdtBody = new PesdtBody<T>(wrapper);
		PesdtBody.PesdtBodyBuilder<T> builder = builder();
		return ok(builder.data(wrapper).metadata(PesdtMetadata.builder().build()).build());
	}

	/**
	 * @param list d'objet T
	 * @param <T>
	 * @return Retourne le builder avec  le body de la liste d'objet et le status OK
	 */
	public static <T extends AbstractDto<?>> ResponseEntity<PesdtBody<List<T>>> ok(List<T> list)
	{
//        pesdtBody = new PesdtBody<T>();
		PesdtBody.PesdtBodyBuilder<List<T>> builder = builder();
		return ok(builder.data(list).metadata(PesdtMetadata.builder().size(list != null ? list.size() : 0).build()).build());

	}

	/**
	 * @param Collection d'objet
	 * @param <S>
	 * @return Retourne le builder avec le body de la collection d'objet et le status OK
	 */
	public static <S> ResponseEntity<PesdtBody<List<S>>> ok(Collection<S> list)
	{
//        pesdtBody = new PesdtBody<S>();
		PesdtBody.PesdtBodyBuilder<Collection<S>> builder = builder();
		return ok(builder.data(new ArrayList((Collection) Optional.ofNullable(list).orElse(Collections.emptyList()))).metadata(PesdtMetadata.builder().size(list != null ? list.size() : 0).build()).build());
	}

	/**
	 * @param URI    de la requête
	 * @param entity
	 * @param <T>
	 * @return Retourne le builder avec le status CREATED, l'URI dans le header et le body de l'entity
	 */
	public static <T extends AbstractDto<?>> ResponseEntity<PesdtBody<T>> created(URI location, T entity)
	{
//        pesdtBody = new PesdtBody<T>(entity);
		PesdtBody.PesdtBodyBuilder<T> builder = builder();
		return created(location).body(builder.data(entity).metadata(PesdtMetadata.builder().build()).build());
	}

	/**
	 * @param list
	 * @param <T>
	 * @return Retourne le builder avec le status ACCEPTED et le body de la liste
	 */
	public static <T extends AbstractDto<?>> ResponseEntity<PesdtBody<List<T>>> accepted(List<T> list)
	{
//        pesdtBody = new PesdtBody<T>();
		PesdtBody.PesdtBodyBuilder<List<T>> builder = builder();
		return accepted().body(builder.data(list).metadata(PesdtMetadata.builder().size(list != null ? list.size() : 0).build()).build());
	}
}