
package fr.gouv.intradef.pesdt.common.api.dto;


import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.time.LocalDateTime;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@Schema(description = "DTO abstrait propre au SI Activités possédant uniquement les champs techniques")
public abstract class AbstractCustomDTO extends AbstractDto<Long>
{

	@JsonProperty("createur")
	@Schema(description = "Nom du créateur", example = "p.dupont", maxLength = 255, nullable = true)
	private String createur;

	@JsonProperty("creation")
	@Schema(description = "Date de création", example = "2021-02-02T00:00:00Z", nullable = true)
	private LocalDateTime creation;

	@JsonProperty("modificateur")
	@Schema(description = "Nom du modificateur", example = "p.dupont", maxLength = 255, nullable = true)
	private String modificateur;

	@JsonProperty("modification")
	@Schema(description = "Date de dernière mise à jour", example = "2021-02-02T00:00:00Z", nullable = true)
	private LocalDateTime modification;

}
