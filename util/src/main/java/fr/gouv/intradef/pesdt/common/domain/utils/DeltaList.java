package fr.gouv.intradef.pesdt.common.domain.utils;

import javax.validation.constraints.NotNull;

import java.util.List;

/**
 * Stockage des éléments de comparaison de deux listes
 *
 * @param <OLD, NEW>
 */
public class DeltaList<OLD, NEW>
{
	/**
	 * Liste qui représente les éléments sui sont présent uniquement en base de données
	 */
	@NotNull
	public List<OLD> oldElements;

	/**
	 * Liste qui représente les éléments communs au deux listes comparées, identique à {@link DeltaList#commonNewElements} sauf que les objets
	 * référencés sont du type OLD
	 */
	@NotNull
	public List<OLD> commonOldElements;

	/**
	 * Liste qui représente les éléments communs au deux listes comparées, identique à {@link DeltaList#commonOldElements} sauf que les objets
	 * référencés sont du type New
	 */
	@NotNull
	public List<NEW> commonNewElements;

	/**
	 * Liste
	 */
	@NotNull
	public List<NEW> newElements;

	/**
	 * Classe permettant de comparer des listes
	 */

}
