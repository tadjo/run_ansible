package fr.gouv.intradef.pesdt.common.infrastructure.configuration.swagger;

import io.swagger.v3.oas.models.OpenAPI;
import org.springdoc.core.GroupedOpenApi;
import org.springdoc.core.customizers.OpenApiCustomiser;

public class ApiDefinition
{
	private String apiName;
	private String apiVersion;
	private OpenApiCustomiser groupCustomiser;
	private OpenAPI openAPI;
	private GroupedOpenApi groupedOpenApi;

	public ApiDefinition(String apiName, String apiVersion, OpenApiCustomiser groupCustomiser, OpenAPI openAPI, GroupedOpenApi groupedOpenApi)
	{
		this.apiName = apiName;
		this.apiVersion = apiVersion;
		this.groupCustomiser = groupCustomiser;
		this.openAPI = openAPI;
		this.groupedOpenApi = groupedOpenApi;
	}

	private static OpenAPI $default$openAPI()
	{
		return new OpenAPI();
	}

	public static ApiDefinitionBuilder builder()
	{
		return new ApiDefinitionBuilder();
	}

	public String getApiName()
	{
		return this.apiName;
	}

	public void setApiName(String apiName)
	{
		this.apiName = apiName;
	}

	public String getApiVersion()
	{
		return this.apiVersion;
	}

	public void setApiVersion(String apiVersion)
	{
		this.apiVersion = apiVersion;
	}

	public OpenApiCustomiser getGroupCustomiser()
	{
		return this.groupCustomiser;
	}

	public void setGroupCustomiser(OpenApiCustomiser groupCustomiser)
	{
		this.groupCustomiser = groupCustomiser;
	}

	public OpenAPI getOpenAPI()
	{
		return this.openAPI;
	}

	public void setOpenAPI(OpenAPI openAPI)
	{
		this.openAPI = openAPI;
	}

	public GroupedOpenApi getGroupedOpenApi()
	{
		return this.groupedOpenApi;
	}

	public void setGroupedOpenApi(GroupedOpenApi groupedOpenApi)
	{
		this.groupedOpenApi = groupedOpenApi;
	}

	public static class ApiDefinitionBuilder
	{
		private String apiName;
		private String apiVersion;
		private OpenApiCustomiser groupCustomiser;
		private boolean openAPI$set;
		private OpenAPI openAPI$value;
		private GroupedOpenApi groupedOpenApi;

		ApiDefinitionBuilder()
		{
		}

		public ApiDefinitionBuilder apiName(String apiName)
		{
			this.apiName = apiName;
			return this;
		}

		public ApiDefinitionBuilder apiVersion(String apiVersion)
		{
			this.apiVersion = apiVersion;
			return this;
		}

		public ApiDefinitionBuilder groupCustomiser(OpenApiCustomiser groupCustomiser)
		{
			this.groupCustomiser = groupCustomiser;
			return this;
		}

		public ApiDefinitionBuilder openAPI(OpenAPI openAPI)
		{
			this.openAPI$value = openAPI;
			this.openAPI$set = true;
			return this;
		}

		public ApiDefinitionBuilder groupedOpenApi(GroupedOpenApi groupedOpenApi)
		{
			this.groupedOpenApi = groupedOpenApi;
			return this;
		}

		public ApiDefinition build()
		{
			OpenAPI openAPI$value = this.openAPI$value;
			if (!this.openAPI$set)
			{
				openAPI$value = ApiDefinition.$default$openAPI();
			}

			return new ApiDefinition(this.apiName, this.apiVersion, this.groupCustomiser, openAPI$value, this.groupedOpenApi);
		}

		public String toString()
		{
			return "ApiDefinition.ApiDefinitionBuilder(apiName=" + this.apiName + ", apiVersion=" + this.apiVersion + ", groupCustomiser=" + this.groupCustomiser + ", openAPI$value=" + this.openAPI$value + ", groupedOpenApi=" + this.groupedOpenApi + ")";
		}
	}
}
