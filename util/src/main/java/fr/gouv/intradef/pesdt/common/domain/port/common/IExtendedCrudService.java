package fr.gouv.intradef.pesdt.common.domain.port.common;

import fr.gouv.intradef.pesdt.common.domain.exceptions.LowerVersionException;

import java.util.List;

public interface IExtendedCrudService<D, K> extends ICrudService<D, K>
{

	/**
	 * Méthode de mise à jour d'une liste d'entités
	 *
	 * @param entities entités à mettre à jour
	 * @return entités persistés
	 */
	List<D> updateAll(List<D> entities);

	/**
	 * Méthode de suppression des entités
	 *
	 * @param ids Identifiant de l'entité
	 * @return Entités supprimées
	 * @throws fr.gouv.intradef.pesdt.common.domain.exceptions.NullParameterException si l'entité est null
	 * @throws LowerVersionException                                                  si l'id est null
	 */
	List<D> deleteAllById(List<K> ids);
}
