package fr.gouv.intradef.pesdt.common.domain.utils;

import org.springframework.http.HttpStatus;

public final class PesdtResponseUtils
{
	private PesdtResponseUtils()
	{
		throw new UnsupportedOperationException("This is a utility class and cannot be instantiated");
	}

	/**
	 * @param pageNumber
	 * @param pageTotal
	 * @return Le statut PARTIAL_CONTENT si le nombre de page est inférieur aux page totales, OK sinon
	 */
	public static HttpStatus getPageableStatusCode(int pageNumber, int pageTotal)
	{
		return pageNumber + 1 < pageTotal ? HttpStatus.PARTIAL_CONTENT : HttpStatus.OK;
	}
}