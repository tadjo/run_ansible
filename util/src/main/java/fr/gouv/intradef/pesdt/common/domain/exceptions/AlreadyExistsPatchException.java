package fr.gouv.intradef.pesdt.common.domain.exceptions;


import fr.gouv.intradef.pesdt.common.infrastructure.adapter.jpa.entity.AbstractEntity;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.*;

/**
 * Exception utilisée dans le cas où l'entité existe déjà
 */
@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class AlreadyExistsPatchException extends RuntimeException
{

	public <T> AlreadyExistsPatchException(List<T> ids, Class<? extends AbstractEntity<T>> clazz)
	{
		super(String.format("Les entités du type %s existent déjà avec ces id %s", clazz.getSimpleName(),
				String.join(", ", (ids != null ? ids.stream().map(Objects::toString).toArray(String[]::new) : new String[0]))));
	}
}
