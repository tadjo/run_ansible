package fr.gouv.intradef.pesdt.common.domain.exceptions;

import fr.gouv.intradef.pesdt.common.infrastructure.adapter.jpa.entity.AbstractEntity;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.PRECONDITION_REQUIRED)
public class LowerVersionException extends RuntimeException
{
	private static final long serialVersionUID = -6729396227937640241L;

	public <T> LowerVersionException(Long tried, Long actual, Class<? extends AbstractEntity<T>> clazz)
	{
		super(String.format("La version de l'objet de type '%s' n'est pas la dernière : %s < %s", clazz.getSimpleName().replace("Entity", ""), tried, actual));
	}
}
