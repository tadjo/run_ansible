package fr.gouv.intradef.pesdt.common.infrastructure.configuration.kafka;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Component;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@EnableAsync
@Component
public class KafkaProducer
{
	@Autowired
	private KafkaTemplate<String, String> kafkaTemplate;
	@Autowired
	private ObjectFactory<ListenableFutureCallback<? super SendResult<String, String>>> kafkaEventHandlerFactory;

	public KafkaProducer()
	{
	}

	public static String prepareData(String operation, String modificateur, String service, String table, Object donnee) throws JsonProcessingException
	{
		return (new ObjectMapper()).writeValueAsString(new KafkaMessageCreateDto(operation, modificateur, LocalDateTime.now().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME), service, table, donnee));
	}

	@Async
	public void send(String topic, String key, String data)
	{
		Message<String> message =
				MessageBuilder.withPayload(data).setHeader("kafka_topic",
						topic.toLowerCase()).setHeader("kafka_messageKey",
						key).build();
		ListenableFuture<SendResult<String, String>> future = this.kafkaTemplate.send(message);
		future.addCallback(this.kafkaEventHandlerFactory.getObject());
	}
}
