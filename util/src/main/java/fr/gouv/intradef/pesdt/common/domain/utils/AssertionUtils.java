/**
 * Classe d'assertion qui renvoi des exception Null ou Empty
 */

package fr.gouv.intradef.pesdt.common.domain.utils;

import fr.gouv.intradef.pesdt.common.domain.exceptions.BadPutParameterException;
import fr.gouv.intradef.pesdt.common.domain.exceptions.NullParameterException;

import java.util.Collection;

import org.springframework.util.CollectionUtils;

public final class AssertionUtils
{
	private AssertionUtils()
	{
		throw new UnsupportedOperationException("This is a utility class and cannot be instantiated");
	}

	public static void assertNotNull(Object o)
	{
		if (o == null)
		{
			throw new NullParameterException();
		}
	}

	public static void assertEquals(Object a, Object b)
	{
		if (!a.equals(b))
		{
			throw new BadPutParameterException();
		}
	}

	public static void assertNotEmpty(Collection<?> collection)
	{
		if (CollectionUtils.isEmpty(collection))
		{
			throw new NullParameterException();
		}
	}
}
