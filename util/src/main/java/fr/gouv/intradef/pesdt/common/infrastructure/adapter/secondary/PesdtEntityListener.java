package fr.gouv.intradef.pesdt.common.infrastructure.adapter.secondary;

import fr.gouv.intradef.pesdt.common.infrastructure.adapter.jpa.entity.AbstractEntity;

import java.util.Optional;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

/**
 * Listener des pesdtEntity
 */
public class PesdtEntityListener
{
	public PesdtEntityListener()
	{
	}

	@PrePersist
	public void prePersist(AbstractEntity<?> abstractEntity)
	{
		if (abstractEntity != null)
		{
			abstractEntity.setVersion(1L);
		}
	}

	@PreUpdate
	public void preUpdate(AbstractEntity<?> abstractEntity)
	{
		if (abstractEntity != null)
		{
			abstractEntity.setVersion(Optional.ofNullable(abstractEntity.getVersion()).orElse(0L) + 1L);
		}
	}
}
