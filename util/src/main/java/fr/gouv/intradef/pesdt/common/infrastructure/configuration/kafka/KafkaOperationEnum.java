package fr.gouv.intradef.pesdt.common.infrastructure.configuration.kafka;

public enum KafkaOperationEnum
{
	CREATION("C"),
	LECTURE("L"),
	MODIFICATION("M"),
	SUPPRESSION("S"),
	NOTIFICATION("N");

	private final String operationValue;

	KafkaOperationEnum(String operationValue)
	{
		this.operationValue = operationValue;
	}

	public String getOperationValue()
	{
		return this.operationValue;
	}
}
