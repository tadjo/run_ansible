package fr.gouv.intradef.pesdt.common.domain.utils;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.auth0.jwt.interfaces.JWTVerifier;
import com.auth0.jwt.interfaces.RSAKeyProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.*;
import java.util.stream.Collectors;

public final class TokenUtils
{
	private TokenUtils()
	{
		throw new UnsupportedOperationException("This is a utility class and cannot be instantiated");
	}

	public static Map<String, Object> parseToken(DecodedJWT jwt)
	{
		return (Map) jwt.getClaims().entrySet().stream().map((e) -> {
			return new AbstractMap.SimpleImmutableEntry(e.getKey(), e.getValue().as(Object.class));
		}).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
	}

	public static Map<String, Object> parseToken(String token)
	{
		return parseToken(JWT.decode(token));
	}

	public static Optional<String> getUserPrincipal()
	{
		return Optional.ofNullable(SecurityContextHolder.getContext()).flatMap((s) -> {
			return Optional.ofNullable(s.getAuthentication());
		}).filter(Authentication::isAuthenticated).map(Authentication::getPrincipal).map(Object::toString);
	}

	public static DecodedJWT verifySignature(String token, RSAKeyProvider keyProvider) throws JWTVerificationException
	{
		DecodedJWT jwt = JWT.decode(token);
		JWTVerifier verifier = JWT.require(Algorithm.RSA256(keyProvider)).withIssuer(new String[]{jwt.getIssuer()}).build();
		return verifier.verify(token);
	}

	public static String getGivenName(Map<String, Object> attributes)
	{
		return (String) attributes.get("given_name");
	}

	public static String getPreferredUsername(Map<String, Object> attributes)
	{
		return (String) attributes.get("preferred_username");
	}

	public static String getEmail(Map<String, Object> attributes)
	{
		return (String) attributes.get("email");
	}

	public static String getIdSIRH(Map<String, Object> attributes)
	{
		return (String) attributes.get("id_sirh");
	}

	public static String getEmployeeNumber(Map<String, Object> attributes)
	{
		return (String) attributes.get("employee_number");
	}

	public static Date getAuthTime(Map<String, Object> attributes)
	{
		return new Date(1000L * (Long) attributes.get("auth_time"));
	}

	public static List<String> getAudience(Map<String, Object> attributes)
	{
		return (List<String>) attributes.get("aud");
	}

	public static Map<String, Object> getResourceAccess(Map<String, Object> attributes)
	{
		return (Map<String, Object>) attributes.get("resource_access");
	}
}
