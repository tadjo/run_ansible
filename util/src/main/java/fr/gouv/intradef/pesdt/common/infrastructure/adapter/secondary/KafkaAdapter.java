package fr.gouv.intradef.pesdt.common.infrastructure.adapter.secondary;

import com.fasterxml.jackson.core.JsonProcessingException;
import fr.gouv.intradef.pesdt.common.domain.exceptions.KafkaParseToJsonException;
import fr.gouv.intradef.pesdt.common.domain.port.secondary.IKafka;
import fr.gouv.intradef.pesdt.common.domain.utils.TokenUtils;
import fr.gouv.intradef.pesdt.common.infrastructure.configuration.kafka.KafkaKeyEnum;
import fr.gouv.intradef.pesdt.common.infrastructure.configuration.kafka.KafkaOperationEnum;
import fr.gouv.intradef.pesdt.common.infrastructure.configuration.kafka.KafkaProducer;
import fr.gouv.intradef.pesdt.common.infrastructure.configuration.security.JWTProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class KafkaAdapter implements IKafka
{
	private static final Logger LOGGER = LoggerFactory.getLogger(KafkaAdapter.class);
	//TODO: Warning, valeur à surveiller si un bug apparait
	@Value("${kafka.topic : Activite}")
	private String kafkaTopic;
	@Autowired
	private KafkaProducer kafkaProducer;
	@Autowired
	private JWTProperties jwtProperties;

	public KafkaAdapter()
	{
	}

	public void save(Object dtoKafka, String table)
	{
		this.send(dtoKafka, table, KafkaKeyEnum.MODIFICATION, KafkaOperationEnum.CREATION);
	}

	public void read(Object dtoKafka, String table)
	{
		this.send(dtoKafka, table, KafkaKeyEnum.LECTURE, KafkaOperationEnum.LECTURE);
	}

	public void update(Object dtoKafka, String table)
	{
		this.send(dtoKafka, table, KafkaKeyEnum.MODIFICATION, KafkaOperationEnum.MODIFICATION);
	}

	public void delete(Object dtoKafka, String table)
	{
		this.send(dtoKafka, table, KafkaKeyEnum.MODIFICATION, KafkaOperationEnum.SUPPRESSION);
	}

	private void send(Object dtoKafka, String table, KafkaKeyEnum key, KafkaOperationEnum operation)
	{
		if (dtoKafka instanceof List && ((List) dtoKafka).isEmpty())
		{
			LOGGER.debug("This data list is empty and will not be sent to Kafka");
		}
		else
		{
			String currentUser = TokenUtils.getUserPrincipal()
					.orElse(this.jwtProperties.getDefaultUnauthenticatedUsername());

			try
			{
				String data = KafkaProducer.prepareData(operation.getOperationValue(), currentUser, this.kafkaTopic,
						table, dtoKafka);
				this.kafkaProducer.send(this.kafkaTopic, key.name(), data);
			}
			catch (JsonProcessingException var7)
			{
				throw new KafkaParseToJsonException(var7.getLocalizedMessage());
			}
		}
	}

	public String getKafkaTopic()
	{
		return this.kafkaTopic;
	}

	public KafkaProducer getKafkaProducer()
	{
		return this.kafkaProducer;
	}

	public JWTProperties getJwtProperties()
	{
		return this.jwtProperties;
	}
}
