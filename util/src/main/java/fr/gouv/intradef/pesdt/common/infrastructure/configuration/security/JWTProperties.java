package fr.gouv.intradef.pesdt.common.infrastructure.configuration.security;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Classe de configuration JWT (Java Web Token)
 */
@Component
public class JWTProperties
{
	@Value("${header.authentication.prefix:Bearer}")
	private String authenticationPrefix;
	@Value("${pesdt.unauthenticated.user.default.username:anonyme}")
	private String defaultUnauthenticatedUsername;
	@Value("${pesdt.unauthenticated.user.allowed:false}")
	private boolean defaultUnauthenticatedUserAllowed;
	@Value("${pesdt.custom.header.authentication.key:" +
			"#{T(fr.gouv.intradef.pesdt.common.api.CustomHeaders).X_PESDT_INTRADEF_AUTHORIZATION_NAME}}")
	private String authenticationKey;
	@Value("${pesdt.custom.header.authentication.shared-secrets.file-path:#{null}}")
	private String sharedSecretFilePath;
	@Value("${pesdt.custom.header.authentication.shared-secrets.username:technique}")
	private String sharedSecretUsername;

	public JWTProperties()
	{
	}

	public String getAuthenticationPrefix()
	{
		return this.authenticationPrefix;
	}

	public String getDefaultUnauthenticatedUsername()
	{
		return this.defaultUnauthenticatedUsername;
	}

	public boolean isDefaultUnauthenticatedUserAllowed()
	{
		return this.defaultUnauthenticatedUserAllowed;
	}

	public String getAuthenticationKey()
	{
		return this.authenticationKey;
	}

	public String getSharedSecretFilePath()
	{
		return this.sharedSecretFilePath;
	}

	public String getSharedSecretUsername()
	{
		return this.sharedSecretUsername;
	}
}
