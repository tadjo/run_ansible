package fr.gouv.intradef.pesdt.common.domain.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class BadSizePageableException extends RuntimeException
{
	private static final long serialVersionUID = -9118536223955186008L;

	public BadSizePageableException()
	{
		super("Page size must not be less than one!");
	}
}
