package fr.gouv.intradef.pesdt.common.domain.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.List;
import java.util.Objects;

/**
 * Exception utilisée dans le cas où la propriété de tri n'est pas connue
 */
@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class IDNotFoundPatchException extends RuntimeException
{

	private static final long serialVersionUID = -6729396227937640241L;

	public <T> IDNotFoundPatchException(List<T> idsNonTrouve)
	{
		super(String.format("Les ids suivants : '%s' ne sont pas trouvés",
				String.join(", ", (idsNonTrouve != null ? idsNonTrouve.stream().map(Objects::toString).toArray(String[]::new) : new String[0]))));
	}
}