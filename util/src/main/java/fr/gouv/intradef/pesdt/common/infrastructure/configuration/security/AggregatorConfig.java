package fr.gouv.intradef.pesdt.common.infrastructure.configuration.security;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * Classe de configuration pour les CorsUrls
 */
@Configuration
@ConfigurationProperties(
		prefix = "aggregator"
)
public class AggregatorConfig
{
	private List<String> corsUrls;

	public AggregatorConfig()
	{
	}

	public List<String> getCorsUrls()
	{
		return (List) Optional.ofNullable(this.corsUrls).orElse(Collections.emptyList());
	}

	public void setCorsUrls(List<String> corsUrls)
	{
		this.corsUrls = corsUrls;
	}
}

