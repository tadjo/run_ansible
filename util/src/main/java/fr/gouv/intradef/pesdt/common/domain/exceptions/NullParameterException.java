package fr.gouv.intradef.pesdt.common.domain.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class NullParameterException extends RuntimeException
{
	private static final long serialVersionUID = 2999776559313042486L;

	public NullParameterException()
	{
		super("The given parameter must not be null!");
	}
}
