package fr.gouv.intradef.pesdt.common.api;

public enum CustomHeaderLocation
{
	REQUEST,
	RESPONSE,
	BOTH
}
