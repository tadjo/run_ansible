package fr.gouv.intradef.pesdt.common.infrastructure.configuration.security;

import com.auth0.jwt.interfaces.RSAKeyProvider;

import java.io.ByteArrayInputStream;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * RSA Public/Private Key provider.
 */
public class KeycloakRSAPublicKeyProvider implements RSAKeyProvider
{
	public static final String NOT_PUBLIC_ERROR = "Only public key retrieval available";

	/**
	 * Clés publiques
	 */
	private final ConcurrentMap<String, String> publicKeys = new ConcurrentHashMap();
	/**
	 * Type de la clé
	 */
	private final String keyType;

	/**
	 * Constructeur
	 *
	 * @param keyType
	 * @param publicKeys
	 */
	public KeycloakRSAPublicKeyProvider(String keyType, Map<String, String> publicKeys)
	{
		this.keyType = keyType;
		this.publicKeys.putAll(publicKeys);
	}

	/**
	 * @param crtStr Certificat
	 * @return Clé publique associée
	 * @throws CertificateException
	 */
	public static String getPublicKeyFromCertificate(String crtStr) throws CertificateException
	{
		byte[] buffer = Base64.getDecoder().decode(crtStr.replaceAll("-----BEGIN CERTIFICATE-----", "")
				.replaceAll("-----END CERTIFICATE-----", ""));
		ByteArrayInputStream byteCertificate = new ByteArrayInputStream(buffer);
		Certificate certificate = CertificateFactory.getInstance("X.509").generateCertificate(byteCertificate);
		PublicKey pk = certificate.getPublicKey();
		return Base64.getEncoder().encodeToString(pk.getEncoded());
	}

	@Override
	public RSAPublicKey getPublicKeyById(String keyId)
	{
		try
		{
			byte[] byteKey = Base64.getDecoder().decode(this.publicKeys.get(keyId)
					.replaceAll("(?:^-----BEGIN PUBLIC KEY-----\\n)|(?:\\n-----END PUBLIC KEY-----\\n*$)|\\r|\\n",
							""));
			KeyFactory kf = KeyFactory.getInstance(this.keyType);
			X509EncodedKeySpec keySpecX509 = new X509EncodedKeySpec(byteKey);
			return (RSAPublicKey) kf.generatePublic(keySpecX509);
		}
		catch (NoSuchAlgorithmException | InvalidKeySpecException e)
		{
			throw new IllegalArgumentException("Unable to create RSA Public Key", e);
		}
	}

	@Override
	public RSAPrivateKey getPrivateKey()
	{
		throw new UnsupportedOperationException("Only public key retrieval available");
	}

	@Override
	public String getPrivateKeyId()
	{
		throw new UnsupportedOperationException("Only public key retrieval available");
	}
}
