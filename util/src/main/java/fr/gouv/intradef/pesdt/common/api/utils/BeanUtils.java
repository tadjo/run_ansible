package fr.gouv.intradef.pesdt.common.api.utils;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

@Component
public class BeanUtils implements ApplicationContextAware
{
	private static ApplicationContext context;

	public BeanUtils()
	{
	}

	public static <T> T getBean(Class<T> beanClass)
	{
		return isContextInitiated() ? context.getBean(beanClass) : null;
	}

	public static boolean isContextInitiated()
	{
		return context != null;
	}

	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException
	{
		context = applicationContext;
	}
}
