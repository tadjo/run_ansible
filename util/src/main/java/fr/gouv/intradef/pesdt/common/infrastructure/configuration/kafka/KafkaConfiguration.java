package fr.gouv.intradef.pesdt.common.infrastructure.configuration.kafka;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Stream;

@Configuration
@ConfigurationProperties(
		prefix = "kafka.aop"
)
public class KafkaConfiguration
{
	private static final String CREATE_ACTION = "create";
	private static final String READ_ACTION = "read";
	private static final String UPDATE_ACTION = "update";
	private static final String DELETE_ACTION = "delete";
	private static final Logger LOGGER = LoggerFactory.getLogger(KafkaConfiguration.class);
	private List<String> activeHandlers;

	public KafkaConfiguration()
	{
	}

	@PostConstruct
	public void postInit()
	{
		LOGGER.debug("Enable following Kafka handlers: {}", this.activeHandlers);
	}

	public List<String> getActiveHandlers()
	{
		return Optional.ofNullable(this.activeHandlers).orElse(Collections.emptyList());
	}

	public void setActiveHandlers(List<String> activeHandlers)
	{
		this.activeHandlers = activeHandlers;
	}

	private boolean handleAction(String action)
	{
		Stream<String> stream = this.getActiveHandlers().stream();
		Objects.requireNonNull(action);
		return stream.anyMatch(action::equalsIgnoreCase);
	}

	public boolean handleCreateAction()
	{
		return this.handleAction("create");
	}

	public boolean handleReadAction()
	{
		return this.handleAction("read");
	}

	public boolean handleUpdateAction()
	{
		return this.handleAction("update");
	}

	public boolean handleDeleteAction()
	{
		return this.handleAction("delete");
	}
}
