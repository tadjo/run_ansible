package fr.gouv.intradef.pesdt.common.infrastructure.adapter.secondary;

import fr.gouv.intradef.pesdt.common.api.utils.BeanUtils;
import fr.gouv.intradef.pesdt.common.domain.port.secondary.IKafka;
import fr.gouv.intradef.pesdt.common.infrastructure.adapter.jpa.entity.AbstractEntity;
import fr.gouv.intradef.pesdt.common.infrastructure.configuration.kafka.KafkaConfiguration;

import javax.persistence.*;
import java.util.Optional;

public class KafkaEntityListener
{
	private IKafka kafkaService;
	private KafkaConfiguration kafkaAOPConfiguration;

	public KafkaEntityListener()
	{
	}

	@PostLoad
	public void postLoad(AbstractEntity<?> abstractEntity)
	{
		if (this.instantiateBean() && this.kafkaAOPConfiguration.handleReadAction() && abstractEntity != null)
		{
			String tableName = this.getTableNameFromClass(abstractEntity.getClass());
			this.kafkaService.read(abstractEntity, tableName);
		}

	}

	@PostPersist
	public void postPersist(AbstractEntity<?> abstractEntity)
	{
		if (this.instantiateBean() && this.kafkaAOPConfiguration.handleCreateAction() && abstractEntity != null)
		{
			String tableName = this.getTableNameFromClass(abstractEntity.getClass());
			this.kafkaService.save(abstractEntity, tableName);
		}

	}

	@PostUpdate
	public void postUpdate(AbstractEntity<?> abstractEntity)
	{
		if (this.instantiateBean() && this.kafkaAOPConfiguration.handleUpdateAction() && abstractEntity != null)
		{
			String tableName = this.getTableNameFromClass(abstractEntity.getClass());
			this.kafkaService.update(abstractEntity, tableName);
		}

	}

	@PostRemove
	public void postRemove(AbstractEntity<?> abstractEntity)
	{
		if (this.instantiateBean() && this.kafkaAOPConfiguration.handleDeleteAction() && abstractEntity != null)
		{
			String tableName = this.getTableNameFromClass(abstractEntity.getClass());
			this.kafkaService.delete(abstractEntity, tableName);
		}

	}

	private <K extends AbstractEntity<?>> String getTableNameFromClass(Class<K> clazz)
	{
		Table tableAnnotation = clazz.getAnnotation(Table.class);
		return Optional.ofNullable(tableAnnotation).map(Table::name).orElse(null);
	}

	private boolean instantiateBean()
	{
		if (BeanUtils.isContextInitiated())
		{
			this.kafkaService = BeanUtils.getBean(KafkaAdapter.class);
			this.kafkaAOPConfiguration = BeanUtils.getBean(KafkaConfiguration.class);
			return true;
		}
		else
		{
			return false;
		}
	}
}
