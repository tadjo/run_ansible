package fr.gouv.intradef.pesdt.common.api.utils;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import java.io.IOException;
import java.time.LocalDateTime;

/**
 * Classe de désérialisation des dates
 */
public class CustomLocalDateTimeDeserializer extends StdDeserializer<LocalDateTime>
{
	private static final long serialVersionUID = 5199147440280061130L;

	/**
	 * Constructeur
	 */
	public CustomLocalDateTimeDeserializer()
	{
		super(LocalDateTime.class);
	}

	@Override
	public LocalDateTime deserialize(JsonParser parser, DeserializationContext context) throws IOException
	{
		return LocalDateTime.parse(parser.readValueAs(String.class));
	}
}
