package fr.gouv.intradef.pesdt.common.domain.exceptions;

import java.util.Optional;

import fr.gouv.intradef.pesdt.common.infrastructure.adapter.jpa.entity.AbstractEntity;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class AlreadyExistsException extends RuntimeException
{
	private static final long serialVersionUID = -4088371178738337896L;

	public <T> AlreadyExistsException(T id, Class<? extends AbstractEntity<T>> clazz)
	{
		super(String.format("L'entité du type %s existe déjà avec cet id %s", clazz.getSimpleName(), Optional.ofNullable(id).map(Object::toString).orElse("")));
	}

}
