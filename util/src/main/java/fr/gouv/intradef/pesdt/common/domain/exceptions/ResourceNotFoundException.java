package fr.gouv.intradef.pesdt.common.domain.exceptions;

import java.util.Objects;
import java.util.Optional;

import fr.gouv.intradef.pesdt.common.infrastructure.adapter.jpa.entity.AbstractEntity;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class ResourceNotFoundException extends RuntimeException
{
	private static final long serialVersionUID = 5436718445365422152L;

	public <T> ResourceNotFoundException(T id, Class<? extends AbstractEntity<T>> clazz)
	{
		super(String.format("Pas d'entité de type '%s' avec l'id '%s' !", clazz.getSimpleName().replace("Entity", ""), Optional.ofNullable(id).map(Objects::toString).orElse("null")));
	}

}