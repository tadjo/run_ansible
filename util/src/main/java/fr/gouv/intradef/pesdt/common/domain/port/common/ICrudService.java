/**
 * Interface pour les services d'écriture
 */

package fr.gouv.intradef.pesdt.common.domain.port.common;

import java.util.List;

public interface ICrudService<D, K>
{
	D save(D entity);

	List<D> saveAll(List<D> entities);

	D update(D entity);

	D deleteById(K id);
}
