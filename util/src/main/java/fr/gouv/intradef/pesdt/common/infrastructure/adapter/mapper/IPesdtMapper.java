package fr.gouv.intradef.pesdt.common.infrastructure.adapter.mapper;


import java.util.List;

/**
 * Interface Mapper, qui permet de définir au minimum les méthodes de conversion d'une entité vers un DTO et vice versa.
 *
 * @param <ENTITY>
 * @param <DTO>
 */
public interface IPesdtMapper<ENTITY, DTO>
{

	/**
	 * Mappe un objet dto en un objet entity
	 *
	 * @param dto source à mapper
	 * @return objet mappé
	 */
	ENTITY toEntity(DTO dto);

	/**
	 * Mappe un objet entity en un objet dto
	 *
	 * @param entity source à mapper
	 * @return objet mappé
	 */
	DTO toDto(ENTITY entity);

	/**
	 * Mappe une liste de d'entités en une liste de dtos
	 *
	 * @param entities liste à mapper
	 * @return liste mappée
	 */
	List<DTO> toDtos(List<ENTITY> entities);

	/**
	 * Mappe une liste de dtos en une liste d'entités
	 *
	 * @param dtos liste à mapper
	 * @return liste mappée
	 */
	List<ENTITY> toEntities(List<DTO> dtos);
}