package fr.gouv.intradef.pesdt.common.infrastructure.configuration.swagger;

import java.util.ArrayList;
import java.util.List;

/**
 * Classe contenant les informations de l'Api (apiName, buildVersion, buildTime, startUpTime, apiVersions,
 * swaggerVersion, serverInfos)
 */
public class ApiInfos
{
	private final String apiName;
	private final String buildVersion;
	private final String buildTime;
	private final String startUpTime;
	private final List<String> apiVersions;
	private final String swaggerVersion;
	private final ServerInfos serverInfos;

	ApiInfos(String apiName, String buildVersion, String buildTime, String startUpTime, List<String> apiVersions,
			 String swaggerVersion, ServerInfos serverInfos)
	{
		this.apiName = apiName;
		this.buildVersion = buildVersion;
		this.buildTime = buildTime;
		this.startUpTime = startUpTime;
		this.apiVersions = apiVersions;
		this.swaggerVersion = swaggerVersion;
		this.serverInfos = serverInfos;
	}

	private static List<String> defaultApiVersions()
	{
		return new ArrayList();
	}

	public static ApiInfosBuilder builder()
	{
		return new ApiInfosBuilder();
	}

	public String getApiName()
	{
		return this.apiName;
	}

	public String getBuildVersion()
	{
		return this.buildVersion;
	}

	public String getBuildTime()
	{
		return this.buildTime;
	}

	public String getStartUpTime()
	{
		return this.startUpTime;
	}

	public List<String> getApiVersions()
	{
		return this.apiVersions;
	}

	public String getSwaggerVersion()
	{
		return this.swaggerVersion;
	}

	public ServerInfos getServerInfos()
	{
		return this.serverInfos;
	}

	public static class ApiInfosBuilder
	{
		private String apiName;
		private String buildVersion;
		private String buildTime;
		private String startUpTime;
		private boolean apiVersionsSet;
		private List<String> apiVersionsValue;
		private String swaggerVersion;
		private ServerInfos serverInfos;

		ApiInfosBuilder()
		{
		}

		public ApiInfosBuilder apiName(String apiName)
		{
			this.apiName = apiName;
			return this;
		}

		public ApiInfosBuilder buildVersion(String buildVersion)
		{
			this.buildVersion = buildVersion;
			return this;
		}

		public ApiInfosBuilder buildTime(String buildTime)
		{
			this.buildTime = buildTime;
			return this;
		}

		public ApiInfosBuilder startUpTime(String startUpTime)
		{
			this.startUpTime = startUpTime;
			return this;
		}

		public ApiInfosBuilder apiVersions(List<String> apiVersions)
		{
			this.apiVersionsValue = apiVersions;
			this.apiVersionsSet = true;
			return this;
		}

		public ApiInfosBuilder swaggerVersion(String swaggerVersion)
		{
			this.swaggerVersion = swaggerVersion;
			return this;
		}

		public ApiInfosBuilder serverInfos(ServerInfos serverInfos)
		{
			this.serverInfos = serverInfos;
			return this;
		}

		public ApiInfos build()
		{
			List<String> apiVersionsValue = this.apiVersionsValue;
			if (!this.apiVersionsSet)
			{
				apiVersionsValue = ApiInfos.defaultApiVersions();
			}

			return new ApiInfos(this.apiName, this.buildVersion, this.buildTime, this.startUpTime, apiVersionsValue,
					this.swaggerVersion, this.serverInfos);
		}

		public String toString()
		{
			return "ApiInfos.ApiInfosBuilder(apiName=" + this.apiName + ", buildVersion=" + this.buildVersion
					+ ", buildTime=" + this.buildTime + ", startUpTime=" + this.startUpTime + ", apiVersionsValue="
					+ this.apiVersionsValue + ", swaggerVersion=" + this.swaggerVersion + ", serverInfos="
					+ this.serverInfos + ")";
		}
	}
}
