package fr.gouv.intradef.pesdt.common.domain.port.secondary;

/**
 * Interface CRUD
 */
public interface IKafka
{
	/**
	 * Sauvegarde
	 *
	 * @param toSave objet à sauvegarder
	 * @param var2
	 */
	void save(Object toSave, String var2);

	/**
	 * Lecture
	 *
	 * @param var1
	 * @param var2
	 */
	void read(Object var1, String var2);

	/**
	 * Mise à jour
	 *
	 * @param var1
	 * @param var2
	 */
	void update(Object var1, String var2);

	/**
	 * Suppression
	 *
	 * @param var1
	 * @param var2
	 */
	void delete(Object var1, String var2);
}
