package fr.gouv.intradef.pesdt.common.domain.port.common;

import fr.gouv.intradef.pesdt.common.api.dto.AbstractDto;
import fr.gouv.intradef.pesdt.common.domain.exceptions.AlreadyExistsPatchException;
import fr.gouv.intradef.pesdt.common.domain.exceptions.IDNotFoundPatchException;
import fr.gouv.intradef.pesdt.common.domain.exceptions.LowerVersionPatchException;
import fr.gouv.intradef.pesdt.common.domain.utils.AssertionUtils;
import fr.gouv.intradef.pesdt.common.domain.utils.DeltaList;
import fr.gouv.intradef.pesdt.common.domain.utils.DeltaListBuilder;
import fr.gouv.intradef.pesdt.common.infrastructure.adapter.jpa.entity.AbstractEntity;
import fr.gouv.intradef.pesdt.common.infrastructure.adapter.mapper.IPesdtMapper;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Classe utilitaire qui implémente les opérations classiques avec des listes
 */
public class CrudServiceUtils
{
	private CrudServiceUtils()
	{
	}

	/**
	 * Sauvegarde une liste de dtos
	 *
	 * @param dtos       DTOs à sauvegarder
	 * @param repository Repository correspondant aux DTOs
	 * @param mapper     Mapper correspondant aux DTOs
	 * @param <K>        Type de la clé utilisé Long la plupart du temps
	 * @param <D>        Type du dto
	 * @param <E>        Type de l'entité lié avec le DTO
	 * @return La liste des DTOs sauvegardés
	 * @throws AlreadyExistsPatchException En cas d'id qui serait déjà présent en base de données, rien n'est envoyé à la base dans ce cas.
	 */
	public static <K, D extends AbstractDto<K>,
			E extends AbstractEntity<K>> List<D> saveAll(List<D> dtos,
														 JpaRepository<E, K> repository, IPesdtMapper<E, D> mapper)
	{
		AssertionUtils.assertNotNull(dtos);

		if (!dtos.isEmpty())
		{
			// On vérifie que les entités à sauver n'ont pas déjà un id et le cas échéant, s'il existe en base
			Set<K> idsToSave = dtos.stream().map(D::getId).collect(Collectors.toSet());
			List<E> result = repository.findAllById(idsToSave);
			if (!result.isEmpty())
			{
				Class<E> entityClass = (Class<E>) result.get(0).getClass();
				throw new AlreadyExistsPatchException(result.stream().map(AbstractEntity::getId).collect(Collectors.toList()), entityClass);
			}
		}

		List<E> toSave = mapper.toEntities(dtos);
		return mapper.toDtos(repository.saveAll(toSave));
	}

	/**
	 * Met à jour une liste de dtos
	 *
	 * @param dtos       DTOs à mettre à jour
	 * @param repository Repository correspondant aux DTOs
	 * @param mapper     Mapper correspondant aux DTOs
	 * @param <K>        Type de la clé utilisé Long la plupart du temps
	 * @param <D>        Type du dto
	 * @param <E>        Type de l'entité lié avec le DTO
	 * @return La liste des DTOs mis à jour
	 * @throws IDNotFoundPatchException   Si au moins 1 id n'est pas retrouvé en base
	 * @throws LowerVersionPatchException Si au moins 1 version fournie est différente de celle trouvée en base
	 */
	public static <K, D extends AbstractDto<K>, E extends AbstractEntity<K>> List<D> updateAll(List<D> dtos,
																							   JpaRepository<E, K> repository, IPesdtMapper<E, D> mapper)
	{
		return updateAll(dtos, repository, mapper, null);
	}

	/**
	 * @param dtos        DTOs à mettre à jour
	 * @param repository  Repository correspondant aux DTOs
	 * @param mapper      Mapper correspondant aux DTOs
	 * @param postProcess Callback optionnel qui permet une action avant de convertir les entités à jour en DTO
	 * @param <K>         Type de la clé utilisé Long la plupart du temps
	 * @param <D>         Type du dto
	 * @param <E>         Type de l'entité
	 * @return la liste des DTOs mis à jour
	 * @throws IDNotFoundPatchException   Si au moins 1 id n'est pas retrouvé en base
	 * @throws LowerVersionPatchException Si au moins 1 version fournie est différente de celle trouvée en base
	 */
	public static <K, D extends AbstractDto<K>, E extends AbstractEntity<K>> List<D> updateAll(
			List<D> dtos,
			JpaRepository<E, K> repository, IPesdtMapper<E, D> mapper, Consumer<List<E>> postProcess,
			List<E> entitiesResult)
	{
		AssertionUtils.assertNotNull(dtos);

		if (!dtos.isEmpty() && !entitiesResult.isEmpty())
		{
			// On vérifie ensuite que les versions de tous les objets soient cohérentes (concurrence)
			Map<K, E> resultMap = entitiesResult.stream().collect(Collectors.toMap(AbstractEntity::getId, Function.identity()));
			List<D> entityVersionError = dtos.stream().filter(dto -> {
				E entity = resultMap.get(dto.getId());
				return !entity.getVersion().equals(dto.getVersion());
			}).collect(Collectors.toList());
			if (entityVersionError.stream().findAny().isPresent())
			{
				Class<E> entityClass = (Class<E>) entitiesResult.get(0).getClass();
				throw new LowerVersionPatchException(
						entityVersionError.stream().map(elt -> resultMap.get(elt.getId()).getVersion()).collect(Collectors.toList()),
						entityVersionError.stream().map(AbstractDto::getVersion).collect(Collectors.toList()), entityClass);
			}
			else
			{
				// On peut mettre à jour, tout est OK
				dtos.forEach(entity -> entity.setVersion(entity.getVersion() + 1));
				List<E> toSave = mapper.toEntities(dtos);
				if (postProcess != null)
				{
					postProcess.accept(toSave);
				}
				return mapper.toDtos(repository.saveAll(toSave));
			}
		}
		else
		{
			return dtos;
		}
	}

	/**
	 * @param <K> Type de l'id de l'entity
	 * @param <D> Dto
	 * @param <E> Entity
	 */
	public static <K, D extends AbstractDto<K>, E extends AbstractEntity<K>> List<D> updateAll(
			List<D> dtos,
			JpaRepository<E, K> repository, IPesdtMapper<E, D> mapper, Consumer<List<E>> postProcess)
	{
		AssertionUtils.assertNotNull(dtos);

		if (!dtos.isEmpty())
		{
			// On vérifie que toutes les entités à maj existent dans la base
			Set<K> idsToSave = dtos.stream().map(D::getId).collect(Collectors.toSet());
			List<E> entitiesResult = repository.findAllById(idsToSave);
			checkOnlyCommonElements(dtos, entitiesResult);
			return updateAll(dtos, repository, mapper, postProcess, entitiesResult);
		}
		else
		{
			return dtos;
		}
	}

	/**
	 * Vérifie si les deux listes passées en paramètre n'ont que des éléments en commun. Si au moins un élément est détecté comme différent, cette
	 * méthode renverra une exception {@link IDNotFoundPatchException}
	 *
	 * @param <K> Type de l'id de l'entity
	 * @param <D> Dto
	 * @param <E> Entity
	 */
	public static <K, D extends AbstractDto<K>, E extends AbstractEntity<K>> DeltaList<E, D> checkOnlyCommonElements(
			List<D> dtos, List<E> entitiesResult) throws IDNotFoundPatchException
	{
		DeltaList<E, D> deltaList = DeltaListBuilder.buildDeltaList(dtos, entitiesResult);
		if (!CollectionUtils.isEmpty(deltaList.newElements))
		{
			throw new IDNotFoundPatchException(deltaList.newElements);
		}
		if (!CollectionUtils.isEmpty(deltaList.oldElements))
		{
			throw new IDNotFoundPatchException(deltaList.oldElements);
		}
		return deltaList;
	}

	/**
	 * @param <K> Type de l'id de l'entity
	 * @param <D> Dto
	 * @param <E> Entity
	 */
	public static <K, D extends AbstractDto<K>, E extends AbstractEntity<K>> List<D> deleteByIds(List<K> ids,
																								 JpaRepository<E, K> repository, IPesdtMapper<E, D> mapper)
	{
		AssertionUtils.assertNotNull(ids);
		List<E> foundActivites = repository.findAllById(ids);
		if ((long) foundActivites.size() != (long) ids.size())
		{
			List<E> entitiesNotFound = repository.findAllById(ids);

			List<K> bddIds = entitiesNotFound.stream().map(AbstractEntity::getId).collect(Collectors.toList());
			DeltaList<K, K> deltaList = DeltaListBuilder.buildDeltaList(ids, bddIds, Function.identity(), Object::equals);

			throw new IDNotFoundPatchException(deltaList.newElements);
		}
		repository.deleteAll(foundActivites);
		return mapper.toDtos(foundActivites);
	}
}
