package fr.gouv.intradef.pesdt.common.domain.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class CustomiserException extends RuntimeException
{
	public CustomiserException(String message)
	{
		super(message);
	}

	public CustomiserException(String message, Throwable t)
	{
		super(message, t);
	}
}
