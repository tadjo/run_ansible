package fr.gouv.intradef.pesdt.common.domain.utils;

import fr.gouv.intradef.pesdt.common.api.dto.AbstractDto;
import fr.gouv.intradef.pesdt.common.infrastructure.adapter.jpa.entity.AbstractEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.BiPredicate;
import java.util.function.Function;

public class DeltaListBuilder
{

	private DeltaListBuilder()
	{
		throw new IllegalStateException("Utilisity Class");
	}

	/**
	 * Fonction qui permet de comparer les éléments d'une liste par rapport à une autre liste.<br>
	 * L'élément retourné est composé de trois listes
	 * <ul>
	 * <li>{@link DeltaList#oldElements}: Elements qui se trouvent dans la liste de la bdd mais pas dans la nouvelle liste</li>
	 * <li>{@link DeltaList#newElements}: Elements qui se trouvent dans la nouvelle liste mais pas dans la liste de la bdd</li>
	 * <li>{@link DeltaList#commonNewElements}: Elements qui se trouvent dans les deux listes, de type NEW</li>
	 * <li>{@link DeltaList#commonOldElements}: Elements qui se trouvent dans les deux listes, de type OLD</li>
	 * </ul>
	 * Les deux listes {@link DeltaList#commonOldElements} et {@link DeltaList#commonNewElements} contiennes des éléments équivalents.
	 * Le cas d'usage typique est un usage pour déterminer les éléments qui sont à envoyer en suppression à hibernate, ceux à envoyer en pertistance
	 * et ceux à envoyer en save.<br>
	 * La méthode de comparaison d'un élément avec un autre est l'id, si l'id est equals à un autre id, alors les éléments comparés seront considérés
	 * comme identiques.
	 *
	 * @param DTOElements    Nouvelle liste à comparer avec l'ancienne liste, qui permet de déterminer les élements old, common et new
	 * @param ENTITYElements Liste des anciens élements, correspond aux anciens éléments de la nouvelle liste
	 * @param <ENTITY>       Dto
	 * @param <DTO>          Entité
	 * @return Les trois listes de comparaison
	 */
	public static <DTO extends AbstractDto<?>, ENTITY extends AbstractEntity<?>> DeltaList<ENTITY, DTO> buildDeltaList(List<DTO> DTOElements,
																													   List<ENTITY> ENTITYElements)
	{
		return buildDeltaList(DTOElements, ENTITYElements, AbstractDto::getId, (a, b) -> a.getId().equals(b.getId()));
	}

	public static <DTO extends AbstractDto<?>> DeltaList<DTO, DTO> buildDeltaListDTOs(List<DTO> DTOElements,
																					  List<DTO> oldElements)
	{
		return buildDeltaList(DTOElements, oldElements, AbstractDto::getId, (a, b) -> a.getId().equals(b.getId()));
	}

	/**
	 * Fonction qui permet de comparer les éléments d'une liste par rapport à une autre liste.<br>
	 * L'élément retourné est composé de trois listes
	 * <ul>
	 * <li>{@link DeltaList#oldElements}: Elements qui se trouvent dans la liste de la bdd mais pas dans la nouvelle liste</li>
	 * <li>{@link DeltaList#newElements}: Elements qui se trouvent dans la nouvelle liste mais pas dans la liste de la bdd</li>
	 * <li>{@link DeltaList#commonNewElements}: Elements qui se trouvent dans les deux listes, de type NEW</li>
	 * <li>{@link DeltaList#commonOldElements}: Elements qui se trouvent dans les deux listes, de type OLD</li>
	 * </ul>
	 * Les deux listes {@link DeltaList#commonOldElements} et {@link DeltaList#commonNewElements} contiennes des éléments équivalents.
	 * Le cas d'usage typique est un usage pour déterminer les éléments qui sont à envoyer en suppression à hibernate, ceux à envoyer en pertistance
	 * et ceux à envoyer en save.<br>
	 *
	 * @param newElements     Nouvelle liste à comparer avec l'ancienne liste, qui permet de déterminer les élements old, common et new
	 * @param oldElements     Liste des anciens élements, correspond aux anciens éléments de la nouvelle liste
	 * @param getKey          Getter pour la clé de l'élément, utilisé uniquement pour savoir si la clé est null ou non
	 * @param compareElements Fonction de comparaison de deux éléments
	 * @param <OLD>           Type old de la liste à comparer
	 * @param <NEW>           Type new de la liste à comparer
	 * @return Les trois listes de comparaison
	 */
	public static <OLD, NEW> DeltaList<OLD, NEW> buildDeltaList(List<NEW> newElements, List<OLD> oldElements, Function<NEW, ?> getKey,
																BiPredicate<NEW, OLD> compareElements)
	{
		DeltaList<OLD, NEW> result = new DeltaList<>();
		result.oldElements = new ArrayList<>();
		result.newElements = new ArrayList<>();
		result.commonNewElements = new ArrayList<>();
		result.commonOldElements = new ArrayList<>();
		// On parcours d'abord la liste des éléments déjà existants pour détemriner lesquels sont à supprimer et à modifier
		oldElements.forEach(oldElement -> {
			Optional<NEW> newElement = newElements.stream().filter(a -> getKey.apply(a) != null && compareElements.test(a, oldElement)).findFirst();
			if (newElement.isPresent())
			{
				result.commonNewElements.add(newElement.get());
				result.commonOldElements.add(oldElement);
			}
			else
			{
				result.oldElements.add(oldElement);
			}
		});

		// On parcours ensuite la liste des éléments à créer pour déterminer lequels sont à créer et à modifier
		newElements.forEach(newElement -> {
			if (getKey.apply(newElement) != null)
			{
				if (oldElements.stream().noneMatch(a -> compareElements.test(newElement, a)))
				{
					result.newElements.add(newElement);
				}
			}
			else
			{
				// Il n'y a pas d'id, c'est que c'est un nouvel élément
				result.newElements.add(newElement);
			}
		});
		return result;
	}
}