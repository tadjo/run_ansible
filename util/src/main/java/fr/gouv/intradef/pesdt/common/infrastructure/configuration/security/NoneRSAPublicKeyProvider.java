package fr.gouv.intradef.pesdt.common.infrastructure.configuration.security;

import com.auth0.jwt.interfaces.RSAKeyProvider;

import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;

/**
 * Classe correspondant à l'absence de provider
 */
public class NoneRSAPublicKeyProvider implements RSAKeyProvider
{
	/**
	 * Constructeur
	 */
	public NoneRSAPublicKeyProvider()
	{
	}

	@Override
	public RSAPublicKey getPublicKeyById(String keyId)
	{
		return null;
	}

	@Override
	public RSAPrivateKey getPrivateKey()
	{
		return null;
	}

	@Override
	public String getPrivateKeyId()
	{
		return null;
	}
}
