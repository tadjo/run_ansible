package fr.gouv.intradef.pesdt.common.domain.exceptions;

import fr.gouv.intradef.pesdt.common.infrastructure.adapter.jpa.entity.AbstractEntity;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.List;
import java.util.Objects;

/**
 * Exception utilisée dans le cas où la version demandée n'est pas la dernière
 */
@ResponseStatus(value = HttpStatus.PRECONDITION_REQUIRED)
public class LowerVersionPatchException extends RuntimeException
{

	private static final long serialVersionUID = -6729396227937640241L;

	/**
	 * HTTP : 428 = Precondition required retourné si on tente une mise à jour d'un objet avec une version antérieure
	 *
	 * @param tried  version de l'objet que l'on souhaite modifier
	 * @param actual version la plus récente de l'objet
	 * @param clazz  Entité
	 * @param <T>    Type d'ID
	 */
	public <T> LowerVersionPatchException(final List<Long> tried, final List<Long> actual, final Class<? extends AbstractEntity<T>> clazz)
	{
		super(String.format("La version des objets l'objet de type '%s' ne sont pas les dernières : %s < %s",
				clazz.getSimpleName().replace("Entity", StringUtils.EMPTY),
				tried.stream().map(Objects::toString).reduce("", (sum, elt) -> sum + ", " + elt),
				actual.stream().map(Objects::toString).reduce("", (sum, elt) -> sum + ", " + elt)));
	}
}