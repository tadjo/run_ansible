package fr.gouv.intradef.pesdt.common.domain.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NO_CONTENT)
public class NoContentException extends RuntimeException
{
	private static final long serialVersionUID = -5651870259640660821L;

	public NoContentException(String message)
	{
		super(message);
	}
}
