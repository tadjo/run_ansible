/**
 * Enveloppe de réponse générique PESDT des services find, save, update et delete
 */
package fr.gouv.intradef.pesdt.common.api.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import io.swagger.v3.oas.annotations.media.Schema;

import java.util.List;

@JsonInclude(Include.NON_NULL)
@Schema(
		description = "Enveloppe de réponse générique PESDT"
)
public class PesdtBody<T>
{
	//    public static final String DESC = "Enveloppe de réponse";
	@Schema(
			description = "Objet(s) de retour"
	)
	private T data;
	@Schema(
			description = "Liste d'erreurs applicatives"
	)
	private List<PesdtError> errors;
	@Schema(
			description = "Informations complémentaires"
	)
	private PesdtMetadata metadata;

	public PesdtBody()
	{
	}

	public PesdtBody(T data)
	{
		this.data = data;
	}

	public PesdtBody(T data, List<PesdtError> errors, PesdtMetadata metadata)
	{
		this.data = data;
		this.errors = errors;
		this.metadata = metadata;
	}

	public static <T> PesdtBodyBuilder<T> builder()
	{
		return new PesdtBodyBuilder();
	}

	public T getData()
	{
		return this.data;
	}

	public void setData(T data)
	{
		this.data = data;
	}

	public List<PesdtError> getErrors()
	{
		return this.errors;
	}

	public void setErrors(List<PesdtError> errors)
	{
		this.errors = errors;
	}

	public PesdtMetadata getMetadata()
	{
		return this.metadata;
	}

	public void setMetadata(PesdtMetadata metadata)
	{
		this.metadata = metadata;
	}

	public String toString()
	{
		return "PesdtBody(data=" + this.getData() + ", errors=" + this.getErrors() + ", metadata=" + this.getMetadata() + ")";
	}

	/**
	 * Builder de PesdtBody
	 *
	 * @param <T>
	 */
	public static class PesdtBodyBuilder<T>
	{
		private T data;
		private List<PesdtError> errors;
		private PesdtMetadata metadata;

		PesdtBodyBuilder()
		{
		}

		public PesdtBodyBuilder<T> data(T data)
		{
			this.data = data;
			return this;
		}

		public PesdtBodyBuilder<T> errors(List<PesdtError> errors)
		{
			this.errors = errors;
			return this;
		}

		public PesdtBodyBuilder<T> metadata(PesdtMetadata metadata)
		{
			this.metadata = metadata;
			return this;
		}

		public PesdtBody<T> build()
		{
			return new PesdtBody(this.data, this.errors, this.metadata);
		}

		public String toString()
		{
			return "PesdtBody.PesdtBodyBuilder(data=" + this.data + ", errors=" + this.errors + ", metadata=" + this.metadata + ")";
		}
	}
}