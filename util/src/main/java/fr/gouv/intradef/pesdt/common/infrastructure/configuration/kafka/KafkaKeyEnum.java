package fr.gouv.intradef.pesdt.common.infrastructure.configuration.kafka;

public enum KafkaKeyEnum
{
	MODIFICATION,
	LECTURE,
	ENVOI
}
