package fr.gouv.intradef.pesdt.common.infrastructure.configuration.kafka;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import java.io.Serializable;

public class KafkaMessageCreateDto implements Serializable
{
	private String operation;
	private String utilisateur;
	private String date;
	private String service;
	private String table;
	@JsonInclude(Include.NON_NULL)
	private transient Object donnee;

	public KafkaMessageCreateDto()
	{
	}

	public KafkaMessageCreateDto(String operation, String utilisateur, String date, String service, String table, Object donnee)
	{
		this.operation = operation;
		this.utilisateur = utilisateur;
		this.date = date;
		this.service = service;
		this.table = table;
		this.donnee = donnee;
	}

	public String getOperation()
	{
		return this.operation;
	}

	public void setOperation(String operation)
	{
		this.operation = operation;
	}

	public String getUtilisateur()
	{
		return this.utilisateur;
	}

	public void setUtilisateur(String utilisateur)
	{
		this.utilisateur = utilisateur;
	}

	public String getDate()
	{
		return this.date;
	}

	public void setDate(String date)
	{
		this.date = date;
	}

	public String getService()
	{
		return this.service;
	}

	public void setService(String service)
	{
		this.service = service;
	}

	public String getTable()
	{
		return this.table;
	}

	public void setTable(String table)
	{
		this.table = table;
	}

	public Object getDonnee()
	{
		return this.donnee;
	}

	public void setDonnee(Object donnee)
	{
		this.donnee = donnee;
	}
}
