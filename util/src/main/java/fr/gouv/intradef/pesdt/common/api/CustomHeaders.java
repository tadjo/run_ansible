package fr.gouv.intradef.pesdt.common.api;

import io.swagger.v3.oas.models.media.BooleanSchema;
import io.swagger.v3.oas.models.media.IntegerSchema;
import io.swagger.v3.oas.models.media.Schema;
import io.swagger.v3.oas.models.media.StringSchema;

import java.util.Arrays;

public enum CustomHeaders
{
	X_PESDT_INTRADEF_AUTHORIZATION("x-pesdt-intradef-authorization", CustomHeaderLocation.REQUEST,
			false, new StringSchema(), "Le token applicatif MindefConnect de l'utilisateur."),
	X_DATA_ACCESS_NETWORK("x-data-access-network", CustomHeaderLocation.RESPONSE, false,
			(new StringSchema())._default("intradef"),
			"permet de décrire les contraintes d'accès (globales, liées au chemin, liées à l'opération)"
					+ " &#58; à titre d'exemple pour les PEM de la passerelle API Internet-Intradef " +
					"&#58; &#34;helissng,intradef&#34; (accessible de HELISS NG et Intradef), &#34;helissng&#34; "
					+ "seulement, &#34;intradef&#34; seulement ... Par défaut, l'accès sera limité au réseau hébergeant "
					+ "les serveurs."),
	X_DATA_ACCESS_AUTHORIZATION("x-data-access-authorization", CustomHeaderLocation.BOTH, false, (new StringSchema())._default("nécessitant une autorisation du fournisseur API"), "&#34;publique&#34;, ou &#34;nécessitant une autorisation du fournisseur API&#34;. Par défaut &#34;nécessitant une autorisation du fournisseur API&#34;."),
	X_DATA_SECURITY_CLASSIFICATION("x-data-security-classification", CustomHeaderLocation.BOTH, false, (new StringSchema())._default("dr")._enum(Arrays.asList("dr", "np")), "permet de décrire le niveau de confidentialité des données échangées  (obligatoire même en intra-domaine) &#58; &#34;np&#34;, &#34;dr&#34;. Par défaut, le niveau du réseau support."),
	X_DATA_SECURITY_MENTION("x-data-security-mention", CustomHeaderLocation.BOTH, false, (new StringSchema())._default("personnel"), "permet de décrire les mentions de manipulation des données (globales, liées au chemin, liées à l'opération) &#58; &#34;aucune&#34;, &#34;medical&#34;, &#34;personnel&#34;, &#34;protection personnel&#34;, &#34;concours&#34;, &#34;industrie&#34;, &#34;technologie&#34;, &#34;commercial&#34; (cf Instruction ministérielle 900)"),
	X_DATA_USE_CONSTRAINT("x-data-use-constraint", CustomHeaderLocation.RESPONSE, false, (new StringSchema())._enum(Arrays.asList("aucune", "dpcs", "rgpd"))._default("rgpd"), "permet de décrire le type de contrainte liée aux données manipulées &#58; &#34;aucune&#34;, &#34;dpcs&#34;, &#34;rgpd&#34;"),
	X_DATA_IS_FILE("x-data-is-file", CustomHeaderLocation.RESPONSE, false, (new BooleanSchema())._default(Boolean.FALSE), "à utiliser dans l'Operation Object. Si présent, indique quel paramètre ou résultat peut être considéré comme étant un fichier, ou une partie d'un fichier."),
	X_MAXIMUM_REQUEST_RATE("x-maximum-request-rate", CustomHeaderLocation.RESPONSE, false, (new IntegerSchema()).format("int64"), "nombre maximum de requêtes par seconde que le fournisseur est techniquement capable d'assurer à l'ensemble de ses consommateurs."),
	X_MAXIMUM_REQUEST_BANDWIDTH("x-maximum-request-bandwidth", CustomHeaderLocation.RESPONSE, false, (new IntegerSchema()).format("int64"), "bande passante maximum par seconde alloué à l'ensemble des consommateurs."),
	X_MAXIMUM_REQUEST_SIZE("x-maximum-request-size", CustomHeaderLocation.RESPONSE, false, (new IntegerSchema()).format("int64"), "en Ko. Taille maximum d'une requête autorisée par le fournisseur d'API."),
	X_MAXIMUM_RESPONSE_SIZE("x-maximum-response-size", CustomHeaderLocation.RESPONSE, false, (new IntegerSchema()).format("int64"), "en Ko. Taille maximum de la réponse du fournisseur d'API à une requête."),
	X_MOCK_API("x-mock-API", CustomHeaderLocation.RESPONSE, false, (new BooleanSchema())._default(Boolean.FALSE), "booléen qui si true (false par défaut) indique que le serveur héberge une implémentation partielle de l'API destinée à la simulation et aux tests.."),
	X_SOURCE("x-source", CustomHeaderLocation.REQUEST, false, (new StringSchema())._default("intradef"), "permet de préciser le réseau hébergeant le fournisseur d'API &#58; &#34;intradef&#34;, &#34;helissng&#34;, ...");

	public static final String X_DATA_ACCESS_AUTHORIZATION_NAME = "x-data-access-authorization";
	public static final String X_DATA_ACCESS_AUTHORIZATION_DEFAULT = "nécessitant une autorisation du fournisseur API";
	public static final String X_DATA_ACCESS_NETWORK_NAME = "x-data-access-network";
	public static final String X_DATA_ACCESS_NETWORK_DEFAULT = "intradef";
	public static final String X_DATA_IS_FILE_NAME = "x-data-is-file";
	public static final String X_DATA_SECURITY_MENTION_NAME = "x-data-security-mention";
	public static final String X_DATA_SECURITY_MENTION_DEFAULT = "personnel";
	public static final String X_DATA_SECURITY_CLASSIFICATION_NAME = "x-data-security-classification";
	public static final String X_DATA_SECURITY_CLASSIFICATION_DEFAULT = "dr";
	public static final String X_DATA_USE_CONSTRAINT_NAME = "x-data-use-constraint";
	public static final String X_DATA_USE_CONSTRAINT_DEFAULT = "rgpd";
	public static final String X_MAXIMUM_REQUEST_BANDWIDTH_NAME = "x-maximum-request-bandwidth";
	public static final String X_MAXIMUM_REQUEST_RATE_NAME = "x-maximum-request-rate";
	public static final String X_MAXIMUM_REQUEST_SIZE_NAME = "x-maximum-request-size";
	public static final String X_MAXIMUM_RESPONSE_SIZE_NAME = "x-maximum-response-size";
	public static final String X_MOCK_API_NAME = "x-mock-API";
	public static final String X_PESDT_INTRADEF_AUTHORIZATION_NAME = "x-pesdt-intradef-authorization";
	public static final String X_SOURCE_NAME = "x-source";
	public static final String X_SOURCE_DEFAULT = "intradef";
	public static final String DEFAULT_EMPTY_VALUE = "aucune";
	public static final String INT_64_FORMAT = "int64";
	private final String name;
	private final CustomHeaderLocation location;
	private final boolean required;
	private final Schema<?> type;
	private final String description;

	private CustomHeaders(String name, CustomHeaderLocation location, boolean required, Schema type, String description)
	{
		this.name = name;
		this.location = location;
		this.required = required;
		this.type = type;
		this.description = description;
	}

	public String getName()
	{
		return this.name;
	}

	public CustomHeaderLocation getLocation()
	{
		return this.location;
	}

	public boolean isRequired()
	{
		return this.required;
	}

	public Schema<?> getType()
	{
		return this.type;
	}

	public String getDescription()
	{
		return this.description;
	}
}
