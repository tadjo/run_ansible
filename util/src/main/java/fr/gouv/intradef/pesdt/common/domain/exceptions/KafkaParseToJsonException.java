package fr.gouv.intradef.pesdt.common.domain.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
public class KafkaParseToJsonException extends RuntimeException
{
	public KafkaParseToJsonException(String message)
	{
		super(message);
	}
}
