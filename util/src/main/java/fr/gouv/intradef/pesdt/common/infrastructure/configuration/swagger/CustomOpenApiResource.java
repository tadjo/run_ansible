package fr.gouv.intradef.pesdt.common.infrastructure.configuration.swagger;


import com.fasterxml.jackson.core.JsonProcessingException;
import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.PathItem;
import io.swagger.v3.oas.models.Paths;
import io.swagger.v3.oas.models.security.SecurityScheme;
import io.swagger.v3.parser.ObjectMapperFactory;
import io.swagger.v3.parser.OpenAPIV3Parser;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.function.UnaryOperator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.springdoc.core.GroupedOpenApi;
import org.springdoc.core.SwaggerUiConfigParameters;
import org.springdoc.core.customizers.OpenApiCustomiser;

/**
 * Classe contenant les paramètres de l'API
 */
public class CustomOpenApiResource
{
	private static final Pattern CUSTOM_DESC =
			Pattern.compile("^<h2>(?<title>[^<]+)</h2>\\n\\r?(?<desc>.+)$", 40);
	private final ConcurrentLinkedQueue<ApiDefinition> apiDefinitions = new ConcurrentLinkedQueue();

	/**
	 * Paramètrres de configuration du swagger
	 */
	private final SwaggerUiConfigParameters swaggerUiConfigParameters;

	/**
	 * Infos sur l'API
	 */
	private final ApiInfos apiInfos;

	private final String actuatorPath;

	/**
	 * Constructeur
	 *
	 * @param swaggerUiConfigParameters Paramètres du swagger
	 * @param apiInfos                  infos de l'API
	 * @param actuatorPath              Chemin de l'actuator (opur obtenir les informations opérationnelles)
	 */
	public CustomOpenApiResource(SwaggerUiConfigParameters swaggerUiConfigParameters,
								 ApiInfos apiInfos,
								 String actuatorPath)
	{
		this.swaggerUiConfigParameters = swaggerUiConfigParameters;
		this.apiInfos = apiInfos;
		this.actuatorPath = actuatorPath;
	}

	/**
	 * Copie des informations du schéma
	 *
	 * @param source source des informations
	 * @param target objet où sont copiés les informations
	 */
	private static void copySecurityScheme(SecurityScheme source, SecurityScheme target)
	{
		target.name(source.getName())
				.type(source.getType())
				.description(source.getDescription())
				.$ref(source.get$ref())
				.in(source.getIn())
				.scheme(source.getScheme())
				.bearerFormat(source.getBearerFormat())
				.flows(source.getFlows())
				.openIdConnectUrl(source.getOpenIdConnectUrl())
				.setExtensions(source.getExtensions());
	}

	/**
	 * Copie des extensions d'une api source à une api target
	 *
	 * @param openAPI    api source
	 * @param newOpenAPI api target
	 */
	private static void copyExtensions(OpenAPI openAPI, OpenAPI newOpenAPI)
	{
		newOpenAPI.setExtensions(openAPI.getExtensions());
		newOpenAPI.getInfo().setExtensions(openAPI.getInfo().getExtensions());
		newOpenAPI.getPaths().forEach((kp, p) ->
				{
					Consumer<Map.Entry<String, PathItem>> entryConsumer = (eop) -> {
						p.setExtensions(eop.getValue().getExtensions());
						p.readOperations().forEach((o) -> {
							eop.getValue().readOperations().stream().filter((ro) ->
									o.getOperationId().equals(ro.getOperationId())).findFirst().ifPresent((ro) ->
									o.setExtensions(ro.getExtensions()));
						});
					};
					openAPI.getPaths().entrySet()
							.stream().filter((epa) -> epa.getKey().equals(kp))
							.findFirst().ifPresent(entryConsumer);
				}
		);
	}

	/**
	 * Initialisation des définitions de l'API
	 *
	 * @param serviceName    nom du service
	 * @param serviceVersion veriosn du service
	 */
	public void addOpenApiDefinition(String serviceName, String serviceVersion)
	{
		OpenApiCustomiser groupCustomiser = new GroupCustomiser(serviceName, serviceVersion,
				String.format("/%s/**", serviceVersion), this.actuatorPath.concat("/**"));
		this.apiDefinitions.add(ApiDefinition.builder().apiName(serviceName).apiVersion(serviceVersion)
				.groupedOpenApi(this.buildOpenApi(serviceName, groupCustomiser)).groupCustomiser(groupCustomiser).build());
		this.apiDefinitions.forEach(d -> {
			this.swaggerUiConfigParameters.addGroup(d.getApiName().concat(" - ").concat(d.getApiVersion()));
		});
	}

	/**
	 * Getter des définitions de l'API
	 *
	 * @param serviceVersion  veriosn du service
	 * @param prod
	 * @param actuatorSecured
	 * @return
	 */
	public Optional<OpenAPI> getOpenApiDefinition(String serviceVersion, boolean prod, boolean actuatorSecured)
	{
		Optional<ApiDefinition> optionalOpenAPI = this.apiDefinitions.stream().filter((d) ->
				d.getApiVersion().equalsIgnoreCase(serviceVersion)).findFirst();
		optionalOpenAPI.ifPresent((o) ->
				((GroupCustomiser) o.getGroupCustomiser()).withSwaggerVersion(this.apiInfos.getSwaggerVersion())
						.withServerInfos(this.apiInfos.getServerInfos()).customise(o.getOpenAPI()));
		return optionalOpenAPI.map(ApiDefinition::getOpenAPI).map((o) -> this.filterProd(o, prod, actuatorSecured));
	}

	/**
	 * @param openAPI         api à filtrer
	 * @param prod            paramètre de filtrage
	 * @param actuatorSecured paramètre de filtrage
	 * @return api filtré
	 */
	private OpenAPI filterProd(OpenAPI openAPI, boolean prod, boolean actuatorSecured)
	{
		if (!prod && actuatorSecured)
		{
			return openAPI;
		}
		else
		{
			OpenAPI newOpenAPI;
			try
			{
				newOpenAPI = (new OpenAPIV3Parser()).readContents(ObjectMapperFactory.createJson().writeValueAsString(openAPI)).getOpenAPI();
			}
			catch (JsonProcessingException var13)
			{
				return openAPI;
			}

			String title = newOpenAPI.getInfo().getTitle();
			String description = newOpenAPI.getInfo().getDescription();
			Matcher customDescMatcher = CUSTOM_DESC.matcher(description);
			if (prod && customDescMatcher.matches())
			{
				title = customDescMatcher.group("title");
				description = customDescMatcher.group("desc");
			}

			newOpenAPI.getInfo().setTitle(title);
			newOpenAPI.getInfo().setDescription(description);
			UnaryOperator<Paths> pathFilter = p -> p;
			UnaryOperator<Components> componentsFilter = c -> c;
			if (prod)
			{
				pathFilter = p -> GroupCustomiser.removeActuatorRootEndpoint(p, this.actuatorPath);
				componentsFilter = GroupCustomiser::removeActuatorRootObject;
			}

			Paths newPaths = pathFilter.apply(newOpenAPI.getPaths());
			Components newComponents = componentsFilter.apply(newOpenAPI.getComponents());

			newComponents.getSecuritySchemes().forEach((name, securityScheme) ->
					Optional.ofNullable(openAPI.getComponents().getSecuritySchemes().get(name)).ifPresent((ss) ->
							copySecurityScheme(ss, securityScheme)));
			Map<String, PathItem> pathItems = new HashMap((Map) newPaths.entrySet().stream()
					.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)));
			pathItems.entrySet().stream().filter((entry) -> prod && (entry.getKey()).startsWith(this.actuatorPath))
					.filter((entry) -> Pattern.compile("^".concat(this.actuatorPath)
							.concat("(?!/health|/metrics).+$")).matcher(entry.getKey()).matches())
					.forEach((entry) -> newPaths.remove(entry.getKey(), entry.getValue()));

			if (!actuatorSecured)
			{
				Predicate<Map.Entry<String, PathItem>> predicate = (pathEntry) ->
						Pattern.compile("^".concat(this.actuatorPath)
								.concat("(/.+)?$")).matcher(pathEntry.getKey()).matches();
				Map<String, PathItem> collect = newPaths.entrySet().stream().filter(predicate)
						.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
				collect.forEach((k, path) -> path.readOperations().forEach(o -> o.setSecurity(Collections.emptyList())));
			}

			newOpenAPI.setPaths(newPaths);
			newOpenAPI.setComponents(newComponents);
			copyExtensions(openAPI, newOpenAPI);
			return newOpenAPI;
		}
	}

	/**
	 * Création d'une OpenApi
	 *
	 * @param apiName         nom
	 * @param groupCustomiser Customizer
	 * @return
	 */
	private GroupedOpenApi buildOpenApi(String apiName, OpenApiCustomiser groupCustomiser)
	{
		return GroupedOpenApi.builder().group(apiName).pathsToMatch(((GroupCustomiser) groupCustomiser)
				.getPathsToMatch()).addOpenApiCustomiser(groupCustomiser).build();
	}

	/**
	 * Getter
	 *
	 * @return liste des apis
	 */
	public List<GroupedOpenApi> getOpenApiDefinitions()
	{
		return this.apiDefinitions.stream().map(ApiDefinition::getGroupedOpenApi).collect(Collectors.toList());
	}

	/**
	 * Getter
	 *
	 * @return liste des apis en fonction des versions
	 */
	public List<String> getOpenApiDefinitionGroups()
	{
		return this.apiDefinitions.stream().map(ApiDefinition::getApiVersion).collect(Collectors.toList());
	}
}
