/**
 * Classe qui étend la classe AbstractDto et comportant une liste d'objet avec un getter, setter et des méthodes equals, hashCode et toString
 */

package fr.gouv.intradef.pesdt.common.api.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.v3.oas.annotations.media.Schema;

import java.util.List;

public abstract class AbstractDtoWrapper<K extends AbstractDto<?>>
{
	@JsonIgnore
	@Schema(
			description = "Liste d'objets encapsulés"
	)
	protected List<K> dataList;

	public AbstractDtoWrapper()
	{
	}

	public List<K> getDataList()
	{
		return this.dataList;
	}

	public void setDataList(List<K> dataList)
	{
		this.dataList = dataList;
	}

	public String toString()
	{
		return "AbstractDtoWrapper(dataList=" + this.getDataList() + ")";
	}

	public boolean equals(Object o)
	{
		boolean result = true;
		if (o != this)
		{
			if (!(o instanceof AbstractDtoWrapper))
			{
				result = false;
			}
			else
			{
				AbstractDtoWrapper<?> other = (AbstractDtoWrapper) o;

				Object this$dataList = this.getDataList();
				Object other$dataList = other.getDataList();
				if (this$dataList == null)
				{
					result = other$dataList == null;
				}
				else
				{
					result = this$dataList.equals(other$dataList);
				}
			}
		}
		return result;

	}

	public int hashCode()
	{
		int result = 1;
		Object $dataList = this.getDataList();
		result = result * 59 + ($dataList == null ? 43 : $dataList.hashCode());
		return result;
	}
}
