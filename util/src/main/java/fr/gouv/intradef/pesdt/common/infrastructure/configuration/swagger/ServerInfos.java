package fr.gouv.intradef.pesdt.common.infrastructure.configuration.swagger;

/**
 * Classe contenant les informations du Server (schema, hostname, port, apiDocUrl, applicationContextPath
 */
public class ServerInfos
{
	public static final int DEFAULT_PORT = 8080;
	public static final String DEFAULT_HOSTNAME = "localhost";
	public static final String DEFAULT_PROTOCOL = "http";
	private final String scheme;
	private final String hostName;
	private final int port;
	private final String apiDocUrl;
	private final String applicationContextPath;

	/**
	 * Constructeur
	 *
	 * @param scheme
	 * @param hostName
	 * @param port
	 * @param apiDocUrl
	 * @param applicationContextPath
	 */
	ServerInfos(String scheme, String hostName, int port, String apiDocUrl, String applicationContextPath)
	{
		this.scheme = scheme;
		this.hostName = hostName;
		this.port = port;
		this.apiDocUrl = apiDocUrl;
		this.applicationContextPath = applicationContextPath;
	}

	/**
	 * @return builder
	 */
	public static ServerInfosBuilder builder()
	{
		return new ServerInfosBuilder();
	}

	/**
	 * @return Schema
	 */
	public String getScheme()
	{
		return this.scheme;
	}

	/**
	 * @return hostname
	 */
	public String getHostName()
	{
		return this.hostName;
	}

	/**
	 * @return port
	 */
	public int getPort()
	{
		return this.port;
	}

	/**
	 * @return url de l'api doc
	 */
	public String getApiDocUrl()
	{
		return this.apiDocUrl;
	}

	/**
	 * @return chemin du contexte
	 */
	public String getApplicationContextPath()
	{
		return this.applicationContextPath;
	}

	/**
	 * Classe builder
	 */
	public static class ServerInfosBuilder
	{
		private String scheme;
		private String hostName;
		private int port;
		private String apiDocUrl;
		private String applicationContextPath;

		ServerInfosBuilder()
		{
		}

		/**
		 * @param scheme input
		 * @return le builder mis à jour
		 */
		public ServerInfosBuilder scheme(String scheme)
		{
			this.scheme = scheme;
			return this;
		}

		/**
		 * @param hostName
		 * @return le builder mis à jour
		 */
		public ServerInfosBuilder hostName(String hostName)
		{
			this.hostName = hostName;
			return this;
		}

		/**
		 * @param port
		 * @return le builder mis à jour
		 */
		public ServerInfosBuilder port(int port)
		{
			this.port = port;
			return this;
		}

		/**
		 * @param apiDocUrl
		 * @return le builder mis à jour
		 */
		public ServerInfosBuilder apiDocUrl(String apiDocUrl)
		{
			this.apiDocUrl = apiDocUrl;
			return this;
		}

		/**
		 * @param applicationContextPath * @return le builder mis à jour
		 */
		public ServerInfosBuilder applicationContextPath(String applicationContextPath)
		{
			this.applicationContextPath = applicationContextPath;
			return this;
		}

		/**
		 * @return les infos du server
		 */
		public ServerInfos build()
		{
			return new ServerInfos(this.scheme, this.hostName, this.port, this.apiDocUrl, this.applicationContextPath);
		}

		@Override
		public String toString()
		{
			return "ServerInfos.ServerInfosBuilder(scheme=" + this.scheme + ", hostName=" + this.hostName + ", port="
					+ this.port + ", apiDocUrl=" + this.apiDocUrl + ", applicationContextPath="
					+ this.applicationContextPath + ")";
		}
	}
}
