package fr.gouv.intradef.pesdt.common.infrastructure.configuration.database;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * TODO: Cette classe est dans le module util mais devrait être dans un module "configuration" ou autre
 * Enum contenant les paramètres pour la configuration des bases (ou schémas)
 * Rajouter un enum pour chaque connexion à une nouvelle base
 */
@Getter
@RequiredArgsConstructor
public enum DataSourceType {
	CYCLEPO("cyclePO");

	private final String name;
}
