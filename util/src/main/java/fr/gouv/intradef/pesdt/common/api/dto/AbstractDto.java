/**
 * Classe comportant un Id et une version avec des getter, setter et des méthodes equals, hashCode et toString
 */

package fr.gouv.intradef.pesdt.common.api.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;

public abstract class AbstractDto<K>
{
	@JsonProperty("id")
	@Schema(
			description = "Identifiant technique de l'entité"
	)
	private K id;
	@JsonProperty("version")
	@Schema(
			description = "Version de l'entité"
	)
	private Long version;

	public AbstractDto()
	{
	}

	public K getId()
	{
		return this.id;
	}

	@JsonProperty("id")
	public void setId(K id)
	{
		this.id = id;
	}

	public Long getVersion()
	{
		return this.version;
	}

	@JsonProperty("version")
	public void setVersion(Long version)
	{
		this.version = version;
	}

	public String toString()
	{
		return "AbstractDto(id=" + this.getId() + ", version=" + this.getVersion() + ")";
	}

	public boolean equals(Object o)
	{
		if (o == this)
		{
			return true;
		}
		else if (!(o instanceof AbstractDto))
		{
			return false;
		}
		else
		{
			AbstractDto<?> other = (AbstractDto) o;

			Object this$id = this.getId();
			Object other$id = other.getId();
			if (this$id == null)
			{
				if (other$id != null)
				{
					return false;
				}
			}
			else if (!this$id.equals(other$id))
			{
				return false;
			}

			Object this$version = this.getVersion();
			Object other$version = other.getVersion();
			if (this$version == null)
			{
				return other$version == null;
			}
			else
			{
				return this$version.equals(other$version);
			}
		}
	}


	@Override
	public int hashCode()
	{
		int result = 1;
		Object $id = this.getId();
		result = result * 59 + ($id == null ? 43 : $id.hashCode());
		Object $version = this.getVersion();
		result = result * 59 + ($version == null ? 43 : $version.hashCode());
		return result;
	}
}
