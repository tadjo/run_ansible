/**
 * Interface pour les services de lecture
 */

package fr.gouv.intradef.pesdt.common.domain.port.common;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface IQueryService<D, K>
{
	List<D> findAll();

	Page<D> findAllPage(Pageable page);

	D findById(K id);
}
