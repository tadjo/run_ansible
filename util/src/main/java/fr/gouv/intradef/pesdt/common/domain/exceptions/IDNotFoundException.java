package fr.gouv.intradef.pesdt.common.domain.exceptions;

import java.util.Optional;

import fr.gouv.intradef.pesdt.common.infrastructure.adapter.jpa.entity.AbstractEntity;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class IDNotFoundException extends RuntimeException
{
	private static final long serialVersionUID = -6729396227937640241L;

	public <T> IDNotFoundException(T idNonTrouve, Class<? extends AbstractEntity<T>> clazz)
	{
		super(String.format("L'id suivant : '%s' n'est pas trouvé pour l'entité du type '%s'", Optional.ofNullable(idNonTrouve).map(Object::toString).orElse(""), clazz.getSimpleName().replace("Entity", "")));
	}

}