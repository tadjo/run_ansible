package fr.gouv.intradef.pesdt.adapters.commands;

import fr.gouv.intradef.pesdt.dto.PeriodeCycleDTO;
import fr.gouv.intradef.pesdt.entity.PeriodeCycleEntity;
import fr.gouv.intradef.pesdt.mappers.PeriodeCycleMapper;
import fr.gouv.intradef.pesdt.repository.PeriodeCycleRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.LocalDateTime;
import java.time.Month;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static fr.gouv.intradef.pesdt.common.domain.utils.AssertionUtils.assertEquals;
import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PeriodeCycleCommandJpaAdapterTest {


    @InjectMocks
    private PeriodeCycleCommandJpaAdapter periodeCycleCommandJpaAdapter;

    @Mock
    private PeriodeCycleRepository periodeCycleRepository;

    @Mock
    private PeriodeCycleMapper periodeCycleMapper;

    private List<PeriodeCycleDTO> periodeCycleDTOList;

    private List<PeriodeCycleDTO> periodeCycleDTOListRegistred;

    @Before
    public void prepare(){

        MockitoAnnotations.openMocks(this);

        PeriodeCycleDTO periodeCycleDTO = new PeriodeCycleDTO();
        periodeCycleDTO.setId(1L);
        periodeCycleDTO.setDateDeDebut(LocalDateTime.of(2020, Month.FEBRUARY, 19, 0, 0));
        periodeCycleDTO.setDateDeFin(LocalDateTime.of(2020, Month.FEBRUARY, 25, 0, 0));
        periodeCycleDTO.setDescription("Test 01");
        periodeCycleDTO.setVersion(1l);

        PeriodeCycleDTO periodeCycleDTO2 = new PeriodeCycleDTO();
        periodeCycleDTO2.setId(2L);
        periodeCycleDTO2.setDateDeDebut(LocalDateTime.of(2021, Month.FEBRUARY, 19, 0, 0));
        periodeCycleDTO2.setDateDeFin(LocalDateTime.of(2021, Month.FEBRUARY, 25, 0, 0));
        periodeCycleDTO2.setDescription("Test 02");
        periodeCycleDTO2.setVersion(1l);

        periodeCycleDTOList = new ArrayList<>();
        periodeCycleDTOList.add(periodeCycleDTO);
        periodeCycleDTOList.add(periodeCycleDTO2);

        PeriodeCycleDTO periodeCycleDtoRegistred = new PeriodeCycleDTO();
        periodeCycleDtoRegistred.setId(1L);
        periodeCycleDtoRegistred.setDateDeDebut(LocalDateTime.of(2020, Month.FEBRUARY, 19, 0, 0));
        periodeCycleDtoRegistred.setDateDeFin(LocalDateTime.of(2020, Month.FEBRUARY, 25, 0, 0));
        periodeCycleDtoRegistred.setDescription("test 1 période de cycle enregistre en bdd");
        periodeCycleDtoRegistred.setVersion(1l);

        PeriodeCycleDTO periodeCycleDtoRegistred2 = new PeriodeCycleDTO();
        periodeCycleDtoRegistred2.setId(2L);
        periodeCycleDtoRegistred2.setDateDeDebut(LocalDateTime.of(2021, Month.FEBRUARY, 19, 0, 0));
        periodeCycleDtoRegistred2.setDateDeFin(LocalDateTime.of(2021, Month.FEBRUARY, 25, 0, 0));
        periodeCycleDtoRegistred2.setDescription("test 2 période de cycle enregistre en bdd");

        periodeCycleDtoRegistred2.setVersion(1l);

        periodeCycleDTOListRegistred = new ArrayList<>();
        periodeCycleDTOListRegistred.add(periodeCycleDtoRegistred);
        periodeCycleDTOListRegistred.add(periodeCycleDtoRegistred2);

    }

    @Test
    public void saveAll() {

        PeriodeCycleEntity periodeCycleEntity = new PeriodeCycleEntity();
        periodeCycleEntity.setId(1L);
        periodeCycleEntity.setDateDeDebut(LocalDateTime.of(2020, Month.FEBRUARY, 19, 0, 0));
        periodeCycleEntity.setDateDeFin(LocalDateTime.of(2020, Month.FEBRUARY, 25, 0, 0));
        periodeCycleEntity.setDescription("test 1 période de cycle enregistre en bdd");
        periodeCycleEntity.setVersion(1l);

        PeriodeCycleDTO periodeCycleDTOReturned = new PeriodeCycleDTO();
        periodeCycleDTOReturned.setId(1L);
        periodeCycleDTOReturned.setDateDeDebut(LocalDateTime.of(2020, Month.FEBRUARY, 19, 0, 0));
        periodeCycleDTOReturned.setDateDeFin(LocalDateTime.of(2020, Month.FEBRUARY, 25, 0, 0));
        periodeCycleDTOReturned.setDescription("Test 01");
        periodeCycleDTOReturned.setVersion(1l);

        when(periodeCycleMapper.toDtos(Collections.singletonList(periodeCycleEntity))).thenReturn(periodeCycleDTOListRegistred);
        when(periodeCycleRepository.saveAll(any())).thenReturn(Collections.singletonList(periodeCycleEntity));

        List<PeriodeCycleDTO> result = periodeCycleCommandJpaAdapter.saveAll(periodeCycleDTOList);

        Assert.assertEquals(result, periodeCycleDTOListRegistred);
        Assert.assertEquals(result.get(0).getId(), periodeCycleDTOListRegistred.get(0).getId());
        Assert.assertEquals(result.get(0).getDateDeDebut(), periodeCycleDTOListRegistred.get(0).getDateDeDebut());
        Assert.assertEquals(result.get(0).getDateDeFin(), periodeCycleDTOListRegistred.get(0).getDateDeFin());
        Assert.assertEquals(result.get(0).getPhase(), periodeCycleDTOListRegistred.get(0).getPhase());
    }

    @Test
    public void save() {
        PeriodeCycleDTO periodeCycleDTO = periodeCycleDTOList.get(0);

        PeriodeCycleDTO periodeCycleDTOReturned = new PeriodeCycleDTO();
        periodeCycleDTOReturned.setId(1L);
        periodeCycleDTOReturned.setDateDeDebut(LocalDateTime.of(2020, Month.FEBRUARY, 19, 0, 0));
        periodeCycleDTOReturned.setDateDeFin(LocalDateTime.of(2020, Month.FEBRUARY, 25, 0, 0));
        periodeCycleDTOReturned.setDescription("Test 01");
        periodeCycleDTOReturned.setVersion(1l);

        PeriodeCycleEntity periodeCycleEntity = new PeriodeCycleEntity();
        periodeCycleEntity.setId(1L);
        periodeCycleEntity.setDateDeDebut(LocalDateTime.of(2020, Month.FEBRUARY, 19, 0, 0));
        periodeCycleEntity.setDateDeFin(LocalDateTime.of(2020, Month.FEBRUARY, 25, 0, 0));
        periodeCycleEntity.setDescription("Test 01");
        periodeCycleEntity.setVersion(1l);

        when(periodeCycleMapper.toDto(periodeCycleEntity)).thenReturn(periodeCycleDTO);
        when(periodeCycleMapper.toEntity(periodeCycleDTO)).thenReturn(periodeCycleEntity);
        when(periodeCycleRepository.save(periodeCycleEntity)).thenReturn(periodeCycleEntity);

        PeriodeCycleDTO result = periodeCycleCommandJpaAdapter.save(periodeCycleDTO);

        Assert.assertEquals(result, periodeCycleDTOReturned);
        Assert.assertEquals(result.getId(), periodeCycleDTOReturned.getId());
        Assert.assertEquals(result.getDateDeDebut(), periodeCycleDTOReturned.getDateDeDebut());
        Assert.assertEquals(result.getDateDeFin(), periodeCycleDTOReturned.getDateDeFin());
        Assert.assertEquals(result.getPhase(), periodeCycleDTOReturned.getPhase());
    }

    @Test
    public void deleteAllById() {
        List<Long> ids = Arrays.asList(1L);

        PeriodeCycleDTO periodeCycleDTOReturned = new PeriodeCycleDTO();
        periodeCycleDTOReturned.setId(1L);
        periodeCycleDTOReturned.setDateDeDebut(LocalDateTime.of(2020, Month.FEBRUARY, 19, 0, 0));
        periodeCycleDTOReturned.setDateDeFin(LocalDateTime.of(2020, Month.FEBRUARY, 25, 0, 0));
        periodeCycleDTOReturned.setDescription("Test 01");
        periodeCycleDTOReturned.setVersion(1l);

        PeriodeCycleEntity periodeCycleEntity = new PeriodeCycleEntity();
        periodeCycleEntity.setId(1L);
        periodeCycleEntity.setDateDeDebut(LocalDateTime.of(2020, Month.FEBRUARY, 19, 0, 0));
        periodeCycleEntity.setDateDeFin(LocalDateTime.of(2020, Month.FEBRUARY, 25, 0, 0));
        periodeCycleEntity.setDescription("Test 01");
        periodeCycleEntity.setVersion(1l);

        List<PeriodeCycleEntity> foundPeriode = Arrays.asList(periodeCycleEntity);
        List<PeriodeCycleDTO> expectedResult = Arrays.asList(periodeCycleDTOReturned);

        Mockito.when(periodeCycleRepository.findAllById(ids)).thenReturn(foundPeriode);
        Mockito.when(periodeCycleMapper.toDtos(foundPeriode)).thenReturn(expectedResult);

        List<PeriodeCycleDTO> result = periodeCycleCommandJpaAdapter.deleteAllById(ids);
        assertEquals(expectedResult, result);

        Mockito.verify(periodeCycleRepository).findAllById(ids);
        Mockito.verify(periodeCycleRepository).deleteAll(foundPeriode);
        Mockito.verify(periodeCycleMapper).toDtos(foundPeriode);
    }

    @Test
    public void deleteById() {
        PeriodeCycleDTO periodeCycleDTOReturned = new PeriodeCycleDTO();
        periodeCycleDTOReturned.setId(1L);
        periodeCycleDTOReturned.setDateDeDebut(LocalDateTime.of(2020, Month.FEBRUARY, 19, 0, 0));
        periodeCycleDTOReturned.setDateDeFin(LocalDateTime.of(2020, Month.FEBRUARY, 25, 0, 0));
        periodeCycleDTOReturned.setDescription("Test 01");
        periodeCycleDTOReturned.setVersion(1l);

        PeriodeCycleEntity periodeCycleEntity = new PeriodeCycleEntity();
        periodeCycleEntity.setId(1L);
        periodeCycleEntity.setDateDeDebut(LocalDateTime.of(2020, Month.FEBRUARY, 19, 0, 0));
        periodeCycleEntity.setDateDeFin(LocalDateTime.of(2020, Month.FEBRUARY, 25, 0, 0));
        periodeCycleEntity.setDescription("Test 01");
        periodeCycleEntity.setVersion(1l);

        //WHEN
        when(periodeCycleMapper.toDto(periodeCycleEntity)).thenReturn(periodeCycleDTOReturned);
        when(periodeCycleRepository.findById(1l)).thenReturn(Optional.of(periodeCycleEntity));

        PeriodeCycleDTO result = periodeCycleCommandJpaAdapter.deleteById(periodeCycleEntity.getId());

        assertEquals(periodeCycleEntity.getId(), result.getId());

    }

    @Test
    public void updateAll() {

        PeriodeCycleDTO periodeCycleDTOReturned = new PeriodeCycleDTO();
        periodeCycleDTOReturned.setId(1L);
        periodeCycleDTOReturned.setDateDeDebut(LocalDateTime.of(2020, Month.FEBRUARY, 19, 0, 0));
        periodeCycleDTOReturned.setDateDeFin(LocalDateTime.of(2020, Month.FEBRUARY, 25, 0, 0));
        periodeCycleDTOReturned.setDescription("Test 01");
        periodeCycleDTOReturned.setVersion(1l);

        PeriodeCycleEntity periodeCycleEntity = new PeriodeCycleEntity();
        periodeCycleEntity.setId(1L);
        periodeCycleEntity.setDateDeDebut(LocalDateTime.of(2020, Month.FEBRUARY, 19, 0, 0));
        periodeCycleEntity.setDateDeFin(LocalDateTime.of(2020, Month.FEBRUARY, 25, 0, 0));
        periodeCycleEntity.setDescription("Test 01");
        periodeCycleEntity.setVersion(1l);

        List<PeriodeCycleEntity> foundPeriode = Arrays.asList(periodeCycleEntity);
        List<PeriodeCycleDTO> expectedResult = Arrays.asList(periodeCycleDTOReturned);

        Mockito.when(periodeCycleRepository.findAllById(any())).thenReturn(foundPeriode);
        Mockito.when(periodeCycleMapper.toDtos(any())).thenReturn(expectedResult);
        when(periodeCycleMapper.toEntities(any())).thenReturn(foundPeriode);

        List<PeriodeCycleDTO> result = periodeCycleCommandJpaAdapter.updateAll(expectedResult);
        assertEquals(expectedResult, result);

    }

    @Test
    public void update() {
        PeriodeCycleDTO periodeCycleDTO = periodeCycleDTOList.get(0);

        PeriodeCycleDTO periodeCycleDTOReturned = new PeriodeCycleDTO();
        periodeCycleDTOReturned.setId(1L);
        periodeCycleDTOReturned.setDateDeDebut(LocalDateTime.of(2020, Month.FEBRUARY, 19, 0, 0));
        periodeCycleDTOReturned.setDateDeFin(LocalDateTime.of(2020, Month.FEBRUARY, 25, 0, 0));
        periodeCycleDTOReturned.setDescription("Test 01");
        periodeCycleDTOReturned.setVersion(1l);

        PeriodeCycleEntity periodeCycleEntity = new PeriodeCycleEntity();
        periodeCycleEntity.setId(1L);
        periodeCycleEntity.setDateDeDebut(LocalDateTime.of(2020, Month.FEBRUARY, 19, 0, 0));
        periodeCycleEntity.setDateDeFin(LocalDateTime.of(2020, Month.FEBRUARY, 25, 0, 0));
        periodeCycleEntity.setDescription("Test 01");
        periodeCycleEntity.setVersion(1l);

        when(periodeCycleMapper.toDto(any())).thenReturn(periodeCycleDTO);
        when(periodeCycleMapper.toEntity(any())).thenReturn(periodeCycleEntity);
        when(periodeCycleRepository.save(any())).thenReturn(periodeCycleEntity);
        when(periodeCycleRepository.findById(any())).thenReturn(Optional.of(periodeCycleEntity));

        PeriodeCycleDTO result = periodeCycleCommandJpaAdapter.update(periodeCycleDTO);

        Assert.assertEquals(result, periodeCycleDTOReturned);
        Assert.assertEquals(result.getId(), periodeCycleDTOReturned.getId());
        Assert.assertEquals(result.getDateDeDebut(), periodeCycleDTOReturned.getDateDeDebut());
        Assert.assertEquals(result.getDateDeFin(), periodeCycleDTOReturned.getDateDeFin());
        Assert.assertEquals(result.getPhase(), periodeCycleDTOReturned.getPhase());
    }
}