package fr.gouv.intradef.pesdt.adapters.commands;

import fr.gouv.intradef.pesdt.common.domain.exceptions.AlreadyExistsException;
import fr.gouv.intradef.pesdt.common.domain.exceptions.IDNotFoundException;
import fr.gouv.intradef.pesdt.common.domain.exceptions.LowerVersionException;
import fr.gouv.intradef.pesdt.common.domain.exceptions.NullParameterException;
import fr.gouv.intradef.pesdt.dto.PhaseDTO;
import fr.gouv.intradef.pesdt.entity.PhaseEntity;
import fr.gouv.intradef.pesdt.mappers.PhaseMapper;
import fr.gouv.intradef.pesdt.repository.PhaseRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static fr.gouv.intradef.pesdt.common.domain.utils.AssertionUtils.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class PhaseCommandJpaAdapterTest {

    private MockMvc mockMvc;
    @InjectMocks
    private PhaseCommandJpaAdapter phaseCommandJpaAdapter;

    @Mock
    private PhaseRepository phaseRepository;

    @Mock
    private PhaseMapper phaseMapper;
    private PhaseDTO phaseDTO;

    private PhaseDTO phaseDTO2;
    private List<PhaseDTO> phaseDTOList;

    private PhaseEntity phaseEntity;

    private List<PhaseDTO> phaseDTOListRegistred;

    @Before
    public void prepare() {
        MockitoAnnotations.openMocks(this);

        phaseDTO = new PhaseDTO();
        phaseDTO.setId(1L);
        phaseDTO.setAbreviation("OPEX");
        phaseDTO.setDescription("OPEX (Opérations Extérieusr) sont les \"interventions des forces militaires françaises en dehors du territoire national\"");
        phaseDTO.setIndexCouleur(1);
        phaseDTO.setVersion(1l);

        phaseDTO2 = new PhaseDTO();
        phaseDTO2.setId(2L);
        phaseDTO2.setAbreviation("RCO");
        phaseDTO2.setDescription("Remise en Condition Operationnel");
        phaseDTO2.setIndexCouleur(2);
        phaseDTO2.setVersion(1l);

        phaseDTOList = new ArrayList<>();
        phaseDTOList.add(phaseDTO);
        phaseDTOList.add(phaseDTO2);

        phaseEntity = new PhaseEntity();
        phaseEntity.setId(1L);
        phaseEntity.setAbreviation("OPEX");
        phaseEntity.setDescription("OPEX (Opérations Extérieusr) sont les \"interventions des forces militaires françaises en dehors du territoire national\"");
        phaseEntity.setIndexCouleur(1);
        phaseEntity.setVersion(1l);
    }

    @Test
    public void save() {
        when(phaseMapper.toDto(phaseEntity)).thenReturn(phaseDTO);
        when(phaseMapper.toEntity(phaseDTO)).thenReturn(phaseEntity);
        when(phaseRepository.save(any())).thenReturn(phaseEntity);

        PhaseDTO result = phaseCommandJpaAdapter.save(phaseDTO);

        Assert.assertEquals(result, phaseDTO);
        Assert.assertEquals(result.getId(), phaseDTO.getId());
        Assert.assertEquals(result.getVersion(), phaseDTO.getVersion());
        Assert.assertEquals(result.getAbreviation(), phaseDTO.getAbreviation());
        Assert.assertEquals(result.getIndexCouleur(), phaseDTO.getIndexCouleur());
    }

    @Test
    public void saveAll() {

        when(phaseMapper.toDtos(Collections.singletonList(phaseEntity))).thenReturn(phaseDTOList);
        when(phaseRepository.saveAll(any())).thenReturn(Collections.singletonList(phaseEntity));

        List<PhaseDTO> result = phaseCommandJpaAdapter.saveAll(Collections.singletonList(phaseDTO));

        Assert.assertEquals(result, phaseDTOList);
        Assert.assertEquals(result.get(0).getId(), phaseDTOList.get(0).getId());
        Assert.assertEquals(result.get(0).getVersion(), phaseDTOList.get(0).getVersion());
        Assert.assertEquals(result.get(0).getAbreviation(), phaseDTOList.get(0).getAbreviation());
        Assert.assertEquals(result.get(0).getIndexCouleur(), phaseDTOList.get(0).getIndexCouleur());
    }

    @Test
    public void update() {
        when(phaseMapper.toDto(any())).thenReturn(phaseDTO);
        when(phaseMapper.toEntity(any())).thenReturn(phaseEntity);
        when(phaseRepository.save(any())).thenReturn(phaseEntity);
        when(phaseRepository.findById(any())).thenReturn(Optional.of(phaseEntity));

        PhaseDTO result = phaseCommandJpaAdapter.update(phaseDTO);

        Assert.assertEquals(result, phaseDTO);
        Assert.assertEquals(result.getId(), phaseDTO.getId());
        Assert.assertEquals(result.getAbreviation(), phaseDTO.getAbreviation());
        Assert.assertEquals(result.getVersion(), phaseDTO.getVersion());
        Assert.assertEquals(result.getIndexCouleur(), phaseDTO.getIndexCouleur());
    }

    @Test
    public void deleteById() {
        //GIVEN

        when(phaseMapper.toDto(phaseEntity)).thenReturn(phaseDTO);
        when(phaseRepository.findById(1L)).thenReturn(Optional.of(phaseEntity));

        //WHEN
        PhaseDTO phaseDTOReturned = phaseCommandJpaAdapter.deleteById(1L);

        //THEN
        Assert.assertNotNull("Activité saved null", phaseDTO);
        Assert.assertEquals(phaseDTOReturned.getId(), phaseDTO.getId());
        Assert.assertEquals(phaseDTOReturned.getVersion(), phaseDTO.getVersion());
        Assert.assertEquals(phaseDTOReturned.getDescription(), phaseDTO.getDescription());
        Assert.assertEquals(phaseDTOReturned.getAbreviation(), phaseDTO.getAbreviation());
    }

    @Test
    public void deleteAllById() {
        List<Long> ids = Arrays.asList(1L);

        List<PhaseEntity> foundPeriode = Arrays.asList(phaseEntity);
        List<PhaseDTO> expectedResult = Arrays.asList(phaseDTO);

        Mockito.when(phaseRepository.findAllById(ids)).thenReturn(foundPeriode);
        Mockito.when(phaseMapper.toDtos(foundPeriode)).thenReturn(expectedResult);

        List<PhaseDTO> result = phaseCommandJpaAdapter.deleteAllById(ids);
        assertEquals(expectedResult, result);

        Mockito.verify(phaseRepository).findAllById(ids);
        Mockito.verify(phaseRepository).deleteAll(foundPeriode);
        Mockito.verify(phaseMapper).toDtos(foundPeriode);
    }

    @Test
    public void updateAll() {

        List<PhaseEntity> foundPeriode = Arrays.asList(phaseEntity);
        List<PhaseDTO> expectedResult = Arrays.asList(phaseDTO);

        Mockito.when(phaseRepository.findAllById(any())).thenReturn(foundPeriode);
        Mockito.when(phaseMapper.toDtos(any())).thenReturn(expectedResult);
        when(phaseMapper.toEntities(any())).thenReturn(foundPeriode);

        List<PhaseDTO> result = phaseCommandJpaAdapter.updateAll(expectedResult);
        assertEquals(expectedResult, result);
    }

    @Test(expected = LowerVersionException.class)
    public void deleteById_NullParameterException()
    {
        phaseDTO.setVersion(2l);

        when(phaseRepository.findById(any())).thenReturn(Optional.of(phaseEntity));

        PhaseDTO result = phaseCommandJpaAdapter.update(phaseDTO);

        Assert.assertEquals(result, phaseDTO);
        Assert.assertEquals(result.getId(), phaseDTO.getId());
        Assert.assertEquals(result.getAbreviation(), phaseDTO.getAbreviation());
        Assert.assertEquals(result.getVersion(), phaseDTO.getVersion());
        Assert.assertEquals(result.getIndexCouleur(), phaseDTO.getIndexCouleur());
    }

    @Test(expected = NullParameterException.class)
    public void update_NullParameter()
    {
        PhaseDTO result = phaseCommandJpaAdapter.save(null);
    }

    @Test(expected = IDNotFoundException.class)
    public void update_IDNotFoundException()
    {
        PhaseDTO result = phaseCommandJpaAdapter.update(phaseDTO);
    }

    @Test(expected = AlreadyExistsException.class)
    public void save_AlreadyExistsId()
    {
        //GIVEN
        PhaseEntity phaseEntityForSaving = new PhaseEntity();

        when(phaseRepository.findById(any())).thenReturn(Optional.of(phaseEntityForSaving));

        //WHEN
        PhaseDTO result = phaseCommandJpaAdapter.save(phaseDTO);

    }
}