package fr.gouv.intradef.pesdt.service.queries;

import fr.gouv.intradef.pesdt.dto.PeriodeCycleDTO;
import fr.gouv.intradef.pesdt.dto.PhaseDTO;
import fr.gouv.intradef.pesdt.mappers.PeriodeCycleMapper;
import fr.gouv.intradef.pesdt.mappers.PhaseMapper;
import fr.gouv.intradef.pesdt.ports.spi.queries.PeriodeCycleQueryPersistencePort;
import fr.gouv.intradef.pesdt.ports.spi.queries.PhaseQueryPersistencePort;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PhaseQueryServiceImplTest {


    private MockMvc mockMvc;

    @InjectMocks
    PhaseQueryServiceImpl phaseQueryServiceImpl;

    @Mock
    private PhaseQueryPersistencePort phaseQueryPersistencePort;

    @Mock
    private PhaseMapper phaseMapper;

    private PhaseDTO phaseDTO;

    private PhaseDTO phaseDTO2;

    private List<PhaseDTO> phaseDTOList;


    @Before
    public void setUp() {

        phaseDTO = new PhaseDTO();
        phaseDTO.setId(1L);
        phaseDTO.setAbreviation("OPEX");
        phaseDTO.setDescription("OPEX (Opérations Extérieusr) sont les \"interventions des forces militaires françaises en dehors du territoire national\"");
        phaseDTO.setIndexCouleur(1);
        phaseDTO.setVersion(1l);

        phaseDTO2 = new PhaseDTO();
        phaseDTO2.setId(2L);
        phaseDTO2.setAbreviation("RCO");
        phaseDTO2.setDescription("Remise en Condition Operationnel");
        phaseDTO2.setIndexCouleur(2);
        phaseDTO2.setVersion(1l);

        phaseDTOList = new ArrayList<>();
        phaseDTOList.add(phaseDTO);
        phaseDTOList.add(phaseDTO2);
    }

    @Test
    public void testFindById() {

        when(phaseQueryPersistencePort.findById(phaseDTO.getId())).thenReturn(phaseDTO);

        PhaseDTO result = phaseQueryServiceImpl.findById(phaseDTO.getId());

        assertEquals(phaseDTO, result);
    }

    @Test
    public void testFindAll() {

        when(phaseQueryPersistencePort.findAll()).thenReturn(phaseDTOList);

        List<PhaseDTO> result = phaseQueryServiceImpl.findAll();

        assertEquals(phaseDTOList, result);
    }

    @Test
    public void testFindAllPage() {
        Pageable pageable = PageRequest.of(0, 1);
        Page<PhaseDTO> phaseDTOPage = new PageImpl<>(phaseDTOList,pageable,1);
        when(phaseQueryPersistencePort.findAllPage(null)).thenReturn(phaseDTOPage);

        Page<PhaseDTO> result = phaseQueryServiceImpl.findAllPage(null);

        assertEquals(phaseDTOPage, result);
    }

    @Test
    public void testFindByIndexCouleur() {

        when(phaseQueryPersistencePort.findByIndexCouleur(phaseDTO.getIndexCouleur())).thenReturn(phaseDTOList);

        List<PhaseDTO> result = phaseQueryServiceImpl.findByIndexCouleur(phaseDTO.getIndexCouleur());

        assertEquals(phaseDTO, result.get(0));
    }
}