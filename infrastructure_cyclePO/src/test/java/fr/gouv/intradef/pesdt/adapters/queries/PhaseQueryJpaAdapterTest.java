package fr.gouv.intradef.pesdt.adapters.queries;

import fr.gouv.intradef.pesdt.dto.PeriodeCycleDTO;
import fr.gouv.intradef.pesdt.dto.PhaseDTO;
import fr.gouv.intradef.pesdt.entity.PeriodeCycleEntity;
import fr.gouv.intradef.pesdt.entity.PhaseEntity;
import fr.gouv.intradef.pesdt.mappers.PeriodeCycleMapper;
import fr.gouv.intradef.pesdt.mappers.PhaseMapper;
import fr.gouv.intradef.pesdt.repository.PeriodeCycleRepository;
import fr.gouv.intradef.pesdt.repository.PhaseRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PhaseQueryJpaAdapterTest {

    @InjectMocks
    private PhaseQueryJpaAdapter phaseQueryJpaAdapter;

    @Mock
    private PhaseRepository phaseRepository;

    @Mock
    private PhaseMapper phaseMapper;

    private PhaseDTO phaseDTO;

    private PhaseDTO phaseDTO2;

    private List<PhaseDTO> phaseDTOList;

    private PhaseEntity phaseEntity;

    private PhaseEntity phaseEntity2;

    private List<PhaseEntity> phaseEntityList;


    @Before
    public void setUp() {
        MockitoAnnotations.openMocks(this);

        phaseDTO = new PhaseDTO();
        phaseDTO.setId(1L);
        phaseDTO.setAbreviation("OPEX");
        phaseDTO.setDescription("OPEX (Opérations Extérieusr) sont les \"interventions des forces militaires françaises en dehors du territoire national\"");
        phaseDTO.setIndexCouleur(1);
        phaseDTO.setVersion(1l);

        phaseDTO2 = new PhaseDTO();
        phaseDTO2.setId(2L);
        phaseDTO2.setAbreviation("RCO");
        phaseDTO2.setDescription("Remise en Condition Operationnel");
        phaseDTO2.setIndexCouleur(2);
        phaseDTO2.setVersion(1l);

        phaseDTOList = new ArrayList<>();
        phaseDTOList.add(phaseDTO);
        phaseDTOList.add(phaseDTO2);

        phaseEntity = new PhaseEntity();
        phaseEntity.setId(1L);
        phaseEntity.setAbreviation("OPEX");
        phaseEntity.setDescription("OPEX (Opérations Extérieusr) sont les \"interventions des forces militaires françaises en dehors du territoire national\"");
        phaseEntity.setIndexCouleur(1);
        phaseEntity.setVersion(1l);

        phaseEntity2 = new PhaseEntity();
        phaseEntity2.setId(2L);
        phaseEntity2.setAbreviation("RCO");
        phaseEntity2.setDescription("Remise en Condition Operationnel");
        phaseEntity2.setIndexCouleur(2);
        phaseEntity2.setVersion(1l);

        phaseEntityList = new ArrayList<>();
        phaseEntityList.add(phaseEntity);
        phaseEntityList.add(phaseEntity2);
    }

    @Test
    public void findAll() {
        when(phaseRepository.findAll()).thenReturn(phaseEntityList);
        when(phaseMapper.toDtos(phaseEntityList)).thenReturn(phaseDTOList);

        List<PhaseDTO> result = phaseQueryJpaAdapter.findAll();

        System.out.println("result" + result.get(0).getId());
    }

    @Test
    public void findAllPage() {
        Pageable pageable = PageRequest.of(0, 1);

        Page<PhaseEntity> phaseEntityPage = new PageImpl<>(phaseEntityList,pageable,1);

        when(phaseRepository.findAll(pageable)).thenReturn(phaseEntityPage);

        Page<PhaseDTO> result = phaseQueryJpaAdapter.findAllPage(pageable);

        assertEquals(phaseEntityPage.getContent().size(), result.getContent().size());
    }

    @Test
    public void findById() {
        when(phaseMapper.toDto(phaseEntity)).thenReturn(phaseDTO);
        when(phaseRepository.findById(phaseEntity.getId())).thenReturn(Optional.ofNullable(phaseEntity));

        PhaseDTO result = phaseQueryJpaAdapter.findById(phaseDTO.getId());

        Assert.assertEquals(result.getId(), phaseEntity.getId());
        Assert.assertEquals(result.getAbreviation(), phaseEntity.getAbreviation());
        Assert.assertEquals(result.getIndexCouleur(), phaseEntity.getIndexCouleur());
    }

    @Test
    public void findByIndexCouleur() {
        when(phaseRepository.findByIndexCouleur(phaseEntity.getIndexCouleur())).thenReturn(phaseEntityList);
        when(phaseMapper.toDtos(phaseEntityList)).thenReturn(phaseDTOList);

        List<PhaseDTO> result = phaseQueryJpaAdapter.findByIndexCouleur(phaseDTO.getIndexCouleur());

        Assert.assertEquals(result.get(0).getId(), phaseEntity.getId());
        Assert.assertEquals(result.get(0).getAbreviation(), phaseEntity.getAbreviation());
        Assert.assertEquals(result.get(0).getIndexCouleur(), phaseEntity.getIndexCouleur());
    }
}