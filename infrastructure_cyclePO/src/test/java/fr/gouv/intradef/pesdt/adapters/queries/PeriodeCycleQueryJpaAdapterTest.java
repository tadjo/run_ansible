package fr.gouv.intradef.pesdt.adapters.queries;

import fr.gouv.intradef.pesdt.dto.PeriodeCycleDTO;
import fr.gouv.intradef.pesdt.entity.PeriodeCycleEntity;
import fr.gouv.intradef.pesdt.mappers.PeriodeCycleMapper;
import fr.gouv.intradef.pesdt.repository.PeriodeCycleRepository;
import fr.gouv.intradef.pesdt.specification.PeriodeCycleSpecification;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

import java.time.LocalDateTime;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PeriodeCycleQueryJpaAdapterTest {

    @InjectMocks
    private PeriodeCycleQueryJpaAdapter periodeCycleQueryJpaAdapter;

    @Mock
    private PeriodeCycleRepository periodeCycleRepository;

    @Mock
    private PeriodeCycleMapper periodeCycleMapper;

    private List<PeriodeCycleDTO> periodeCycleDTOList;

    private List<PeriodeCycleEntity> periodeCycleEntityList;

    private PeriodeCycleDTO periodeCycleDTO;

    private PeriodeCycleEntity periodeCycleEntity;

    @Mock
    private Root<PeriodeCycleEntity> root;

    @Mock
    private CriteriaQuery<?> query;

    @Mock
    private CriteriaBuilder builder;

    List<Integer> organismeIdList = new ArrayList<>();

    List<Integer> phaseIdList = new ArrayList<>();

    List<LocalDateTime> datesDeDebut = new ArrayList<>();

    List<LocalDateTime> datesDeFin = new ArrayList<>();

    @Before
    public void prepare(){

        MockitoAnnotations.openMocks(this);

        periodeCycleDTO = new PeriodeCycleDTO();
        periodeCycleDTO.setId(1L);
        periodeCycleDTO.setDateDeDebut(LocalDateTime.of(2020, Month.FEBRUARY, 19, 0, 0));
        periodeCycleDTO.setDateDeFin(LocalDateTime.of(2020, Month.FEBRUARY, 25, 0, 0));
        periodeCycleDTO.setDescription("Test 01");
        periodeCycleDTO.setOrganismeId(50);
        periodeCycleDTO.setVersion(1l);

        PeriodeCycleDTO periodeCycleDTO2 = new PeriodeCycleDTO();
        periodeCycleDTO2.setId(2L);
        periodeCycleDTO2.setDateDeDebut(LocalDateTime.of(2021, Month.FEBRUARY, 19, 0, 0));
        periodeCycleDTO2.setDateDeFin(LocalDateTime.of(2021, Month.FEBRUARY, 25, 0, 0));
        periodeCycleDTO2.setDescription("Test 02");
        periodeCycleDTO2.setOrganismeId(55);
        periodeCycleDTO2.setVersion(1l);

        periodeCycleDTOList = new ArrayList<>();
        periodeCycleDTOList.add(periodeCycleDTO);
        periodeCycleDTOList.add(periodeCycleDTO2);

        periodeCycleEntity = new PeriodeCycleEntity();
        periodeCycleEntity.setId(1L);
        periodeCycleEntity.setDateDeDebut(LocalDateTime.of(2020, Month.FEBRUARY, 19, 0, 0));
        periodeCycleEntity.setDateDeFin(LocalDateTime.of(2020, Month.FEBRUARY, 25, 0, 0));
        periodeCycleEntity.setDescription("test 1 période de cycle enregistre en bdd");
        periodeCycleEntity.setOrganismeId(50);
        periodeCycleEntity.setPhaseId(1);
        periodeCycleEntity.setVersion(1l);

        PeriodeCycleEntity periodeCycleEntity2 = new PeriodeCycleEntity();
        periodeCycleEntity2.setId(2L);
        periodeCycleEntity2.setDateDeDebut(LocalDateTime.of(2021, Month.FEBRUARY, 19, 0, 0));
        periodeCycleEntity2.setDateDeFin(LocalDateTime.of(2021, Month.FEBRUARY, 25, 0, 0));
        periodeCycleEntity2.setDescription("test 2 période de cycle enregistre en bdd");
        periodeCycleEntity2.setOrganismeId(55);
        periodeCycleEntity2.setPhaseId(2);
        periodeCycleEntity2.setVersion(1l);

        periodeCycleEntityList = new ArrayList<>();
        periodeCycleEntityList.add(periodeCycleEntity);
        periodeCycleEntityList.add(periodeCycleEntity2);

        organismeIdList = new ArrayList<>();
        organismeIdList.add(55);
        organismeIdList.add(55);

        phaseIdList = new ArrayList<>();
        phaseIdList.add(1);
        phaseIdList.add(2);

        datesDeDebut = new ArrayList<>();
        datesDeDebut.add(LocalDateTime.of(2020, Month.FEBRUARY, 19, 0, 0));
        datesDeDebut.add(LocalDateTime.of(2021, Month.FEBRUARY, 19, 0, 0));

        datesDeFin = new ArrayList<>();
        datesDeFin.add(LocalDateTime.of(2020, Month.FEBRUARY, 25, 0, 0));
        datesDeFin.add(LocalDateTime.of(2021, Month.FEBRUARY, 25, 0, 0));

    }

    @Test
    public void findAll() {

        when(periodeCycleRepository.findAll()).thenReturn(periodeCycleEntityList);
        when(periodeCycleMapper.toDtos(periodeCycleEntityList)).thenReturn(periodeCycleDTOList);

        List<PeriodeCycleDTO> result = periodeCycleQueryJpaAdapter.findAll();

        System.out.println("result" + result.get(0).getId());
    }

    @Test
    public void findById() {
        when(periodeCycleMapper.toDto(periodeCycleEntity)).thenReturn(periodeCycleDTO);
        when(periodeCycleRepository.findById(periodeCycleEntity.getId())).thenReturn(Optional.ofNullable(periodeCycleEntity));

        PeriodeCycleDTO result = periodeCycleQueryJpaAdapter.findById(periodeCycleDTO.getId());

        Assert.assertEquals(result.getId(), periodeCycleEntity.getId());
        Assert.assertEquals(result.getDateDeDebut(), periodeCycleEntity.getDateDeDebut());
        Assert.assertEquals(result.getDateDeFin(), periodeCycleEntity.getDateDeFin());
        Assert.assertEquals(result.getPhase(), periodeCycleEntity.getPhase());
    }

    @Test
    public void findByOrganismeId() {

        when(periodeCycleRepository.findAllByOrganismeId(periodeCycleEntity.getOrganismeId())).thenReturn(periodeCycleEntityList);
        when(periodeCycleMapper.toDtos(periodeCycleEntityList)).thenReturn(periodeCycleDTOList);

        List<PeriodeCycleDTO> result = periodeCycleQueryJpaAdapter.findByOrganismeId(periodeCycleDTO.getOrganismeId());

        Assert.assertEquals(result.get(0).getId(), periodeCycleEntity.getId());
        Assert.assertEquals(result.get(0).getDateDeDebut(), periodeCycleEntity.getDateDeDebut());
        Assert.assertEquals(result.get(0).getDateDeFin(), periodeCycleEntity.getDateDeFin());
        Assert.assertEquals(result.get(0).getPhase(), periodeCycleEntity.getPhase());
    }

    @Test
    public void findAllPageByFilter() {

        // arrange
        Pageable pageable = PageRequest.of(0, 10);
        List<Integer> organismeIdList = new ArrayList<>();
        organismeIdList.add(55);
        organismeIdList.add(55);

        List<Integer> phaseIdList = new ArrayList<>();
        phaseIdList.add(1);
        phaseIdList.add(2);

        List<LocalDateTime> datesDeDebut = new ArrayList<>();
        datesDeDebut.add(LocalDateTime.of(2020, Month.FEBRUARY, 19, 0, 0));
        datesDeDebut.add(LocalDateTime.of(2021, Month.FEBRUARY, 19, 0, 0));

        List<LocalDateTime> datesDeFin = new ArrayList<>();
        datesDeFin.add(LocalDateTime.of(2020, Month.FEBRUARY, 25, 0, 0));
        datesDeFin.add(LocalDateTime.of(2021, Month.FEBRUARY, 25, 0, 0));

        Page<PeriodeCycleEntity> entityPage = new PageImpl<>(periodeCycleEntityList, pageable, periodeCycleEntityList.size());

        when(periodeCycleRepository.findAll(any(Specification.class), any(Pageable.class))).thenReturn(entityPage);
        when(periodeCycleMapper.toDto(any(PeriodeCycleEntity.class))).thenReturn(new PeriodeCycleDTO());

        Page<PeriodeCycleDTO> result = periodeCycleQueryJpaAdapter.findAllPageByFilter(pageable, organismeIdList, phaseIdList, datesDeDebut, datesDeFin);

        assertEquals(entityPage.getContent().size(), result.getContent().size());
    }


    @Test
    public void findAllPage() {

        Pageable pageable = PageRequest.of(0, 1);

        Page<PeriodeCycleEntity> periodeDTOPage = new PageImpl<>(periodeCycleEntityList,pageable,1);

        when(periodeCycleRepository.findAll(pageable)).thenReturn(periodeDTOPage);

        Page<PeriodeCycleDTO> result = periodeCycleQueryJpaAdapter.findAllPage(pageable);

        assertEquals(periodeDTOPage.getContent().size(), result.getContent().size());
    }

    @Test(expected=NullPointerException.class)
    public void testSpecificationDatesDeDebut() {
        Specification<PeriodeCycleEntity> specificationNull = PeriodeCycleSpecification.filterDateDeDebutIn(null);
        Predicate predicateNull = specificationNull.toPredicate(root, query, builder);

        Specification<PeriodeCycleEntity> specificationdDatesDeDebut = PeriodeCycleSpecification.filterDateDeDebutIn(datesDeDebut);
        Predicate predicateDatesDeDebut = specificationdDatesDeDebut.toPredicate(root, query, builder);
    }

    @Test(expected=NullPointerException.class)
    public void testSpecificationDatesDeFin() {
        Specification<PeriodeCycleEntity> specificationNull = PeriodeCycleSpecification.filterDateDeFinIn(null);
        Predicate predicateNull = specificationNull.toPredicate(root, query, builder);

        Specification<PeriodeCycleEntity> specificationdDatesDeFin = PeriodeCycleSpecification.filterDateDeFinIn(datesDeFin);
        Predicate predicateDatesDeFin = specificationdDatesDeFin.toPredicate(root, query, builder);
    }

    @Test(expected=NullPointerException.class)
    public void testSpecificationOrganismeIdList() {
        Specification<PeriodeCycleEntity> specificationNull = PeriodeCycleSpecification.filterOrganismeIdIn(null);
        Predicate predicateNull = specificationNull.toPredicate(root, query, builder);

        Specification<PeriodeCycleEntity> specificationdOrganismeIdList = PeriodeCycleSpecification.filterOrganismeIdIn(organismeIdList);
        Predicate predicateOrganismeIdList = specificationdOrganismeIdList.toPredicate(root, query, builder);
    }

    @Test(expected=NullPointerException.class)
    public void testSpecificationPhaseIdList() {
        Specification<PeriodeCycleEntity> specificationNull = PeriodeCycleSpecification.filterPhaseIdIn(null);
        Predicate predicateNull = specificationNull.toPredicate(root, query, builder);

        Specification<PeriodeCycleEntity> specificationdPhaseIdList = PeriodeCycleSpecification.filterPhaseIdIn(phaseIdList);
        Predicate predicatePhaseIdList = specificationdPhaseIdList.toPredicate(root, query, builder);
    }
}