package fr.gouv.intradef.pesdt.service.commands;

import fr.gouv.intradef.pesdt.dto.PeriodeCycleDTO;
import fr.gouv.intradef.pesdt.mappers.PeriodeCycleMapper;
import fr.gouv.intradef.pesdt.ports.spi.commands.PeriodeCycleCommandPersistencePort;
import fr.gouv.intradef.pesdt.ports.spi.queries.PeriodeCycleQueryPersistencePort;
import fr.gouv.intradef.pesdt.service.queries.PeriodeCycleQueryServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.time.LocalDateTime;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PeriodeCycleCommandServiceImplTest {

    private MockMvc mockMvc;
    @InjectMocks
    PeriodeCycleCommandServiceImpl periodeCycleCommandService;

    @Mock
    private PeriodeCycleCommandPersistencePort periodeCycleCommandPersistencePort;

    @Mock
    private PeriodeCycleMapper periodeCycleMapper;

    private List<PeriodeCycleDTO> periodeCycleDTOList;

    private List<PeriodeCycleDTO> periodeCycleDTOListRegistred;

    @Before
    public void prepare(){

            this.mockMvc = MockMvcBuilders.standaloneSetup(periodeCycleCommandService).build();

            PeriodeCycleDTO periodeCycleDTO = new PeriodeCycleDTO();
            periodeCycleDTO.setId(1L);
            periodeCycleDTO.setDateDeDebut(LocalDateTime.of(2020, Month.FEBRUARY, 19, 0, 0));
            periodeCycleDTO.setDateDeFin(LocalDateTime.of(2020, Month.FEBRUARY, 25, 0, 0));
            periodeCycleDTO.setDescription("Test 01");
            periodeCycleDTO.setVersion(1l);

            PeriodeCycleDTO periodeCycleDTO2 = new PeriodeCycleDTO();
            periodeCycleDTO2.setId(2L);
            periodeCycleDTO2.setDateDeDebut(LocalDateTime.of(2021, Month.FEBRUARY, 19, 0, 0));
            periodeCycleDTO2.setDateDeFin(LocalDateTime.of(2021, Month.FEBRUARY, 25, 0, 0));
            periodeCycleDTO2.setDescription("Test 02");
            periodeCycleDTO2.setVersion(1l);

            periodeCycleDTOList = new ArrayList<>();
            periodeCycleDTOList.add(periodeCycleDTO);
            periodeCycleDTOList.add(periodeCycleDTO2);

            PeriodeCycleDTO periodeCycleDtoRegistred = new PeriodeCycleDTO();
            periodeCycleDtoRegistred.setId(1L);
            periodeCycleDtoRegistred.setDateDeDebut(LocalDateTime.of(2020, Month.FEBRUARY, 19, 0, 0));
            periodeCycleDtoRegistred.setDateDeFin(LocalDateTime.of(2020, Month.FEBRUARY, 25, 0, 0));
            periodeCycleDtoRegistred.setDescription("test 1 période de cycle enregistre en bdd");
            periodeCycleDtoRegistred.setVersion(1l);

            PeriodeCycleDTO periodeCycleDtoRegistred2 = new PeriodeCycleDTO();
            periodeCycleDtoRegistred2.setId(2L);
            periodeCycleDtoRegistred2.setDateDeDebut(LocalDateTime.of(2021, Month.FEBRUARY, 19, 0, 0));
            periodeCycleDtoRegistred2.setDateDeFin(LocalDateTime.of(2021, Month.FEBRUARY, 25, 0, 0));
            periodeCycleDtoRegistred2.setDescription("test 2 période de cycle enregistre en bdd");
            periodeCycleDtoRegistred2.setVersion(1l);

            periodeCycleDTOListRegistred = new ArrayList<>();
            periodeCycleDTOListRegistred.add(periodeCycleDtoRegistred);
            periodeCycleDTOListRegistred.add(periodeCycleDtoRegistred2);

    }

    @Test
    public void save() {
        PeriodeCycleDTO periodeCycleDTO = periodeCycleDTOList.get(0);

        PeriodeCycleDTO periodeCycleDTOReturned = new PeriodeCycleDTO();
        periodeCycleDTOReturned.setId(1L);
        periodeCycleDTOReturned.setDateDeDebut(LocalDateTime.of(2020, Month.FEBRUARY, 19, 0, 0));
        periodeCycleDTOReturned.setDateDeFin(LocalDateTime.of(2020, Month.FEBRUARY, 25, 0, 0));
        periodeCycleDTOReturned.setDescription("Test 01");
        periodeCycleDTOReturned.setVersion(1l);


        when(periodeCycleCommandPersistencePort.save(periodeCycleDTO)).thenReturn(periodeCycleDTOReturned);

        PeriodeCycleDTO result = periodeCycleCommandService.save(periodeCycleDTO);

        Assert.assertEquals(result, periodeCycleDTOReturned);
        Assert.assertEquals(result.hashCode(), periodeCycleDTOReturned.hashCode());
        Assert.assertEquals(result.getId(), periodeCycleDTOReturned.getId());
        Assert.assertEquals(result.getDateDeDebut(), periodeCycleDTOReturned.getDateDeDebut());
        Assert.assertEquals(result.getDateDeFin(), periodeCycleDTOReturned.getDateDeFin());
        Assert.assertEquals(result.getPhase(), periodeCycleDTOReturned.getPhase());
        Assert.assertTrue(result.equals(periodeCycleDTOReturned));

    }

    @Test
    public void saveAll() {

        when(periodeCycleCommandPersistencePort.saveAll(periodeCycleDTOList)).thenReturn(periodeCycleDTOListRegistred);

        List<PeriodeCycleDTO> result = periodeCycleCommandService.saveAll(periodeCycleDTOList);

        Assert.assertEquals(result, periodeCycleDTOListRegistred);
        Assert.assertEquals(result.get(0).getId(), periodeCycleDTOListRegistred.get(0).getId());
        Assert.assertEquals(result.get(0).getDateDeDebut(), periodeCycleDTOListRegistred.get(0).getDateDeDebut());
        Assert.assertEquals(result.get(0).getDateDeFin(), periodeCycleDTOListRegistred.get(0).getDateDeFin());
        Assert.assertEquals(result.get(0).getPhase(), periodeCycleDTOListRegistred.get(0).getPhase());
    }

    @Test
    public void update() {
        PeriodeCycleDTO periodeCycleDTO = periodeCycleDTOList.get(0);

        PeriodeCycleDTO periodeCycleDTOReturned = new PeriodeCycleDTO();
        periodeCycleDTOReturned.setId(1L);
        periodeCycleDTOReturned.setDateDeDebut(LocalDateTime.of(2020, Month.FEBRUARY, 19, 0, 0));
        periodeCycleDTOReturned.setDateDeFin(LocalDateTime.of(2020, Month.FEBRUARY, 25, 0, 0));
        periodeCycleDTOReturned.setDescription("Test 01");
        periodeCycleDTOReturned.setVersion(1l);

        when(periodeCycleCommandPersistencePort.update(periodeCycleDTO)).thenReturn(periodeCycleDTOReturned);

        PeriodeCycleDTO result = periodeCycleCommandService.update(periodeCycleDTO);

        Assert.assertEquals(result, periodeCycleDTOReturned);
        Assert.assertEquals(result.getId(), periodeCycleDTOReturned.getId());
        Assert.assertEquals(result.getDateDeDebut(), periodeCycleDTOReturned.getDateDeDebut());
        Assert.assertEquals(result.getDateDeFin(), periodeCycleDTOReturned.getDateDeFin());
        Assert.assertEquals(result.getPhase(), periodeCycleDTOReturned.getPhase());
    }

    @Test
    public void deleteById() {
        PeriodeCycleDTO periodeCycleDTO = periodeCycleDTOList.get(0);

        PeriodeCycleDTO periodeCycleDTOReturned = new PeriodeCycleDTO();
        periodeCycleDTOReturned.setId(1L);
        periodeCycleDTOReturned.setDateDeDebut(LocalDateTime.of(2020, Month.FEBRUARY, 19, 0, 0));
        periodeCycleDTOReturned.setDateDeFin(LocalDateTime.of(2020, Month.FEBRUARY, 25, 0, 0));
        periodeCycleDTOReturned.setDescription("Test 01");
        periodeCycleDTOReturned.setVersion(1l);

        when(periodeCycleCommandPersistencePort.deleteById(periodeCycleDTO.getId())).thenReturn(periodeCycleDTOReturned);

        PeriodeCycleDTO result = periodeCycleCommandService.deleteById(periodeCycleDTO.getId());

        Assert.assertEquals(result, periodeCycleDTOReturned);
        Assert.assertEquals(result.getId(), periodeCycleDTOReturned.getId());
        Assert.assertEquals(result.getDateDeDebut(), periodeCycleDTOReturned.getDateDeDebut());
        Assert.assertEquals(result.getDateDeFin(), periodeCycleDTOReturned.getDateDeFin());
        Assert.assertEquals(result.getPhase(), periodeCycleDTOReturned.getPhase());
    }
}