package fr.gouv.intradef.pesdt.service.queries;

import fr.gouv.intradef.pesdt.dto.PeriodeCycleCheckedDTO;
import fr.gouv.intradef.pesdt.dto.PeriodeCycleDTO;
import fr.gouv.intradef.pesdt.mappers.PeriodeCycleMapper;
import fr.gouv.intradef.pesdt.ports.spi.queries.PeriodeCycleQueryPersistencePort;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.time.LocalDateTime;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PeriodeCycleQueryServiceImplTest {

    private MockMvc mockMvc;
    @InjectMocks
    PeriodeCycleQueryServiceImpl periodeCycleQueryService;

    @Mock
    private PeriodeCycleQueryPersistencePort periodeCycleQueryPersistencePort;

    @Mock
    private PeriodeCycleMapper periodeCycleMapper;

    private List<PeriodeCycleDTO> periodeCycleDTOList;

    private List<PeriodeCycleDTO> periodeCycleDTOListRegistred;

    @Before
    public void prepare()
    {
        this.mockMvc = MockMvcBuilders.standaloneSetup(periodeCycleQueryService).build();

        PeriodeCycleDTO periodeCycleDTO = new PeriodeCycleDTO();
        periodeCycleDTO.setId(1L);
        periodeCycleDTO.setDateDeDebut(LocalDateTime.of(2020, Month.FEBRUARY, 19, 0, 0));
        periodeCycleDTO.setDateDeFin(LocalDateTime.of(2020, Month.FEBRUARY, 25, 0, 0));
        periodeCycleDTO.setDescription("Test 01");
        periodeCycleDTO.setVersion(1l);

        PeriodeCycleDTO periodeCycleDTO2 = new PeriodeCycleDTO();
        periodeCycleDTO2.setId(2L);
        periodeCycleDTO2.setDateDeDebut(LocalDateTime.of(2021, Month.FEBRUARY, 19, 0, 0));
        periodeCycleDTO2.setDateDeFin(LocalDateTime.of(2021, Month.FEBRUARY, 25, 0, 0));
        periodeCycleDTO2.setDescription("Test 02");
        periodeCycleDTO2.setVersion(1l);

        periodeCycleDTOList = new ArrayList<>();
        periodeCycleDTOList.add(periodeCycleDTO);
        periodeCycleDTOList.add(periodeCycleDTO2);

        PeriodeCycleDTO periodeCycleDtoRegistred = new PeriodeCycleDTO();
        periodeCycleDtoRegistred.setId(1L);
        periodeCycleDtoRegistred.setDateDeDebut(LocalDateTime.of(2020, Month.FEBRUARY, 19, 0, 0));
        periodeCycleDtoRegistred.setDateDeFin(LocalDateTime.of(2020, Month.FEBRUARY, 25, 0, 0));
        periodeCycleDtoRegistred.setDescription("test 1 période de cycle enregistre en bdd");
        periodeCycleDtoRegistred.setVersion(1l);

        PeriodeCycleDTO periodeCycleDtoRegistred2 = new PeriodeCycleDTO();
        periodeCycleDtoRegistred2.setId(2L);
        periodeCycleDtoRegistred2.setDateDeDebut(LocalDateTime.of(2021, Month.FEBRUARY, 19, 0, 0));
        periodeCycleDtoRegistred2.setDateDeFin(LocalDateTime.of(2021, Month.FEBRUARY, 25, 0, 0));
        periodeCycleDtoRegistred2.setDescription("test 2 période de cycle enregistre en bdd");
        periodeCycleDtoRegistred2.setVersion(1l);

        periodeCycleDTOListRegistred = new ArrayList<>();
        periodeCycleDTOListRegistred.add(periodeCycleDtoRegistred);
        periodeCycleDTOListRegistred.add(periodeCycleDtoRegistred2);
    }

    @Test
    public void checkPeriodeCycleBeforeRegistred_All_KO() {

        //GIVEN
        when(periodeCycleQueryPersistencePort.findByOrganismeId(57)).thenReturn(periodeCycleDTOListRegistred);

        //WHEN
        List<PeriodeCycleDTO> periodeCycleDTOListReturned = periodeCycleQueryService.checkPeriodeCycleBeforeRegistred(57,periodeCycleDTOList);

        //THEN
        Assert.assertNotNull("la liste de périodes est vide", periodeCycleDTOListReturned);
        Assert.assertEquals(periodeCycleDTOListReturned.stream().count(), 2);
        Assert.assertEquals(periodeCycleDTOListReturned.get(0).getId(), periodeCycleDTOList.get(0).getId());
        Assert.assertEquals(periodeCycleDTOListReturned.get(0).getDateDeDebut(), periodeCycleDTOList.get(0).getDateDeDebut());
        Assert.assertEquals(periodeCycleDTOListReturned.get(0).getDateDeFin(), periodeCycleDTOList.get(0).getDateDeFin());
        Assert.assertEquals(periodeCycleDTOListReturned.get(1).getId(), periodeCycleDTOList.get(1).getId());
        Assert.assertEquals(periodeCycleDTOListReturned.get(1).getDateDeDebut(), periodeCycleDTOList.get(1).getDateDeDebut());
        Assert.assertEquals(periodeCycleDTOListReturned.get(1).getDateDeFin(), periodeCycleDTOList.get(1).getDateDeFin());



    }

    @Test
    public void checkPeriodeCycleBeforeRegistred_Two_OK_And_Three_KO() {

        // Periode de cycle avec des dates valide
        PeriodeCycleDTO periodeCycleDto3 = new PeriodeCycleDTO();
        periodeCycleDto3.setId(3L);
        periodeCycleDto3.setDateDeDebut(LocalDateTime.of(2019, Month.FEBRUARY, 19, 0, 0));
        periodeCycleDto3.setDateDeFin(LocalDateTime.of(2020, Month.FEBRUARY, 18, 0, 0));
        periodeCycleDto3.setDescription("Test 03");
        periodeCycleDto3.setVersion(1l);

        // Periode de cycle avec des dates valide
        PeriodeCycleDTO periodeCycleDto4 = new PeriodeCycleDTO();
        periodeCycleDto4.setId(4L);
        periodeCycleDto4.setDateDeDebut(LocalDateTime.of(2021, Month.FEBRUARY, 26, 0, 0));
        periodeCycleDto4.setDateDeFin(LocalDateTime.of(2021, Month.SEPTEMBER, 26, 0, 0));
        periodeCycleDto4.setDescription("Test 04");
        periodeCycleDto4.setVersion(1l);

        periodeCycleDTOList.add(periodeCycleDto3);
        periodeCycleDTOList.add(periodeCycleDto4);

        //GIVEN
        when(periodeCycleQueryPersistencePort.findByOrganismeId(57)).thenReturn(periodeCycleDTOListRegistred);

        //WHEN
        List<PeriodeCycleDTO> periodeCycleDTOListReturned = periodeCycleQueryService.checkPeriodeCycleBeforeRegistred(57,periodeCycleDTOList);

        //THEN
        Assert.assertNotNull("la liste de périodes est vide", periodeCycleDTOListReturned);
        Assert.assertEquals(periodeCycleDTOListReturned.stream().count(), 2);

    }

    @Test
    public void findAll() {

        //GIVEN
        when(periodeCycleQueryPersistencePort.findAll()).thenReturn(periodeCycleDTOListRegistred);

        //WHEN
        List<PeriodeCycleDTO> periodeCycleDTOListReturned = periodeCycleQueryService.findAll();

        //THEN
        Assert.assertNotNull("la liste de périodes est vide", periodeCycleDTOListReturned);
        Assert.assertEquals(periodeCycleDTOListReturned.stream().count(), 2);
    }

    @Test
    public void findAllPageByFilter() {
        List<Integer> organismeIdList = new ArrayList<>();
        organismeIdList.add(57);
        organismeIdList.add(18);

        List<Integer> phaseIdList = new ArrayList<>();
        phaseIdList.add(1);
        phaseIdList.add(2);

        List<LocalDateTime> datesDeDebut = new ArrayList<>();
        datesDeDebut.add(LocalDateTime.of(2020, Month.FEBRUARY, 19, 0, 0));
        datesDeDebut.add(LocalDateTime.of(2021, Month.FEBRUARY, 19, 0, 0));

        List<LocalDateTime> datesDeFin = new ArrayList<>();
        datesDeFin.add(LocalDateTime.of(2020, Month.FEBRUARY, 25, 0, 0));
        datesDeFin.add(LocalDateTime.of(2021, Month.FEBRUARY, 25, 0, 0));

        Pageable pageable = PageRequest.of(0, 1);
        Page<PeriodeCycleDTO> periodeDTOPage = new PageImpl<>(periodeCycleDTOListRegistred,pageable,1);

        when(periodeCycleQueryPersistencePort.findAllPageByFilter(pageable,organismeIdList,phaseIdList,datesDeDebut,datesDeFin)).thenReturn(periodeDTOPage);

        Page<PeriodeCycleDTO> result = periodeCycleQueryService.findAllPageByFilter(pageable,organismeIdList,phaseIdList,datesDeDebut,datesDeFin);

        assertEquals(periodeDTOPage, result);
    }

    @Test
    public void findAllPage() {
        Pageable pageable = PageRequest.of(0, 1);
        Page<PeriodeCycleDTO> periodeDTOPage = new PageImpl<>(periodeCycleDTOListRegistred,pageable,1);

        when(periodeCycleQueryPersistencePort.findAllPage(pageable)).thenReturn(periodeDTOPage);

        Page<PeriodeCycleDTO> result = periodeCycleQueryService.findAllPage(pageable);

        assertEquals(periodeDTOPage, result);
    }

    @Test
    public void findById() {
        PeriodeCycleDTO periodeCycleDTO = periodeCycleDTOListRegistred.get(0);

        when(periodeCycleQueryPersistencePort.findById(periodeCycleDTO.getId())).thenReturn(periodeCycleDTO);

        PeriodeCycleDTO result = periodeCycleQueryService.findById(periodeCycleDTO.getId());

        assertEquals(periodeCycleDTO, result);
    }

    @Test
    public void checkDateDebutEtDeFin() {

        // Periode de cycle avec des dates valide
        PeriodeCycleDTO periodeCycleDto3 = new PeriodeCycleDTO();
        periodeCycleDto3.setId(3L);
        periodeCycleDto3.setDateDeDebut(LocalDateTime.of(2020, Month.FEBRUARY, 24, 0, 0));
        periodeCycleDto3.setDateDeFin(LocalDateTime.of(2021, Month.FEBRUARY, 20, 0, 0));
        periodeCycleDto3.setDescription("Test 03");
        periodeCycleDto3.setOrganismeId(57);
        periodeCycleDto3.setVersion(1l);

        //GIVEN
        when(periodeCycleQueryPersistencePort.findByOrganismeId(57)).thenReturn(periodeCycleDTOList);

        //WHEN
       PeriodeCycleCheckedDTO periodeCycleChecked = periodeCycleQueryService.checkDateDebutEtDeFin(periodeCycleDto3);

        //THEN
        Assert.assertNotNull(periodeCycleChecked);
        Assert.assertEquals(true, periodeCycleChecked.getIsExist());
        Assert.assertEquals(2, periodeCycleChecked.getPeriodeCycleList().size());
        Assert.assertEquals(2, periodeCycleChecked.getPeriodeCycleList().get(0).getId().longValue());
        Assert.assertEquals(1, periodeCycleChecked.getPeriodeCycleList().get(1).getId().longValue());
    }


}