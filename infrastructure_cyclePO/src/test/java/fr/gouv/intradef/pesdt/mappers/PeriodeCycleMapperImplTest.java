package fr.gouv.intradef.pesdt.mappers;
import fr.gouv.intradef.pesdt.dto.PeriodeCycleDTO;
import fr.gouv.intradef.pesdt.dto.PhaseDTO;
import fr.gouv.intradef.pesdt.entity.PeriodeCycleEntity;
import fr.gouv.intradef.pesdt.entity.PhaseEntity;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


import java.time.LocalDateTime;
import java.time.Month;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static fr.gouv.intradef.pesdt.common.domain.utils.AssertionUtils.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {PeriodeCycleMapperImpl.class, PhaseMapperImpl.class})
public class PeriodeCycleMapperImplTest {


    @Autowired
    private PeriodeCycleMapper periodeCycleMapper;

    @Autowired
    private PhaseMapper phaseMapper;

    private PhaseEntity phaseEntity;

    private PeriodeCycleEntity periodeCycleEntity;

    private PeriodeCycleEntity periodeCycleEntity2;

    private PeriodeCycleDTO periodeCycleDTO;

    private PeriodeCycleDTO periodeCycleDTO2;

    private PhaseDTO phaseDTO;

    private PhaseDTO phaseDTO2;

    private List<PeriodeCycleEntity> periodeCycleEntityList;

    private List<PeriodeCycleDTO> periodeCycleDtoList;

    private List<PhaseDTO> phaseDTOList;

    private List<PhaseEntity> phaseEntityList;


    @Before
    public void setUp() throws Exception {
        //GIVEN
        periodeCycleDTO = new PeriodeCycleDTO();
        periodeCycleDTO.setId(1L);
        periodeCycleDTO.setDateDeDebut(LocalDateTime.of(2020, Month.FEBRUARY, 19, 0, 0));
        periodeCycleDTO.setDateDeFin(LocalDateTime.of(2020, Month.FEBRUARY, 28, 0, 0));
        periodeCycleDTO.setDescription("test periode de cycle 1");
        periodeCycleDTO.setOrganismeId(57);
        periodeCycleDTO.setVersion(1l);

        phaseDTO = new PhaseDTO();
        phaseDTO.setId(1L);
        phaseDTO.setAbreviation("OPEX");
        phaseDTO.setDescription("OPEX (Opérations Extérieusr) sont les \"interventions des forces militaires françaises en dehors du territoire national\"");
        phaseDTO.setIndexCouleur(1);
        phaseDTO.setVersion(1l);

        periodeCycleDTO.setPhase(phaseDTO);


        periodeCycleDTO2 = new PeriodeCycleDTO();
        periodeCycleDTO2.setId(2L);
        periodeCycleDTO2.setDateDeDebut(LocalDateTime.of(2021, Month.FEBRUARY, 19, 0, 0));
        periodeCycleDTO2.setDateDeFin(LocalDateTime.of(2021, Month.FEBRUARY, 28, 0, 0));
        periodeCycleDTO2.setDescription("Remise en Condition Operationnel");
        periodeCycleDTO2.setOrganismeId(57);
        periodeCycleDTO2.setVersion(1l);

        phaseDTO2 = new PhaseDTO();
        phaseDTO2.setId(2L);
        phaseDTO2.setAbreviation("RCO");
        phaseDTO2.setDescription("Remise en Condition Operationnel");
        phaseDTO2.setIndexCouleur(2);
        phaseDTO2.setVersion(1l);

        periodeCycleDTO2.setPhase(phaseDTO2);
        periodeCycleDtoList = new ArrayList<>();
        periodeCycleDtoList.add(periodeCycleDTO);
        periodeCycleDtoList.add(periodeCycleDTO2);

        phaseEntity = new PhaseEntity(1L,"OPEX (Opérations Extérieusr) sont les \"interventions des forces militaires françaises en dehors du territoire national\"",
                1,"OPEX");

        periodeCycleEntity = new PeriodeCycleEntity(1l,
                LocalDateTime.of(2020, Month.FEBRUARY, 19, 0, 0),
                LocalDateTime.of(2020, Month.FEBRUARY, 28, 0, 0),
                "test periode de cycle 1",1,"cvt",1,phaseEntity);

        periodeCycleEntity2 = new PeriodeCycleEntity();
        periodeCycleEntity2.setId(1L);
        periodeCycleEntity2.setDateDeDebut(LocalDateTime.of(2020, Month.FEBRUARY, 19, 0, 0));
        periodeCycleEntity2.setDateDeFin(LocalDateTime.of(2020, Month.FEBRUARY, 28, 0, 0));
        periodeCycleEntity2.setDescription("test periode de cycle 1");
        periodeCycleEntity2.setOrganismeId(57);
        periodeCycleEntity2.setVersion(1l);


        periodeCycleEntity2.setPhase(phaseEntity);
        periodeCycleEntityList = new ArrayList<>();
        periodeCycleEntityList.add(periodeCycleEntity);
        periodeCycleEntityList.add(periodeCycleEntity2);

    }

    @Test
    public void toEntity() {

        PeriodeCycleEntity periodeCycleEntity = periodeCycleMapper.toEntity(periodeCycleDTO);

        assertEquals(periodeCycleDTO.getId(), periodeCycleEntity.getId());
        assertEquals(periodeCycleDTO.getOrganismeId(), periodeCycleEntity.getOrganismeId());
        assertEquals(periodeCycleDTO.getDescription(), periodeCycleEntity.getDescription());
        assertEquals(periodeCycleDTO.getDateDeDebut(), periodeCycleEntity.getDateDeDebut());
        assertEquals(periodeCycleDTO.getDateDeFin(), periodeCycleEntity.getDateDeFin());

        assertEquals(periodeCycleDTO.getPhase().getId(), periodeCycleEntity.getPhase().getId());
        assertEquals(periodeCycleDTO.getPhase().getAbreviation(), periodeCycleEntity.getPhase().getAbreviation());
    }

    @Test
    public void toDto() {

        PeriodeCycleDTO periodeCycleDTO = periodeCycleMapper.toDto(periodeCycleEntity);

        assertEquals(periodeCycleDTO.getId(), periodeCycleEntity.getId());
        assertEquals(periodeCycleDTO.getOrganismeId(), periodeCycleEntity.getOrganismeId());
        assertEquals(periodeCycleDTO.getDescription(), periodeCycleEntity.getDescription());
        assertEquals(periodeCycleDTO.getDateDeDebut(), periodeCycleEntity.getDateDeDebut());
        assertEquals(periodeCycleDTO.getDateDeFin(), periodeCycleEntity.getDateDeFin());

        assertEquals(periodeCycleDTO.getPhase().getId(), periodeCycleEntity.getPhase().getId());
        assertEquals(periodeCycleDTO.getPhase().getAbreviation(), periodeCycleEntity.getPhase().getAbreviation());
    }

    @Test
    public void toDtos() {
        phaseDTOList = phaseMapper.toDtos(Collections.singletonList(phaseEntity));

        List<PeriodeCycleDTO> periodeCycleDTOS = periodeCycleMapper.toDtos(periodeCycleEntityList);

        assertEquals(periodeCycleDTOS.get(0).getId(), periodeCycleEntityList.get(0).getId());
        assertEquals(periodeCycleDTOS.get(0).getOrganismeId(), periodeCycleEntityList.get(0).getOrganismeId());
        assertEquals(periodeCycleDTOS.get(0).getDescription(), periodeCycleEntityList.get(0).getDescription());

        assertEquals(periodeCycleDTOS.get(0).getPhase().getId(), periodeCycleEntityList.get(0).getPhase().getId());
        assertEquals(periodeCycleDTOS.get(0).getPhase().getAbreviation(), periodeCycleEntityList.get(0).getPhase().getAbreviation());

        assertEquals(periodeCycleDTOS.get(1).getId(), periodeCycleEntityList.get(1).getId());
        assertEquals(periodeCycleDTOS.get(1).getOrganismeId(), periodeCycleEntityList.get(1).getOrganismeId());
        assertEquals(periodeCycleDTOS.get(1).getDescription(), periodeCycleEntityList.get(1).getDescription());

        assertEquals(periodeCycleDTOS.get(1).getPhase().getId(), periodeCycleEntityList.get(1).getPhase().getId());
        assertEquals(periodeCycleDTOS.get(1).getPhase().getAbreviation(), periodeCycleEntityList.get(1).getPhase().getAbreviation());
        assertEquals(phaseDTOList.get(0).getId(), phaseEntity.getId());
    }

    @Test
    public void toEntities() {
        phaseEntityList = phaseMapper.toEntities(Collections.singletonList(phaseDTO));

        List<PeriodeCycleEntity> periodeCycleEntities = periodeCycleMapper.toEntities(periodeCycleDtoList);

        assertEquals(periodeCycleEntities.get(0).getId(), periodeCycleDtoList.get(0).getId());
        assertEquals(periodeCycleEntities.get(0).getOrganismeId(), periodeCycleDtoList.get(0).getOrganismeId());
        assertEquals(periodeCycleEntities.get(0).getDescription(), periodeCycleDtoList.get(0).getDescription());

        assertEquals(periodeCycleEntities.get(0).getPhase().getId(), periodeCycleDtoList.get(0).getPhase().getId());
        assertEquals(periodeCycleEntities.get(0).getPhase().getAbreviation(), periodeCycleDtoList.get(0).getPhase().getAbreviation());

        assertEquals(periodeCycleEntities.get(1).getId(), periodeCycleDtoList.get(1).getId());
        assertEquals(periodeCycleEntities.get(1).getOrganismeId(), periodeCycleDtoList.get(1).getOrganismeId());
        assertEquals(periodeCycleEntities.get(1).getDescription(), periodeCycleDtoList.get(1).getDescription());

        assertEquals(periodeCycleEntities.get(1).getPhase().getId(), periodeCycleDtoList.get(1).getPhase().getId());
        assertEquals(periodeCycleEntities.get(1).getPhase().getAbreviation(), periodeCycleDtoList.get(1).getPhase().getAbreviation());
        assertEquals(phaseEntityList.get(0).getId(), phaseDTO.getId());
    }

    @Test
    public void toEntities_KO() {
        phaseEntityList = phaseMapper.toEntities(Collections.singletonList(null));

        List<PeriodeCycleEntity> periodeCycleEntities = periodeCycleMapper.toEntities(null);
        Assert.assertNull(periodeCycleEntities);
    }

    @Test
    public void toDtos_KO() {
        phaseDTOList = phaseMapper.toDtos(Collections.singletonList(null));

        List<PeriodeCycleDTO> periodeCycleDTOS = periodeCycleMapper.toDtos(null);
        Assert.assertNull(periodeCycleDTOS);
    }

    @Test
    public void toEntity_KO() {
        PhaseEntity phaseEntity = phaseMapper.toEntity(null);

        PeriodeCycleEntity periodeCycleEntity = periodeCycleMapper.toEntity(null);
        Assert.assertNull(periodeCycleEntity);
        Assert.assertNull(phaseEntity);
    }

    @Test
    public void toDto_KO() {
        PhaseDTO phaseDTO = phaseMapper.toDto(null);

        PeriodeCycleDTO periodeCycleDTO = periodeCycleMapper.toDto(null);
        Assert.assertNull(periodeCycleDTO);
        Assert.assertNull(phaseDTO);
    }

}