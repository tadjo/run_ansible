package fr.gouv.intradef.pesdt.service.commands;

import fr.gouv.intradef.pesdt.dto.PeriodeCycleDTO;
import fr.gouv.intradef.pesdt.dto.PhaseDTO;
import fr.gouv.intradef.pesdt.mappers.PhaseMapper;
import fr.gouv.intradef.pesdt.ports.spi.commands.PhaseCommandPersistencePort;
import fr.gouv.intradef.pesdt.ports.spi.queries.PhaseQueryPersistencePort;
import fr.gouv.intradef.pesdt.service.queries.PhaseQueryServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDateTime;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PhaseCommandServiceImplTest {


    private MockMvc mockMvc;

    @InjectMocks
    PhaseCommandServiceImpl phaseCommandServiceImpl;

    @Mock
    private PhaseCommandPersistencePort phaseCommandPersistencePort;

    @Mock
    private PhaseMapper phaseMapper;

    private PhaseDTO phaseDTO;

    private PhaseDTO phaseDTO2;

    private List<PhaseDTO> phaseDTOList;

    @Before
    public void setUp() {

        phaseDTO = new PhaseDTO();
        phaseDTO.setId(1L);
        phaseDTO.setAbreviation("OPEX");
        phaseDTO.setDescription("OPEX (Opérations Extérieusr) sont les \"interventions des forces militaires françaises en dehors du territoire national\"");
        phaseDTO.setIndexCouleur(1);
        phaseDTO.setVersion(1l);

        phaseDTO2 = new PhaseDTO();
        phaseDTO2.setId(2L);
        phaseDTO2.setAbreviation("RCO");
        phaseDTO2.setDescription("Remise en Condition Operationnel");
        phaseDTO2.setIndexCouleur(2);
        phaseDTO2.setVersion(1l);

        phaseDTOList = new ArrayList<>();
        phaseDTOList.add(phaseDTO);
        phaseDTOList.add(phaseDTO2);
    }

    @Test
    public void save() {
        PhaseDTO phaseDTO  = phaseDTOList.get(0);

        PhaseDTO phaseDTOReturned = new PhaseDTO();
        phaseDTO.setId(1L);
        phaseDTO.setAbreviation("OPEX");
        phaseDTO.setDescription("OPEX (Opérations Extérieusr) sont les \"interventions des forces militaires françaises en dehors du territoire national\"");
        phaseDTO.setIndexCouleur(1);
        phaseDTO.setVersion(1l);

        when(phaseCommandPersistencePort.save(phaseDTO)).thenReturn(phaseDTOReturned);

        PhaseDTO result = phaseCommandServiceImpl.save(phaseDTO);

        Assert.assertEquals(result, phaseDTOReturned);
        Assert.assertEquals(result.getId(), phaseDTOReturned.getId());
        Assert.assertEquals(result.getAbreviation(), phaseDTOReturned.getAbreviation());
        Assert.assertEquals(result.getIndexCouleur(), phaseDTOReturned.getIndexCouleur());
    }

    @Test
    public void saveAll() {

        PhaseDTO phaseDTOReturned = new PhaseDTO();
        phaseDTOReturned.setId(1L);
        phaseDTOReturned.setAbreviation("OPEX");
        phaseDTOReturned.setDescription("OPEX (Opérations Extérieusr) sont les \"interventions des forces militaires françaises en dehors du territoire national\"");
        phaseDTOReturned.setIndexCouleur(1);
        phaseDTOReturned.setVersion(1l);

        PhaseDTO phaseDTOReturned2 = new PhaseDTO();
        phaseDTOReturned2.setId(2L);
        phaseDTOReturned2.setAbreviation("RCO");
        phaseDTOReturned2.setDescription("Remise en Condition Operationnel");
        phaseDTOReturned2.setIndexCouleur(2);
        phaseDTOReturned2.setVersion(1l);

        List<PhaseDTO>  phaseDTOListReturned = new ArrayList<>();
        phaseDTOListReturned.add(phaseDTOReturned);
        phaseDTOListReturned.add(phaseDTOReturned2);

        when(phaseCommandPersistencePort.saveAll(phaseDTOList)).thenReturn(phaseDTOListReturned);

        List<PhaseDTO> result = phaseCommandServiceImpl.saveAll(phaseDTOList);

        Assert.assertEquals(result, phaseDTOListReturned);
        Assert.assertEquals(result.get(0).getId(), phaseDTOListReturned.get(0).getId());
        Assert.assertEquals(result.get(0).getAbreviation(), phaseDTOListReturned.get(0).getAbreviation());
        Assert.assertEquals(result.get(0).getIndexCouleur(), phaseDTOListReturned.get(0).getIndexCouleur());
    }

    @Test
    public void update() {

        PhaseDTO phaseDTOReturned = new PhaseDTO();
        phaseDTO.setId(1L);
        phaseDTO.setAbreviation("OPEX");
        phaseDTO.setDescription("OPEX (Opérations Extérieusr) sont les \"interventions des forces militaires françaises en dehors du territoire national\"");
        phaseDTO.setIndexCouleur(1);
        phaseDTO.setVersion(1l);

        when(phaseCommandPersistencePort.update(phaseDTO)).thenReturn(phaseDTOReturned);

        PhaseDTO result = phaseCommandServiceImpl.update(phaseDTO);

        Assert.assertEquals(result, phaseDTOReturned);
        Assert.assertEquals(result.getId(), phaseDTOReturned.getId());
        Assert.assertEquals(result.getAbreviation(), phaseDTOReturned.getAbreviation());
        Assert.assertEquals(result.getIndexCouleur(), phaseDTOReturned.getIndexCouleur());
    }

    @Test
    public void deleteAllById() {

        List<Long> phaseIdList = new ArrayList<>();
        phaseIdList.add(1l);
        phaseIdList.add(2l);

        when(phaseCommandPersistencePort.deleteAllById(phaseIdList)).thenReturn(phaseDTOList);

        List<PhaseDTO> result = phaseCommandServiceImpl.deleteAllById(phaseIdList);

        Assert.assertEquals(phaseIdList.get(0), phaseDTOList.get(0).getId());
        Assert.assertEquals(phaseIdList.get(1), phaseDTOList.get(1).getId());

    }
}