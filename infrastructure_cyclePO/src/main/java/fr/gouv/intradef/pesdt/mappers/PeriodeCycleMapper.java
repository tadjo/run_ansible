package fr.gouv.intradef.pesdt.mappers;

import fr.gouv.intradef.pesdt.common.infrastructure.adapter.mapper.IPesdtMapper;
import fr.gouv.intradef.pesdt.dto.PeriodeCycleDTO;
import fr.gouv.intradef.pesdt.dto.PhaseDTO;
import fr.gouv.intradef.pesdt.entity.PeriodeCycleEntity;
import fr.gouv.intradef.pesdt.entity.PhaseEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(uses = {PhaseMapper.class})
public interface PeriodeCycleMapper extends IPesdtMapper<PeriodeCycleEntity, PeriodeCycleDTO>
{
    /**
     * Mappe un objet phaseDTO en un objet periodeCycleEntity
     *
     * @param periodeCycleDTO
     *            source à mapper
     * @return objet mappé
     */
    @Mapping(source = "phase", target = "phase")
    PeriodeCycleEntity toEntity(PeriodeCycleDTO periodeCycleDTO);

    /**
     * Mappe un objet periodeCycleEntity en un objet periodeCycleDTO
     *
     * @param periodeCycleEntity
     *            source à mapper
     * @return objet mappé
     */
    @Mapping(source = "phase", target = "phase")
    PeriodeCycleDTO toDto(PeriodeCycleEntity periodeCycleEntity);

    /**
     * Mappe une liste de periodeCycleEntities en une liste de periodeCycleDTOs
     *
     * @param periodeCycleEntities
     *            liste à mapper
     * @return liste mappée
     */
    List<PeriodeCycleDTO> toDtos(List<PeriodeCycleEntity> periodeCycleEntities);

    /**
     * Mappe une liste de periodeCycleDTOs en une liste de periodeCycleEntities
     *
     * @param PeriodeCycleDTOS
     *            liste à mapper
     * @return liste mappée
     */
    List<PeriodeCycleEntity> toEntities(List<PeriodeCycleDTO> PeriodeCycleDTOS);


}
