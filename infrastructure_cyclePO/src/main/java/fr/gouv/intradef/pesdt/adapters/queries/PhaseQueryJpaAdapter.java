package fr.gouv.intradef.pesdt.adapters.queries;

import fr.gouv.intradef.pesdt.common.domain.exceptions.BadPropertyPageableException;
import fr.gouv.intradef.pesdt.common.domain.exceptions.ResourceNotFoundException;
import fr.gouv.intradef.pesdt.common.domain.utils.AssertionUtils;
import fr.gouv.intradef.pesdt.common.infrastructure.configuration.database.DataSourceType;
import fr.gouv.intradef.pesdt.common.infrastructure.configuration.database.WithDatabase;
import fr.gouv.intradef.pesdt.dto.PhaseDTO;
import fr.gouv.intradef.pesdt.entity.PhaseEntity;
import fr.gouv.intradef.pesdt.mappers.PhaseMapper;
import fr.gouv.intradef.pesdt.ports.spi.queries.PhaseQueryPersistencePort;
import fr.gouv.intradef.pesdt.repository.PhaseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mapping.PropertyReferenceException;

import java.util.List;

public class PhaseQueryJpaAdapter implements PhaseQueryPersistencePort
{
	private PhaseRepository phaseRepository;

	@Autowired
	private PhaseMapper phaseMapper;

	@Autowired
	public PhaseQueryJpaAdapter(PhaseRepository phaseRepository)
	{
		this.phaseRepository = phaseRepository;
	}

	@Override
	public List<PhaseDTO> findAll() {
		List<PhaseEntity> phaseEntityList = phaseRepository.findAll();

		return phaseMapper.toDtos(phaseEntityList);
	}

	@Override
	public Page<PhaseDTO> findAllPage(Pageable page) {
		try {
			Page<PhaseEntity> phasePage = this.phaseRepository.findAll(page);
			return phasePage.map(g -> this.phaseMapper.toDto(g));
		} catch (PropertyReferenceException e) {
			throw new BadPropertyPageableException(e.getLocalizedMessage());
		}
	}


	@Override
	public PhaseDTO findById(Long id) {
		AssertionUtils.assertNotNull(id);
		PhaseEntity phaseEntity = this.phaseRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException(id, PhaseEntity.class));
		return phaseMapper.toDto(phaseEntity);
	}

	@Override
	public List<PhaseDTO> findByIndexCouleur(Integer indexCouleur) {
		AssertionUtils.assertNotNull(indexCouleur);
		List<PhaseEntity> phaseEntityList = phaseRepository.findByIndexCouleur(indexCouleur);
		return phaseMapper.toDtos(phaseEntityList);
	}

}
