package fr.gouv.intradef.pesdt.repository;


import fr.gouv.intradef.pesdt.entity.PhaseEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PhaseRepository extends JpaRepository<PhaseEntity, Long>, JpaSpecificationExecutor<PhaseEntity>
{
    List<PhaseEntity> findByIndexCouleur(Integer indexCouleur);

}
