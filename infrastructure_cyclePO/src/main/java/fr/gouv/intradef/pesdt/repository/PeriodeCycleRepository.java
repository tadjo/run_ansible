package fr.gouv.intradef.pesdt.repository;


import fr.gouv.intradef.pesdt.entity.PeriodeCycleEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface PeriodeCycleRepository extends JpaRepository<PeriodeCycleEntity, Long>, JpaSpecificationExecutor<PeriodeCycleEntity>
{

    List<PeriodeCycleEntity> findAllByOrganismeId(Integer id);
}
