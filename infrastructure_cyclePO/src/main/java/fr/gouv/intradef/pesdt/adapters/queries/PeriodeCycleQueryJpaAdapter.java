package fr.gouv.intradef.pesdt.adapters.queries;

import fr.gouv.intradef.pesdt.common.domain.exceptions.ResourceNotFoundException;
import fr.gouv.intradef.pesdt.common.domain.utils.AssertionUtils;
import fr.gouv.intradef.pesdt.dto.PeriodeCycleDTO;
import fr.gouv.intradef.pesdt.dto.enums.MessageErreurCyclePOEnum;
import fr.gouv.intradef.pesdt.entity.PeriodeCycleEntity;
import fr.gouv.intradef.pesdt.common.domain.exceptions.BadPropertyPageableException;
import fr.gouv.intradef.pesdt.common.domain.exceptions.NoContentException;
import fr.gouv.intradef.pesdt.mappers.PeriodeCycleMapper;
import fr.gouv.intradef.pesdt.ports.spi.queries.PeriodeCycleQueryPersistencePort;
import fr.gouv.intradef.pesdt.repository.PeriodeCycleRepository;
import fr.gouv.intradef.pesdt.specification.PeriodeCycleSpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.mapping.PropertyReferenceException;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public class PeriodeCycleQueryJpaAdapter implements PeriodeCycleQueryPersistencePort
{
	@Autowired
	private PeriodeCycleMapper periodeCycleMapper;
	private PeriodeCycleRepository periodeCycleRepository;

	@Autowired
	public PeriodeCycleQueryJpaAdapter(PeriodeCycleRepository periodeCycleRepository)
	{
		this.periodeCycleRepository = periodeCycleRepository;
	}

	@Override
	public List<PeriodeCycleDTO> findAll()
	{
		List<PeriodeCycleEntity> filtrePeriodeCycleList = periodeCycleRepository.findAll();

		return this.periodeCycleMapper.toDtos(filtrePeriodeCycleList);
	}

	@Override
	public PeriodeCycleDTO findById(Long id)
	{
		AssertionUtils.assertNotNull(id);
		PeriodeCycleEntity filtrePeriodeCycle = this.periodeCycleRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException(id, PeriodeCycleEntity.class));
		return this.periodeCycleMapper.toDto(filtrePeriodeCycle);
	}

	@Override
	public List<PeriodeCycleDTO> findByOrganismeId(Integer id)
	{
		AssertionUtils.assertNotNull(id);
		List<PeriodeCycleEntity> periodesCycle = this.periodeCycleRepository.findAllByOrganismeId(id);

		return this.periodeCycleMapper.toDtos(periodesCycle);
	}

	@Override
	public Page<PeriodeCycleDTO> findAllPageByFilter(Pageable translatePageable, List<Integer> organismeId,
													 List<Integer> phaseId,
													 List<LocalDateTime> dateDeDebut,
													 List<LocalDateTime> dateDeFin)
	{
		try
		{
			Specification<PeriodeCycleEntity> specification =
					Optional.ofNullable(PeriodeCycleSpecification.filterOrganismeIdIn(organismeId))
							.map(spec -> spec.and(PeriodeCycleSpecification.filterPhaseIdIn(phaseId)))
							.map(spec -> spec.and(PeriodeCycleSpecification.filterDateDeDebutIn(dateDeDebut)))
							.map(spec -> spec.and(PeriodeCycleSpecification.filterDateDeFinIn(dateDeFin)))
							.orElse(null);

			Page<PeriodeCycleEntity> filtrePeriodeCyclePage = this.periodeCycleRepository.findAll(specification, translatePageable);
			if (filtrePeriodeCyclePage.isEmpty())
			{
				throw new NoContentException(MessageErreurCyclePOEnum.MESSAGE_ERREUR_FILTRE_CYCLEPO_ENUM.getMessageErreur());
			}
			else
			{
				return filtrePeriodeCyclePage.map(periodeCycleMapper::toDto);
			}
		}
		catch (PropertyReferenceException e)
		{
			throw new BadPropertyPageableException(e.getLocalizedMessage());
		}
	}

	/**
	 *
	 * @param page
	 * @return PeriodeCycleDTO
	 */
	@Override
	public Page<PeriodeCycleDTO> findAllPage(Pageable page)
	{
		try {
			Page<PeriodeCycleEntity> periodeCyclePage = this.periodeCycleRepository.findAll(page);
			return periodeCyclePage.map(g -> this.periodeCycleMapper.toDto(g));
		} catch (PropertyReferenceException e) {
			throw new BadPropertyPageableException(e.getLocalizedMessage());
		}
	}

}
