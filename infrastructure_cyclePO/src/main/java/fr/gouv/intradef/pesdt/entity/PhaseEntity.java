package fr.gouv.intradef.pesdt.entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import fr.gouv.intradef.pesdt.common.infrastructure.adapter.jpa.entity.AbstractEntity;
import lombok.*;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;


@Getter
@Setter
@Entity
@AllArgsConstructor
@NoArgsConstructor
@JsonIdentityInfo(scope = PhaseEntity.class, generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
@Table(name = "phase")
public class PhaseEntity extends AbstractEntity<Long>
{

	/**
	 * Generated UUID
	 */
	private static final long serialVersionUID = -8658115099226317607L;


	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "description")
	private String description;

	@Column(name = "index_couleur")
	private Integer indexCouleur;

	@Column(name = "abreviation")
	private String abreviation;

}
