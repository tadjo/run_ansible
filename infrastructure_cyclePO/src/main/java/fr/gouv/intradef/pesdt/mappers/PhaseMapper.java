package fr.gouv.intradef.pesdt.mappers;

import fr.gouv.intradef.pesdt.common.infrastructure.adapter.mapper.IPesdtMapper;
import fr.gouv.intradef.pesdt.dto.PhaseDTO;
import fr.gouv.intradef.pesdt.entity.PhaseEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(uses = {PeriodeCycleMapper.class})
public interface PhaseMapper extends IPesdtMapper<PhaseEntity, PhaseDTO>
{
    /**
     * Mappe un objet phaseDTO en un objet phaseEntity
     *
     * @param phaseDTO
     *            source à mapper
     * @return objet mappé
     */
    PhaseEntity toEntity(PhaseDTO phaseDTO);

    /**
     * Mappe un objet phaseEntity en un objet phaseDTO
     *
     * @param phaseEntity
     *            source à mapper
     * @return objet mappé
     */
    PhaseDTO toDto(PhaseEntity phaseEntity);

    /**
     * Mappe une liste de phaseEntities en une liste de phaseDTOs
     *
     * @param phaseEntities
     *            liste à mapper
     * @return liste mappée
     */
    List<PhaseDTO> toDtos(List<PhaseEntity> phaseEntities);

    /**
     * Mappe une liste de phaseDTOs en une liste de phaseEntities
     *
     * @param phaseDTOS
     *            liste à mapper
     * @return liste mappée
     */
    List<PhaseEntity> toEntities(List<PhaseDTO> phaseDTOS);

}
