package fr.gouv.intradef.pesdt.specification;
import fr.gouv.intradef.pesdt.dto.PeriodeCycleDTO;
import fr.gouv.intradef.pesdt.entity.PeriodeCycleEntity;
import fr.gouv.intradef.pesdt.entity.PhaseEntity;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.CollectionUtils;

import javax.persistence.criteria.Path;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.List;

public class PeriodeCycleSpecification {


    /**
     * Filtre unitaire qui permet de récupérer les entités qui ont leur Nom égaux à
     * l'argument. Si l'argument est vide (Optional non présent, pas null), alors le
     * filtre ne s'applique simplement pas.
     *
     * @param dateDeDebut
     *            Argument pour ajouter le filtre.
     * @return La spécification pour chainer les filtres.
     */
    public static Specification<PeriodeCycleEntity> filterDateDeDebutIn(List<LocalDateTime> dateDeDebut) {
        return (root, query, builder) -> {
            if (!CollectionUtils.isEmpty(dateDeDebut)) {
                // filtre par liste de Nom
                query.distinct(true);
                Path<PeriodeCycleEntity> periodeCyclePath = root.get("dateDeDebut");
                return periodeCyclePath.in(dateDeDebut);
            } else {
                // ignore filtre
                return builder.conjunction();
            }
        };
    }

    /**
     * Filtre unitaire qui permet de récupérer les entités qui ont leur Nom égaux à
     * l'argument. Si l'argument est vide (Optional non présent, pas null), alors le
     * filtre ne s'applique simplement pas.
     *
     * @param dateDeFin
     *            Argument pour ajouter le filtre.
     * @return La spécification pour chainer les filtres.
     */
    public static Specification<PeriodeCycleEntity> filterDateDeFinIn(List<LocalDateTime> dateDeFin) {
        return (root, query, builder) -> {
            if (!CollectionUtils.isEmpty(dateDeFin)) {
                // filtre par liste de Nom
                query.distinct(true);
                Path<PeriodeCycleEntity> periodeCyclePath = root.get("dateDeFin");
                return periodeCyclePath.in(dateDeFin);
            } else {
                // ignore filtre
                return builder.conjunction();
            }
        };
    }

    /**
     * Filtre unitaire qui permet de récupérer les entités qui ont leur organismeId
     * égaux à l'argument. Si l'argument est vide (Optional non
     * présent, pas null), alors le filtre ne s'applique simplement pas.
     *
     * @param organismeId
     *            Argument pour ajouter le filtre.
     * @return La spécification pour chainer les filtres.
     */
    public static Specification<PeriodeCycleEntity> filterOrganismeIdIn(List<Integer> organismeId) {
        return (root, query, builder) -> {
            if (!CollectionUtils.isEmpty(organismeId)) {
                // filtre par liste de organismeId
                query.distinct(true);
                Path<PeriodeCycleEntity> periodeCyclePath = root.get("organismeId");
                return periodeCyclePath.in(organismeId);
            } else {
                // ignore filtre
                return builder.conjunction();
            }
        };
    }

    /**
     * Filtre unitaire qui permet de récupérer les entités qui ont leur PhaseId
     * égaux à l'argument. Si l'argument est vide (Optional non
     * présent, pas null), alors le filtre ne s'applique simplement pas.
     *
     * @param phaseId
     *            Argument pour ajouter le filtre.
     * @return La spécification pour chainer les filtres.
     */
    public static Specification<PeriodeCycleEntity> filterPhaseIdIn(List<Integer> phaseId) {
        return (root, query, builder) -> {
            if (!CollectionUtils.isEmpty(phaseId)) {
                // filtre par liste de phase id
                query.distinct(true);
                Path<PeriodeCycleEntity> periodeCyclePath = root.get("phaseId");
                return periodeCyclePath.in(phaseId);
            } else {
                // ignore filtre
                return builder.conjunction();
            }
        };
    }

}
