package fr.gouv.intradef.pesdt.adapters.commands;

import fr.gouv.intradef.pesdt.common.domain.exceptions.AlreadyExistsException;
import fr.gouv.intradef.pesdt.common.domain.exceptions.IDNotFoundException;
import fr.gouv.intradef.pesdt.common.domain.exceptions.LowerVersionException;
import fr.gouv.intradef.pesdt.common.domain.port.common.CrudServiceUtils;
import fr.gouv.intradef.pesdt.common.domain.utils.AssertionUtils;
import fr.gouv.intradef.pesdt.dto.PeriodeCycleDTO;
import fr.gouv.intradef.pesdt.entity.PeriodeCycleEntity;
import fr.gouv.intradef.pesdt.mappers.PeriodeCycleMapper;
import fr.gouv.intradef.pesdt.ports.spi.commands.PeriodeCycleCommandPersistencePort;
import fr.gouv.intradef.pesdt.repository.PeriodeCycleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class PeriodeCycleCommandJpaAdapter implements PeriodeCycleCommandPersistencePort {

    @Autowired
    private PeriodeCycleRepository periodeCycleRepository;

    @Autowired
    private PeriodeCycleMapper periodeCycleMapper;

    public PeriodeCycleCommandJpaAdapter(PeriodeCycleRepository periodeCycleRepository)
    {
        this.periodeCycleRepository = periodeCycleRepository;
    }

    @Override
    public List<PeriodeCycleDTO> saveAll(List<PeriodeCycleDTO> entities) {
        return CrudServiceUtils.saveAll(entities, periodeCycleRepository, periodeCycleMapper);
    }

    public PeriodeCycleDTO save(PeriodeCycleDTO entity)
    {
        AssertionUtils.assertNotNull(entity);

        if (entity.getId() != null)
        {
            this.periodeCycleRepository.findById(entity.getId()).ifPresent(f -> {
                throw new AlreadyExistsException(entity.getId(), PeriodeCycleEntity.class);
            });
        }
        entity.setVersion(1L);
        PeriodeCycleEntity toSave = this.periodeCycleMapper.toEntity(entity);
        return this.periodeCycleMapper.toDto(this.periodeCycleRepository.save(toSave));
    }

    @Override
    public List<PeriodeCycleDTO> deleteAllById(List<Long> ids) {
        return CrudServiceUtils.deleteByIds(ids, periodeCycleRepository, periodeCycleMapper);
    }

    @Override
    public PeriodeCycleDTO deleteById(Long id) {
        AssertionUtils.assertNotNull(id);

        PeriodeCycleEntity foundPeriodeCycle = this.periodeCycleRepository.findById(id)
                .orElseThrow(() -> new IDNotFoundException(id, PeriodeCycleEntity.class));
        this.periodeCycleRepository.delete(foundPeriodeCycle);

        return this.periodeCycleMapper.toDto(foundPeriodeCycle);
    }

    @Override
    public List<PeriodeCycleDTO> updateAll(List<PeriodeCycleDTO> entities) {

        Set<Long> idsToSave = entities.stream().map(PeriodeCycleDTO::getId).collect(Collectors.toSet());
        List<PeriodeCycleEntity> entitiesResult = this.periodeCycleRepository.findAllById(idsToSave);
        CrudServiceUtils.checkOnlyCommonElements(entities, entitiesResult);
        return CrudServiceUtils.updateAll(entities, periodeCycleRepository, periodeCycleMapper);
    }

    @Override
    public PeriodeCycleDTO update(PeriodeCycleDTO entities) {
        AssertionUtils.assertNotNull(entities);

        PeriodeCycleEntity found = this.periodeCycleRepository.findById(entities.getId())
                .orElseThrow(() -> new IDNotFoundException(entities.getId(), PeriodeCycleEntity.class));

        if (found.getVersion().equals(entities.getVersion()))
        {
            entities.setVersion(entities.getVersion() + 1);
            PeriodeCycleEntity toSave = this.periodeCycleMapper.toEntity(entities);
            return this.periodeCycleMapper.toDto(this.periodeCycleRepository.save(toSave));
        }
        else
        {
            throw new LowerVersionException(entities.getVersion(), found.getVersion(), PeriodeCycleEntity.class);
        }
    }
}
