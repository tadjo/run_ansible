package fr.gouv.intradef.pesdt.adapters.commands;

import fr.gouv.intradef.pesdt.common.domain.exceptions.AlreadyExistsException;
import fr.gouv.intradef.pesdt.common.domain.exceptions.IDNotFoundException;
import fr.gouv.intradef.pesdt.common.domain.exceptions.LowerVersionException;
import fr.gouv.intradef.pesdt.common.domain.port.common.CrudServiceUtils;
import fr.gouv.intradef.pesdt.common.domain.utils.AssertionUtils;
import fr.gouv.intradef.pesdt.dto.PhaseDTO;
import fr.gouv.intradef.pesdt.entity.PhaseEntity;
import fr.gouv.intradef.pesdt.mappers.PhaseMapper;
import fr.gouv.intradef.pesdt.ports.spi.commands.PhaseCommandPersistencePort;
import fr.gouv.intradef.pesdt.repository.PhaseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.List;

@Service
public class PhaseCommandJpaAdapter implements PhaseCommandPersistencePort {

    @Autowired
    private PhaseRepository phaseRepository;

    @Autowired
    private PhaseMapper phaseMapper;


    public PhaseCommandJpaAdapter(PhaseRepository phaseRepository)
    {

        this.phaseRepository = phaseRepository;
    }

    @Override
    public PhaseDTO save(PhaseDTO entity)
    {
        AssertionUtils.assertNotNull(entity);

        if (entity.getId() != null)
        {
            this.phaseRepository.findById(entity.getId()).ifPresent(f -> {
                throw new AlreadyExistsException(entity.getId(), PhaseEntity.class);
            });
        }
        entity.setVersion(1L);
        PhaseEntity toSave = this.phaseMapper.toEntity(entity);
        return this.phaseMapper.toDto(this.phaseRepository.save(toSave));
    }

    @Override
    public List<PhaseDTO> saveAll(List<PhaseDTO> entities)
    {
        return CrudServiceUtils.saveAll(entities, phaseRepository, phaseMapper);
    }

    @Override
    public List<PhaseDTO> deleteAllById(List<Long> ids) {
        return CrudServiceUtils.deleteByIds(ids, phaseRepository, phaseMapper);
    }

    @Override
    public PhaseDTO update(PhaseDTO phaseDTO)
    {
        AssertionUtils.assertNotNull(phaseDTO);

        PhaseEntity found = this.phaseRepository.findById(phaseDTO.getId())
                .orElseThrow(() -> new IDNotFoundException(phaseDTO.getId(), PhaseEntity.class));

        if (found.getVersion().equals(phaseDTO.getVersion()))
        {
            phaseDTO.setVersion(phaseDTO.getVersion() + 1);
            PhaseEntity toSave = this.phaseMapper.toEntity(phaseDTO);
            return this.phaseMapper.toDto(this.phaseRepository.save(toSave));
        }
        else
        {
            throw new LowerVersionException(phaseDTO.getVersion(), found.getVersion(), PhaseEntity.class);
        }
    }

    @Override
    public PhaseDTO deleteById(Long id)
    {
        AssertionUtils.assertNotNull(id);

        PhaseEntity foundPhaseEntity = this.phaseRepository.findById(id)
                .orElseThrow(() -> new IDNotFoundException(id, PhaseEntity.class));
        this.phaseRepository.delete(foundPhaseEntity);

        return this.phaseMapper.toDto(foundPhaseEntity);
    }

    @Override
    public List<PhaseDTO> updateAll(List<PhaseDTO> entities)
    {
        return CrudServiceUtils.updateAll(entities, phaseRepository, phaseMapper);
    }


}

