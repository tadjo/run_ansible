package fr.gouv.intradef.pesdt.entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import fr.gouv.intradef.pesdt.common.infrastructure.adapter.jpa.entity.AbstractEntity;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GenerationType;
import javax.persistence.GeneratedValue;
import javax.persistence.Column;
import javax.persistence.OneToOne;
import javax.persistence.JoinColumn;
import java.time.LocalDateTime;

@Getter
@Setter
@Entity
@AllArgsConstructor
@NoArgsConstructor
@JsonIdentityInfo(scope = PeriodeCycleEntity.class, generator = ObjectIdGenerators.PropertyGenerator.class,
		property = "id")
@Table(name = "periode_cycle")
public class PeriodeCycleEntity extends AbstractEntity<Long>
{

	private static final long serialVersionUID = -4596992864584514448L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "date_de_debut")
	private LocalDateTime dateDeDebut;

	@Column(name = "date_de_fin")
	private LocalDateTime dateDeFin;

	@Column(name = "description")
	private String description;

	@Column(name = "organisme_id")
	private Integer organismeId;

	@Column(name = "organisme_abreviation")
	private String organisme_abreviation;

	@Column(name = "phase_id")
	private Integer phaseId;

	/**
	 * phase de la periode de cycle
	 */
	@OneToOne
	@JoinColumn(name = "phase_id", referencedColumnName = "id", insertable = false, updatable = false)
	private PhaseEntity phase;

}
