package fr.gouv.intradef.pesdt.repository.commands;

import fr.gouv.intradef.pesdt.CyclePOApplication;
import fr.gouv.intradef.pesdt.dto.PeriodeCycleDTO;
import fr.gouv.intradef.pesdt.dto.PhaseDTO;
import fr.gouv.intradef.pesdt.entity.PeriodeCycleEntity;
import fr.gouv.intradef.pesdt.entity.PhaseEntity;
import fr.gouv.intradef.pesdt.mappers.PeriodeCycleMapper;
import fr.gouv.intradef.pesdt.ports.spi.commands.PeriodeCycleCommandPersistencePort;
import fr.gouv.intradef.pesdt.ports.spi.commands.PhaseCommandPersistencePort;
import fr.gouv.intradef.pesdt.repository.PeriodeCycleRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.time.Month;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;


@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@SpringBootTest(classes = CyclePOApplication.class)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class PeriodeCycleCommandPersistencePortTest {

    @Autowired
    private PeriodeCycleCommandPersistencePort periodeCycleCommandPersistencePort;

    @Autowired
    private PhaseCommandPersistencePort phaseCommandPersistencePort;

    @Autowired
    private PeriodeCycleRepository periodeCycleRepository;

    @Autowired
    private PeriodeCycleMapper periodeCycleMapper;

    private PeriodeCycleDTO periodeCycleDTO;

    private PeriodeCycleDTO periodeCycleDTO2;

    private PhaseDTO phaseDTO;

    private PhaseDTO phaseDTO2;


    @Before
    public void setUp(){
        periodeCycleDTO = new PeriodeCycleDTO();
        periodeCycleDTO.setDateDeDebut(LocalDateTime.of(2020, Month.FEBRUARY, 19, 0, 0));
        periodeCycleDTO.setDateDeFin(LocalDateTime.of(2020, Month.FEBRUARY, 25, 0, 0));
        periodeCycleDTO.setDescription("Test 01");
        periodeCycleDTO.setOrganismeId(1);
        periodeCycleDTO.setOrganisme_abreviation("minarm");
        periodeCycleDTO.setPhaseId(1);

        periodeCycleDTO2 = new PeriodeCycleDTO();
        periodeCycleDTO2.setDateDeDebut(LocalDateTime.of(2021, Month.FEBRUARY, 19, 0, 0));
        periodeCycleDTO2.setDateDeFin(LocalDateTime.of(2021, Month.FEBRUARY, 25, 0, 0));
        periodeCycleDTO2.setDescription("Test 02");
        periodeCycleDTO2.setOrganismeId(2);
        periodeCycleDTO2.setOrganisme_abreviation("minarm");
        periodeCycleDTO2.setPhaseId(1);

        phaseDTO = new PhaseDTO();
        phaseDTO.setId(7l);
        phaseDTO.setAbreviation("OPEX");
        phaseDTO.setDescription("OPEX (Opérations Extérieusr) sont les \"interventions des forces militaires françaises en dehors du territoire national\"");
        phaseDTO.setIndexCouleur(1);

        phaseDTO2 = new PhaseDTO();
        phaseDTO.setId(8l);
        phaseDTO2.setAbreviation("RCO");
        phaseDTO2.setDescription("Remise en Condition Operationnel");
        phaseDTO2.setIndexCouleur(2);
    }


    @Test
    public void save() {

        long countBefore = periodeCycleRepository.count();


        PeriodeCycleDTO periodeCycleDTOReturned = periodeCycleCommandPersistencePort.save(periodeCycleDTO);

        long countAfter = periodeCycleRepository.count();

        assertThat(countBefore).isLessThan(countAfter);

        periodeCycleCommandPersistencePort.deleteById(periodeCycleDTOReturned.getId());

        Optional<PeriodeCycleEntity> deletedPeriodeCycle = periodeCycleRepository.findById(periodeCycleDTOReturned.getId());

        assertThat(deletedPeriodeCycle.isPresent());
    }

    @Test
    public void saveAll() {

        long countBefore = periodeCycleRepository.count();

        List<PeriodeCycleDTO> savedPeriodeCycle = periodeCycleCommandPersistencePort.saveAll(Arrays.asList(periodeCycleDTO,periodeCycleDTO2));

        long countAfter = periodeCycleRepository.count();

        assertThat(countBefore).isLessThan(countAfter);
        assertThat(!savedPeriodeCycle.isEmpty());
        Assertions.assertEquals(savedPeriodeCycle.get(0).getPhaseId(), periodeCycleDTO.getPhaseId());
    }
    @Test
    public void update() {
        phaseCommandPersistencePort.save(phaseDTO);
        periodeCycleCommandPersistencePort.save(periodeCycleDTO);
        PeriodeCycleEntity periodeCycleEntity = periodeCycleRepository.findById(1l).orElse(null);

        PeriodeCycleDTO periodeCycleDTOReturned = periodeCycleMapper.toDto(periodeCycleEntity);

        // update the PeriodeCycleDTO
        periodeCycleDTOReturned.setDescription("Updated Test 01");
        periodeCycleCommandPersistencePort.update(periodeCycleDTOReturned);

        // retrieve the PeriodeCycleDTO from the repository
        PeriodeCycleEntity updatedPeriodeCycle = periodeCycleRepository.findById(periodeCycleDTOReturned.getId()).orElse(null);

        // check that the PeriodeCycleDTO was updated correctly
        assertThat(updatedPeriodeCycle).isNotNull();
        assertThat(updatedPeriodeCycle.getDescription()).isEqualTo("Updated Test 01");
    }

    @Test
    public void updateAll() {
        phaseCommandPersistencePort.save(phaseDTO);
        periodeCycleCommandPersistencePort.saveAll(Arrays.asList(periodeCycleDTO,periodeCycleDTO2));
        List<PeriodeCycleEntity> periodeCycleEntity = periodeCycleRepository.findAll();

        List<PeriodeCycleDTO> periodeCycleDTOReturned = periodeCycleMapper.toDtos(periodeCycleEntity);

        // update the PeriodeCycleDTO
        periodeCycleDTOReturned.get(0).setDescription("Updated Test 01");
        periodeCycleDTOReturned.get(1).setDescription("Updated Test 02");
        periodeCycleCommandPersistencePort.updateAll(periodeCycleDTOReturned);

        // retrieve the PeriodeCycleDTO from the repository
        List<PeriodeCycleEntity> updatedPeriodeCycle = periodeCycleRepository.findAll();

        // check that the PeriodeCycleDTO was updated correctly
        assertThat(updatedPeriodeCycle).isNotNull();
        assertThat(updatedPeriodeCycle.get(0).getDescription()).isEqualTo("Updated Test 01");
        assertThat(updatedPeriodeCycle.get(1).getDescription()).isEqualTo("Updated Test 02");
        Assertions.assertEquals(updatedPeriodeCycle.get(0).getPhaseId(), periodeCycleDTO.getPhaseId());
        Assertions.assertEquals(updatedPeriodeCycle.get(1).getPhaseId(), periodeCycleDTO.getPhaseId());
    }

    @Test
    public void deleteAll() {

        periodeCycleCommandPersistencePort.saveAll(Arrays.asList(periodeCycleDTO,periodeCycleDTO2));
        List<PeriodeCycleEntity> periodeCycleEntity = periodeCycleRepository.findAll();

        List<PeriodeCycleDTO> periodeCycleDTOReturned = periodeCycleMapper.toDtos(periodeCycleEntity);

        periodeCycleCommandPersistencePort.deleteAllById(Arrays.asList(periodeCycleDTOReturned.get(0).getId(),periodeCycleDTOReturned.get(1).getId()));

        // retrieve the PeriodeCycleDTO from the repository
        List<PeriodeCycleEntity> updatedPeriodeCycle = periodeCycleRepository.findAll();

        // check that the PeriodeCycleDTO was deleted
        assertThat(updatedPeriodeCycle.isEmpty());
    }

}