package fr.gouv.intradef.pesdt.repository.queries;

import fr.gouv.intradef.pesdt.CyclePOApplication;
import fr.gouv.intradef.pesdt.configuration.database.DatabaseContextHolder;
import fr.gouv.intradef.pesdt.dto.PeriodeCycleDTO;
import fr.gouv.intradef.pesdt.dto.PhaseDTO;
import fr.gouv.intradef.pesdt.entity.PeriodeCycleEntity;
import fr.gouv.intradef.pesdt.mappers.PeriodeCycleMapper;
import fr.gouv.intradef.pesdt.mappers.PhaseMapper;
import fr.gouv.intradef.pesdt.ports.api.queries.PeriodeCycleQueryServicePort;
import fr.gouv.intradef.pesdt.ports.api.queries.PhaseQueryServicePort;
import fr.gouv.intradef.pesdt.ports.spi.commands.PeriodeCycleCommandPersistencePort;
import fr.gouv.intradef.pesdt.ports.spi.commands.PhaseCommandPersistencePort;
import fr.gouv.intradef.pesdt.ports.spi.queries.PeriodeCycleQueryPersistencePort;
import fr.gouv.intradef.pesdt.repository.PeriodeCycleRepository;
import fr.gouv.intradef.pesdt.repository.PhaseRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.time.Month;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@SpringBootTest(classes = CyclePOApplication.class)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class PeriodeCycleQueryPersistencePortTest {

    @Autowired
    private PeriodeCycleQueryPersistencePort periodeCycleQueryPersistencePort;

    @Autowired
    private PeriodeCycleQueryServicePort periodeCycleQueryServicePort;

    @Autowired
    private PeriodeCycleCommandPersistencePort periodeCycleCommandPersistencePort;

    @Autowired
    private PhaseCommandPersistencePort phaseCommandPersistencePort;

    @Autowired
    private PeriodeCycleRepository periodeCycleRepository;

    @Autowired
    private PeriodeCycleMapper periodeCycleMapper;

    private PeriodeCycleDTO periodeCycleDTO;

    private PeriodeCycleDTO periodeCycleDTO2;

    private PhaseDTO phaseDTO;

    private PhaseDTO phaseDTO2;

    private PeriodeCycleDTO periodeCycleDTOReturned;

    private List<PeriodeCycleDTO> savedPeriodeCycle;

    @Before
    public void setUp(){
        periodeCycleDTO = new PeriodeCycleDTO();
        periodeCycleDTO.setDateDeDebut(LocalDateTime.of(2020, Month.FEBRUARY, 19, 0, 0));
        periodeCycleDTO.setDateDeFin(LocalDateTime.of(2020, Month.FEBRUARY, 25, 0, 0));
        periodeCycleDTO.setDescription("Test 01");
        periodeCycleDTO.setOrganismeId(1);
        periodeCycleDTO.setOrganisme_abreviation("minarm");
        periodeCycleDTO.setPhaseId(1);

        periodeCycleDTO2 = new PeriodeCycleDTO();
        periodeCycleDTO2.setDateDeDebut(LocalDateTime.of(2021, Month.FEBRUARY, 19, 0, 0));
        periodeCycleDTO2.setDateDeFin(LocalDateTime.of(2021, Month.FEBRUARY, 25, 0, 0));
        periodeCycleDTO2.setDescription("Test 02");
        periodeCycleDTO2.setOrganismeId(2);
        periodeCycleDTO2.setOrganisme_abreviation("minarm");
        periodeCycleDTO2.setPhaseId(1);

        phaseDTO = new PhaseDTO();
        phaseDTO.setAbreviation("OPEX");
        phaseDTO.setDescription("OPEX (Opérations Extérieusr) sont les \"interventions des forces militaires françaises en dehors du territoire national\"");
        phaseDTO.setIndexCouleur(1);

        phaseDTO2 = new PhaseDTO();
        phaseDTO2.setAbreviation("RCO");
        phaseDTO2.setDescription("Remise en Condition Operationnel");
        phaseDTO2.setIndexCouleur(2);

        phaseCommandPersistencePort.save(phaseDTO);
        periodeCycleDTOReturned = periodeCycleCommandPersistencePort.save(periodeCycleDTO);
        savedPeriodeCycle = periodeCycleCommandPersistencePort.saveAll(Arrays.asList(periodeCycleDTO,periodeCycleDTO2));
    }
    @Test
    public void findByOrganismeId() {

        List<PeriodeCycleDTO> periodeCycleFounded = periodeCycleQueryPersistencePort.findByOrganismeId(periodeCycleDTOReturned.getOrganismeId());

        Assertions.assertNotNull(periodeCycleFounded);
        Assertions.assertNotNull(periodeCycleFounded.get(0).getId());
    }

    @Test
    public void findAllPageByFilter() {

        Pageable pageable = PageRequest.of(0, 1);

        Page<PeriodeCycleDTO> periodeCycleFounded =
                periodeCycleQueryPersistencePort.findAllPageByFilter(pageable, Arrays.asList(savedPeriodeCycle.get(0).getOrganismeId()),null,null,null);

        Assertions.assertNotNull(periodeCycleFounded);
    }

    @Test
    public void findById(){

        PeriodeCycleDTO periodeCycleFounded = periodeCycleQueryPersistencePort.findById(periodeCycleDTOReturned.getId());

        Assertions.assertNotNull(periodeCycleFounded);
        Assertions.assertNotNull(periodeCycleFounded.getId());
    }
}