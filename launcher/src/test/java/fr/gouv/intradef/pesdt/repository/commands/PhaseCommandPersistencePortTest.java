package fr.gouv.intradef.pesdt.repository.commands;

import fr.gouv.intradef.pesdt.CyclePOApplication;
import fr.gouv.intradef.pesdt.dto.PeriodeCycleDTO;
import fr.gouv.intradef.pesdt.dto.PhaseDTO;
import fr.gouv.intradef.pesdt.entity.PeriodeCycleEntity;
import fr.gouv.intradef.pesdt.entity.PhaseEntity;
import fr.gouv.intradef.pesdt.mappers.PeriodeCycleMapper;
import fr.gouv.intradef.pesdt.mappers.PhaseMapper;
import fr.gouv.intradef.pesdt.ports.spi.commands.PeriodeCycleCommandPersistencePort;
import fr.gouv.intradef.pesdt.ports.spi.commands.PhaseCommandPersistencePort;
import fr.gouv.intradef.pesdt.repository.PeriodeCycleRepository;
import fr.gouv.intradef.pesdt.repository.PhaseRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.Assert.*;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@SpringBootTest(classes = CyclePOApplication.class)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class PhaseCommandPersistencePortTest {

    @Autowired
    private PhaseCommandPersistencePort phaseCommandPersistencePort;

    @Autowired
    private PhaseRepository phaseRepository;

    @Autowired
    private PhaseMapper phaseMapper;

    private PhaseDTO phaseDTO;

    private PhaseDTO phaseDTO2;

    private PhaseDTO phaseDTO3;

    @Before
    public void setUp() {

        phaseDTO = new PhaseDTO();
        phaseDTO.setAbreviation("OPEX");
        phaseDTO.setDescription("OPEX (Opérations Extérieusr) sont les \"interventions des forces militaires françaises en dehors du territoire national\"");
        phaseDTO.setIndexCouleur(1);

        phaseDTO2 = new PhaseDTO();
        phaseDTO2.setAbreviation("RCO");
        phaseDTO2.setDescription("Remise en Condition Operationnel");
        phaseDTO2.setIndexCouleur(2);

        phaseDTO3 = new PhaseDTO();
        phaseDTO3.setAbreviation("MCF");
        phaseDTO3.setDescription("MCF(Mise en Condition Finale)");
        phaseDTO3.setIndexCouleur(3);

    }

    @Test
    public void saveAndUpdateAndDelete() {

        long countBefore = phaseRepository.count();

        PhaseDTO phaseDTOReturned = phaseCommandPersistencePort.save(phaseDTO);

        long countAfter = phaseRepository.count();

        assertThat(countBefore).isLessThan(countAfter);

        phaseDTOReturned.setAbreviation("OPEX URGENCE");
        phaseCommandPersistencePort.update(phaseDTOReturned);

        PhaseEntity updatedPhase = phaseRepository.findById(phaseDTOReturned.getId()).orElse(null);

        assertThat(updatedPhase).isNotNull();
        assertThat(updatedPhase.getAbreviation()).isEqualTo("OPEX URGENCE");

        phaseCommandPersistencePort.deleteById(phaseDTOReturned.getId());

        PhaseEntity deletedPhase = phaseRepository.findById(phaseDTOReturned.getId()).orElse(null);

        Assertions.assertEquals(deletedPhase,null);
    }

    @Test
    public void saveAll() {
        long countBefore = phaseRepository.count();


        List<PhaseDTO> savedPhase = phaseCommandPersistencePort.saveAll(Arrays.asList(phaseDTO,phaseDTO2));

        long countAfter = phaseRepository.count();

        assertThat(countBefore).isLessThan(countAfter);
        assertThat(!savedPhase.isEmpty());
        Assertions.assertEquals(savedPhase.get(0).getAbreviation(), phaseDTO.getAbreviation());
        Assertions.assertEquals(savedPhase.get(1).getAbreviation(), phaseDTO2.getAbreviation());
    }

    @Test
    public void updateAll() {

        phaseCommandPersistencePort.saveAll(Arrays.asList(phaseDTO,phaseDTO2));
        List<PhaseEntity> phaseEntityList = phaseRepository.findAll();

        List<PhaseDTO> phaseDTOReturned = phaseMapper.toDtos(phaseEntityList);

        phaseDTOReturned.get(0).setAbreviation("OPEX URGENCE");
        phaseDTOReturned.get(1).setAbreviation("RCO-PR");
        phaseCommandPersistencePort.updateAll(phaseDTOReturned);

        List<PhaseEntity> updatedPhase = phaseRepository.findAll();

        assertThat(updatedPhase).isNotNull();
        assertThat(updatedPhase.get(0).getAbreviation()).isEqualTo("OPEX URGENCE");
        assertThat(updatedPhase.get(1).getAbreviation()).isEqualTo("RCO-PR");
        Assertions.assertEquals(updatedPhase.get(0).getIndexCouleur(), phaseDTOReturned.get(0).getIndexCouleur());
        Assertions.assertEquals(updatedPhase.get(1).getIndexCouleur(), phaseDTOReturned.get(1).getIndexCouleur());
    }

}