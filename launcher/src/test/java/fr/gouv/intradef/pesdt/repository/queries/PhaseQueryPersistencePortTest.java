package fr.gouv.intradef.pesdt.repository.queries;

import fr.gouv.intradef.pesdt.CyclePOApplication;
import fr.gouv.intradef.pesdt.dto.PeriodeCycleDTO;
import fr.gouv.intradef.pesdt.dto.PhaseDTO;
import fr.gouv.intradef.pesdt.mappers.PeriodeCycleMapper;
import fr.gouv.intradef.pesdt.mappers.PhaseMapper;
import fr.gouv.intradef.pesdt.ports.api.queries.PeriodeCycleQueryServicePort;
import fr.gouv.intradef.pesdt.ports.api.queries.PhaseQueryServicePort;
import fr.gouv.intradef.pesdt.ports.spi.commands.PeriodeCycleCommandPersistencePort;
import fr.gouv.intradef.pesdt.ports.spi.commands.PhaseCommandPersistencePort;
import fr.gouv.intradef.pesdt.ports.spi.queries.PeriodeCycleQueryPersistencePort;
import fr.gouv.intradef.pesdt.ports.spi.queries.PhaseQueryPersistencePort;
import fr.gouv.intradef.pesdt.repository.PeriodeCycleRepository;
import fr.gouv.intradef.pesdt.repository.PhaseRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.time.Month;
import java.util.Arrays;
import java.util.List;

import static fr.gouv.intradef.pesdt.common.domain.utils.AssertionUtils.assertNotNull;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@SpringBootTest(classes = CyclePOApplication.class)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class PhaseQueryPersistencePortTest {

    @Autowired
    private PhaseQueryPersistencePort phaseQueryPersistencePort;

    @Autowired
    private PhaseQueryServicePort phaseQueryServicePort;

    @Autowired
    private PhaseCommandPersistencePort phaseCommandPersistencePort;

    @Autowired
    private PhaseRepository phaseRepository;

    @Autowired
    private PhaseMapper phaseMapper;

    private PhaseDTO phaseDTO;

    private PhaseDTO phaseDTO2;

    private PhaseDTO phaseDTOReturned;




    @Before
    public void setUp(){

        phaseDTO = new PhaseDTO();
        phaseDTO.setAbreviation("OPEX");
        phaseDTO.setDescription("OPEX (Opérations Extérieusr) sont les \"interventions des forces militaires françaises en dehors du territoire national\"");
        phaseDTO.setIndexCouleur(1);

        phaseDTO2 = new PhaseDTO();
        phaseDTO2.setAbreviation("RCO");
        phaseDTO2.setDescription("Remise en Condition Operationnel");
        phaseDTO2.setIndexCouleur(2);

        phaseDTOReturned = phaseCommandPersistencePort.save(phaseDTO);
    }

    @Test
    public void findById() {

        PhaseDTO phaseFounded = phaseQueryServicePort.findById(phaseDTOReturned.getId());

        Assertions.assertNotNull(phaseFounded);
        Assertions.assertNotNull(phaseFounded.getId());
        Assertions.assertNotNull(phaseFounded.getAbreviation());
        Assertions.assertNotNull(phaseFounded.getIndexCouleur());
    }

    @Test
    public void findAll() {

        List<PhaseDTO> periodeCycleFounded = phaseQueryServicePort.findAll();

        Assertions.assertNotNull(periodeCycleFounded);
    }

    @Test
    public void findByIndexCouleur(){
        List<PhaseDTO> phaseDTOFounded = phaseQueryServicePort.findByIndexCouleur(phaseDTOReturned.getIndexCouleur());

        Assertions.assertNotNull(phaseDTOFounded);
        Assertions.assertNotNull(phaseDTOFounded.get(0).getId());
        Assertions.assertNotNull(phaseDTOFounded.get(0).getIndexCouleur());
    }

}