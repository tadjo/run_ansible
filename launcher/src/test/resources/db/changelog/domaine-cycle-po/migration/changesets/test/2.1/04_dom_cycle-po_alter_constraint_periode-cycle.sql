--liquibase formatted sql

--changeset migration:004 dbms:h2
--alter table periode_cycle add CONSTRAINT periode_cycle_pk PRIMARY KEY (id);
-- mise en commentaire afin de corriger le probleme lié a la duplication de clé primaire

--changeset migration:005 dbms:h2
--alter TABLE periode_cycle add CONSTRAINT periode_cycle_to_phase_fk FOREIGN KEY (phase_id) REFERENCES phase(id);
-- mise en commentaire afin de corriger le probleme lié a la duplication de clé primaire
