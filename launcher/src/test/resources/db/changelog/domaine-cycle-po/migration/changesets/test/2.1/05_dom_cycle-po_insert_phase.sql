--liquibase formatted sql

--changeset migration:006 dbms:h2
INSERT INTO phase (abreviation, description, index_couleur)
VALUES
    ('OPEX', 'Opérations Extérieures', 1),
    ('RCO', 'Remise en Condition Operationnelle', 2),
    ('POM/TN1', 'Préparation Opérationnelle Métier / Territoire National 1', 3),
    ('POIA/ENU', 'Préparation Opérationnelle Inter-Armée / Echelon National d Urgence', 4),
    ('POM/TN2', 'Préparation Opérationnelle Métier / Territoire National 2', 5),
    ('MCF', 'Mise en Condition Finale', 6);