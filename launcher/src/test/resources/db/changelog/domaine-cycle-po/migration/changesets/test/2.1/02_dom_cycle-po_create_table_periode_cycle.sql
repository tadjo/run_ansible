--liquibase formatted sql

--changeset migration:002 dbms:h2
CREATE TABLE IF NOT EXISTS periode_cycle (
    id SERIAL,
    phase_id int4 not null,
    date_de_debut timestamp,
    date_de_fin timestamp,
    description varchar(255),
    index_couleur int2 NOT NULL,
    organisme_id int4 not null ,
    organisme_abreviation varchar(120) not null,
    createur character varying(120),
    creation date,
    modificateur character varying(120),
    modification date,
    version int8 NOT NULL DEFAULT 1,
    CONSTRAINT periode_cycle_to_phase_fk FOREIGN KEY (phase_id) REFERENCES phase(id),
    CONSTRAINT periode_cycle_pk PRIMARY KEY (id)-- correction fichier 03_dom_cycle-po-alter_constraint_periode-cycle.sql pour les tests repository
    );