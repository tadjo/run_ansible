--changeset migration:001 dbms:h2
CREATE TABLE IF NOT EXISTS periode_cycle (
    id SERIAL,
    phase_id int4 not null,
    date_de_debut timestamptz,
    date_de_fin timestamptz,
    description varchar(255),
    index_couleur int2 NOT NULL,
    organisme_id int4 not null ,
    organisme_abreviation varchar(120) not null,
    createur character varying(120),
    creation date,
    modificateur character varying(120),
    modification date,
    "version" int8 NOT NULL DEFAULT 1
    );