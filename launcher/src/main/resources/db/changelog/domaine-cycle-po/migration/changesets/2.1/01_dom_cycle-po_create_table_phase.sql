--changeset migration:001 dbms:h2
CREATE TABLE IF NOT EXISTS phase (
    id SERIAL,
    abreviation varchar(120) NOT NULL,
    description varchar(255) NOT NULL,
    index_couleur int2 NOT NULL,
    createur character varying(120),
    creation date,
    modificateur character varying(120),
    modification date,
    "version" int8 NOT NULL DEFAULT 1
    );