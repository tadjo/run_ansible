package fr.gouv.intradef.pesdt.configuration;



import fr.gouv.intradef.pesdt.adapters.commands.PeriodeCycleCommandJpaAdapter;
import fr.gouv.intradef.pesdt.adapters.commands.PhaseCommandJpaAdapter;
import fr.gouv.intradef.pesdt.adapters.queries.PeriodeCycleQueryJpaAdapter;
import fr.gouv.intradef.pesdt.adapters.queries.PhaseQueryJpaAdapter;
import fr.gouv.intradef.pesdt.ports.api.commands.*;
import fr.gouv.intradef.pesdt.ports.api.queries.*;
import fr.gouv.intradef.pesdt.ports.spi.commands.PeriodeCycleCommandPersistencePort;
import fr.gouv.intradef.pesdt.ports.spi.commands.PhaseCommandPersistencePort;
import fr.gouv.intradef.pesdt.ports.spi.queries.*;
import fr.gouv.intradef.pesdt.repository.PeriodeCycleRepository;
import fr.gouv.intradef.pesdt.repository.PhaseRepository;
import fr.gouv.intradef.pesdt.service.queries.*;
import fr.gouv.intradef.pesdt.service.commands.*;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EntityScan("fr.gouv.intradef.pesdt.entity")
public class CyclePOConfiguration
{
	//	--------------------------------- Periode cycle ------------------------------------------
	@Bean
	public PeriodeCycleQueryPersistencePort periodeCycleQueryPersistencePort(
			PeriodeCycleRepository periodeCycleRepository)
	{
		return new PeriodeCycleQueryJpaAdapter(periodeCycleRepository);
	}

	@Bean
	public PeriodeCycleQueryServicePort periodeCycleQueryServicePort(PeriodeCycleRepository periodeCycleRepository)
	{
		return new PeriodeCycleQueryServiceImpl(periodeCycleQueryPersistencePort(periodeCycleRepository));
	}

	@Bean
	public PeriodeCycleCommandPersistencePort periodeCycleCommandPersistencePort(
			PeriodeCycleRepository periodeCycleRepository)
	{
		return new PeriodeCycleCommandJpaAdapter(periodeCycleRepository);
	}

	@Bean
	public PeriodeCycleCommandServicePort periodeCycleCommandServicePort(
			PeriodeCycleRepository periodeCycleRepository)
	{
		return new PeriodeCycleCommandServiceImpl(periodeCycleCommandPersistencePort(
				periodeCycleRepository));
	}

	//	--------------------------------- Phase ------------------------------------------
	@Bean
	public PhaseQueryPersistencePort phaseQueryPersistencePort(
			PhaseRepository phaseRepository)
	{
		return new PhaseQueryJpaAdapter(phaseRepository);
	}

	@Bean
	public PhaseQueryServicePort phaseQueryServicePort(PhaseRepository phaseRepository)
	{
		return new PhaseQueryServiceImpl(phaseQueryPersistencePort(phaseRepository));
	}

	@Bean
	public PhaseCommandPersistencePort phaseCommandPersistencePort(
			PhaseRepository phaseRepository)
	{
		return new PhaseCommandJpaAdapter(phaseRepository);
	}

	@Bean
	public PhaseCommandServicePort phaseCommandServicePort(
			PhaseRepository phaseRepository)
	{
		return new PhaseCommandServiceImpl(phaseCommandPersistencePort(
				phaseRepository));
	}

}
