package fr.gouv.intradef.pesdt.configuration.database;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import fr.gouv.intradef.pesdt.CyclePOApplication;
import fr.gouv.intradef.pesdt.common.infrastructure.configuration.database.DataSourceType;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.orm.jpa.hibernate.SpringImplicitNamingStrategy;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.lang.reflect.Proxy;
import java.util.*;

@Configuration
@RequiredArgsConstructor
public class DataSourceConfig {

	private final Environment env;

	private final Map<String, List<DataSourceType>> dataSourceTypeGroupByDriver = new HashMap<>();

	@Bean
	public DataSource getDataSource() {
		final Map<Object, Object> dataSources = this.buildDataSources();

		final RoutingDataSource routingDataSource = new RoutingDataSource();
		routingDataSource.setTargetDataSources(dataSources);
		routingDataSource.setDefaultTargetDataSource(dataSources.get(DataSourceType.CYCLEPO));

		return routingDataSource;
	}

	/**
	 * Crée un bean for {@link EntityManagerFactory}.
	 * Crée différent entity manager factory pour chaqye type de SQL driver fourni
	 *
	 * @param routingDataSource -
	 * @return - proxy et choisit dynamiquement l’instance appropriée en fonction de la datasource requise.
	 *
	 */
	@Bean("entityManagerFactory")
	public EntityManagerFactory getEntityManagerFactory(DataSource routingDataSource) {
		final Map<DataSourceType, EntityManagerFactory> emfPerDataSource = new HashMap<>();

		this.dataSourceTypeGroupByDriver.forEach((driver, dataSourceGroup) -> {
			DatabaseContextHolder.setCtx(dataSourceGroup.get(0));
			final EntityManagerFactory emf = this.createEntityManagerFactory(
					String.format("EMF_%s", driver),
					routingDataSource
			);
			DatabaseContextHolder.restoreCtx();

			dataSourceGroup.forEach(dataSourceType -> emfPerDataSource.put(dataSourceType, emf));
		});

		final int hashcode = UUID.randomUUID().toString().hashCode();
		final Object emf = Proxy.newProxyInstance(Thread.currentThread().getContextClassLoader(),
				new Class[]{EntityManagerFactory.class},
				(proxy, method, args) -> {
					if (method.getName().equals("hashCode")) return hashcode;
					if (method.getName().equals("equals")) return proxy == args[0];

					Object instance = emfPerDataSource.get(DataSourceType.CYCLEPO);
					final Optional<DataSourceType> dataSourceType = DatabaseContextHolder.peekDataSource();
					if (dataSourceType.isPresent()) {
						instance = emfPerDataSource.get(dataSourceType.get());
					}

					return method.invoke(instance);
				}
		);

		return (EntityManagerFactory) emf;
	}

	private Map<Object, Object> buildDataSources() {
		final Map<Object, Object> result = new HashMap<>();

		for (DataSourceType sourceType : DataSourceType.values()) {
			result.put(sourceType, this.buildDataSource(sourceType));
		}

		return result;
	}

	private DataSource buildDataSource(DataSourceType sourceType) {
		final HikariConfig config = new HikariConfig();

		config.setJdbcUrl(this.env.getProperty(String.format("spring.datasource.%s.url", sourceType.getName())));
		config.setUsername(this.env.getProperty(String.format("spring.datasource.%s.username", sourceType.getName())));
		config.setPassword(this.env.getProperty(String.format("spring.datasource.%s.password", sourceType.getName())));

		String driverClassName = this.env.getProperty(String.format(
				"spring.datasource.%s.driverClassName", sourceType.getName()
		));

		config.setDriverClassName(driverClassName);

		config.setAutoCommit(false);

		this.dataSourceTypeGroupByDriver.putIfAbsent(driverClassName, new ArrayList<>());
		this.dataSourceTypeGroupByDriver.get(driverClassName).add(sourceType);
		return new HikariDataSource(config);
	}

	private EntityManagerFactory createEntityManagerFactory(String providerName,
															DataSource routingDataSource) {
		final LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();

		final Properties properties = new Properties() {{
			put("hibernate.implicit_naming_strategy", new SpringImplicitNamingStrategy());
		}};

		factory.setJpaProperties(properties);
		factory.setPackagesToScan(CyclePOApplication.class.getPackage().getName());

		factory.setDataSource(routingDataSource);

		final HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();

		factory.setJpaVendorAdapter(vendorAdapter);

		factory.afterPropertiesSet();
		factory.setPersistenceUnitName(providerName + "Unit");
		factory.setBeanName(providerName + "Bean");

		return factory.getObject();
	}
}