package fr.gouv.intradef.pesdt;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@SpringBootApplication
public class CyclePOApplication extends SpringBootServletInitializer
{
	public static void main(String[] args)
	{
		SpringApplication.run(CyclePOApplication.class, args);
	}

	@Autowired
	private Environment environment;

	@Bean
	public WebMvcConfigurer corsConfigurer()
	{
		return new WebMvcConfigurer()
		{
			@Override
			public void addCorsMappings(CorsRegistry registry)
			{

				String allowedOrigins = environment.getProperty("spring.cors.allowed-origins");
				registry.addMapping("/**").allowedOrigins(allowedOrigins)
						.allowedMethods("GET", "POST", "PUT", "PATCH", "DELETE", "OPTIONS", "HEAD");

			}
		};
	}

}
