package fr.gouv.intradef.pesdt.configuration.database;

import fr.gouv.intradef.pesdt.common.infrastructure.configuration.database.DataSourceType;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

@Slf4j
public class RoutingDataSource extends AbstractRoutingDataSource {

	/**
	 * Méthode appelé par Spring à chaque transaction
	 * @return le Datasource choisi pour la prochaine transaction
	 */
	@Override
	protected Object determineCurrentLookupKey() {
		final DataSourceType sourceType = DatabaseContextHolder.peekDataSource().orElse(null);

		if (sourceType != null) {
			log.info("Chose {} data source for the next transaction.", sourceType);
		}

		return sourceType;
	}
}