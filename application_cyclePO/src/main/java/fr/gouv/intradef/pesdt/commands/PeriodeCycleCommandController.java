
package fr.gouv.intradef.pesdt.commands;
import fr.gouv.intradef.pesdt.common.api.dto.PesdtBody;
import fr.gouv.intradef.pesdt.common.api.dto.PesdtWrapper;
import fr.gouv.intradef.pesdt.common.domain.utils.AssertionUtils;
import fr.gouv.intradef.pesdt.dto.PeriodeCycleDTO;
import fr.gouv.intradef.pesdt.ports.spi.commands.PeriodeCycleCommandPersistencePort;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.util.List;

/**
 * Controller pour les méthodes d'écriture de l'entité periode de cycle
 */
@RestController
@RequestMapping("/v1/periode-cycle")
@Tag(name = "periode-cycle", description = "Entité periode-cycle")
public class PeriodeCycleCommandController {

	@Autowired
	private PeriodeCycleCommandPersistencePort periodeCycleCommandPersistencePort;

	/**
	 * Crée une entité Periode de cycle
	 *
	 * @param periodeCycle
	 *            entité à créer
	 * @param builder
	 *            uriBuilder
	 * @return entité créée
	 */
	@Operation(summary = "Crée une entité periode de cycle")
	@PostMapping(path = {"createPeriodeCycle"}, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<PesdtBody<PeriodeCycleDTO>> savePeriodeCycle(
			@Valid @RequestBody @Parameter(description = "entité à créer") PeriodeCycleDTO periodeCycle,
			UriComponentsBuilder builder) {
		PeriodeCycleDTO saved = this.periodeCycleCommandPersistencePort.save(periodeCycle);

		String pathContext = builder.build().getPath();
		return PesdtWrapper.created(builder.replacePath("{path}/v1/periode-cycle/{id}")
				.buildAndExpand(pathContext, saved.getId()).toUri(), saved);
	}

	/**
	 * Met à jour une entité periode de cycle
	 *
	 * @param periodeCycle
	 *            entité à mettre à jour
	 * @return periode cycle
	 */
	@Operation(summary = "Met à jour une entité periode cycle")
	@PutMapping(path = {"updatePeriodeCycle"}, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<PesdtBody<PeriodeCycleDTO>> updatePeriodeCycle(
			@Valid @RequestBody @Parameter(description = "entité à mettre à jour") PeriodeCycleDTO periodeCycle) {

		PeriodeCycleDTO updated = this.periodeCycleCommandPersistencePort.update(periodeCycle);
		return PesdtWrapper.ok(updated);
	}

	/**
	 * Supprime une entité periode de cycle
	 *
	 * @param id
	 *            identifiant de l'entité
	 * @return Deleted periode cycle
	 */
	@Operation(summary = "Supprime une entité periode de cycle")
	@DeleteMapping(path = "deletePeriodeCycle/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<PesdtBody<PeriodeCycleDTO>> deletePeriodeCycleById(
			@PathVariable("id") @Parameter(description = "identifiant de l'entité") Long id) {
		PeriodeCycleDTO deleted = this.periodeCycleCommandPersistencePort.deleteById(id);

		return PesdtWrapper.ok(deleted);
	}

	 /**
	 * Met à jour une liste d'entités periode de cycle
	 *
	 * @param periodeCycle
	 *            liste d'entités à mettre à jour
	 *            id
	 * @return periode cycle
	 */
	@Operation(summary = "Met à jour une liste d'entités periode cycle")
	@PutMapping(path = "updateAllPeriodeCycle", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<PesdtBody<List<PeriodeCycleDTO>>> updateAllPeriodeCycle(
			@Valid @RequestBody @Parameter(description = "entité à mettre à jour") List<PeriodeCycleDTO> periodeCycle) {
		List<PeriodeCycleDTO> listUpdated = this.periodeCycleCommandPersistencePort.updateAll(periodeCycle);

		return PesdtWrapper.accepted(listUpdated);
	}

	/**
	 * Supprime une liste d'entités periode de cycle
	 *
	 * @param ids
	 *            liste d'entités à mettre à jour
	 *            id
	 * @return periode cycle
	 */
	@Operation(summary = "Supprime une liste d'entités periode cycle")
	@DeleteMapping(path = "deleteAllPeriodeCycle/ids/{ids}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<PesdtBody<List<PeriodeCycleDTO>>> deleteAllPeriodeCycle(
			@Valid @PathVariable("ids") @Parameter(description = "supprime une liste de periode de cycle") List<Long> ids) {
		List<PeriodeCycleDTO> listDeleted = this.periodeCycleCommandPersistencePort.deleteAllById(ids);

		return PesdtWrapper.accepted(listDeleted);
	}

}
