package fr.gouv.intradef.pesdt.commands;

import fr.gouv.intradef.pesdt.common.api.dto.PesdtBody;
import fr.gouv.intradef.pesdt.common.api.dto.PesdtWrapper;
import fr.gouv.intradef.pesdt.common.domain.utils.AssertionUtils;
import fr.gouv.intradef.pesdt.dto.PhaseDTO;
import fr.gouv.intradef.pesdt.ports.api.commands.PhaseCommandServicePort;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;

/**
 * Controller pour les méthodes d'écriture de l'entité Phase
 */
@RestController
@Tag(name = "phase", description = "Entité phase")
@RequestMapping("/v1/phase")
public class PhaseCommandController
{


	@Autowired
	private PhaseCommandServicePort phaseCommandServicePort;

	/**
	 * Crée une entité phase
	 *
	 * @param phase entité à créer
	 * @param builder         uriBuilder
	 * @return entité créée
	 */

	@Operation(summary = "Crée une phase")
	@PostMapping(path = {"createPhase"}, consumes = MediaType.APPLICATION_JSON_VALUE,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<PesdtBody<PhaseDTO>> save(
			@Valid @RequestBody @Parameter(description = "entité à créer") PhaseDTO phase,
			UriComponentsBuilder builder)
	{
		PhaseDTO saved = this.phaseCommandServicePort.save(phase);
		return PesdtWrapper.ok(saved);
	}


	/**
	 * Met à jour une entité phase
	 *
	 * @param phase entité à mettre à jour
	 * @param id             id
	 * @return phase
	 */
	@Operation(summary = "Met à jour une entité phase")
	@PutMapping(path = {"updatePhase/{id}"}, consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<PesdtBody<PhaseDTO>> updatePhase(
			@Valid @RequestBody @Parameter(description = "entité à mettre à jour") PhaseDTO phase,
			@PathVariable("id") @Parameter(description = "identifiant de l'entité") Long id)
	{
		AssertionUtils.assertEquals(id, phase.getId());

		PhaseDTO updated = this.phaseCommandServicePort.update(phase);

		return PesdtWrapper.ok(updated);
	}

	/**
	 * Supprime une entité phase
	 *
	 * @param id identifiant de l'entité
	 * @return Deleted phase
	 */
	@Operation(summary = "Supprime une entité phase")
	@DeleteMapping(path = {"deletePhaseById/{id}"}, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<PesdtBody<PhaseDTO>> deletePhaseeById(
			@PathVariable("id") @Parameter(description = "identifiant de l'entité") Long id)
	{
		PhaseDTO deleted = this.phaseCommandServicePort.deleteById(id);
		return PesdtWrapper.ok(deleted);
	}
}


