package fr.gouv.intradef.pesdt.queries;

import fr.gouv.intradef.pesdt.common.api.dto.PesdtBody;
import fr.gouv.intradef.pesdt.common.api.dto.PesdtWrapper;
import fr.gouv.intradef.pesdt.dto.PeriodeCycleDTO;
import fr.gouv.intradef.pesdt.dto.PhaseDTO;
import fr.gouv.intradef.pesdt.ports.api.queries.PhaseQueryServicePort;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

/**
 * Controller pour les méthodes de lecture de l'entité phase
 */
@RestController
@RequestMapping("/v1/phase")
@Tag(name = "phase", description = "Entité phase")
public class PhaseQueryController
{
	@Autowired
	private PhaseQueryServicePort phaseQueryServicePort;


	@Operation(summary = "Récupère une liste de phase")
	@GetMapping(path = "/all", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<PesdtBody<List<PhaseDTO>>> findAllPhases()
	{
		return PesdtWrapper.ok(phaseQueryServicePort.findAll());
	}

	/**
	 * Récupère une entité Phase par son id
	 *
	 * @param id
	 *            identifiant de l'entité
	 * @return entité
	 */
	@Operation(summary = "Récupère une entité phase par son id")
	@GetMapping(path = "getPhaseById/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<PesdtBody<PhaseDTO>> findPhaseById(
			@PathVariable("id") @Parameter(description = "identifiant de l'entité") Long id) {
		return PesdtWrapper.ok(this.phaseQueryServicePort.findById(id));
	}

	/**
	 * Récupère une entité Phase par son index couleur
	 *
	 * @param id
	 *            identifiant couleur de l'entité
	 * @return entité
	 */
	@Operation(summary = "Récupère une entité phase par son index couleur")
	@GetMapping(path = "getPhaseByIndexCouleur/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<PesdtBody<List<PhaseDTO>>> findPhaseByIndexCouleur(
			@PathVariable("id") @Parameter(description = "identifiant de l'index couleur") Integer id) {
		return PesdtWrapper.ok(this.phaseQueryServicePort.findByIndexCouleur(id));
	}
}
