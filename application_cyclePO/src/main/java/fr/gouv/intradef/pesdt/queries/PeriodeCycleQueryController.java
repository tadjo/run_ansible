package fr.gouv.intradef.pesdt.queries;


import fr.gouv.intradef.pesdt.common.api.dto.PesdtBody;
import fr.gouv.intradef.pesdt.common.api.dto.PesdtWrapper;
import fr.gouv.intradef.pesdt.common.domain.utils.PageableUtils;
import fr.gouv.intradef.pesdt.dto.PeriodeCycleCheckedDTO;
import fr.gouv.intradef.pesdt.dto.PeriodeCycleDTO;
import fr.gouv.intradef.pesdt.ports.api.queries.PeriodeCycleQueryServicePort;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

/**
 * Controller pour les méthodes de lecture de l'entité Periode de cycle
 */
@RestController
@RequestMapping("/v1/periode-cycle")
@Tag(name = "periode-cycle", description = "Entité periode-cycle")
public class PeriodeCycleQueryController {

	@Autowired
	private PeriodeCycleQueryServicePort periodeCycleQueryServicePort;

	/**
	 * Récupère une liste paginée d'entité periode de cycle
	 *
	 * @param page
	 *            numéro de la page
	 * @param size
	 *            taille de la page
	 * @param sorts
	 *            nom de l'attribut pour le tri
	 * @return page d'entités
	 * @param organismeId
	 * @param phaseId
	 */
	@Operation(summary = "Récupère une liste paginée d'entité periode de cycle par filtre")
	@ApiOperation(value = "", nickname = "findAllPeriodeCycleByFilter", notes = "", response = PeriodeCycleDTO.class,
			responseContainer = "List", tags = {"ActiviteQuery"})
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "default response", response = PeriodeCycleDTO.class, responseContainer = "List")})
	@GetMapping(path = "getAllByFilter", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<PesdtBody<List<PeriodeCycleDTO>>> findAllPeriodeCycleByFilter(
			@RequestParam(required = false) @Parameter(description = "numéro de la page") Integer page,
			@RequestParam(required = false) @Parameter(description = "taille de la page") Integer size,
			@RequestParam(required = false) @Parameter(description = "attributs pour le tri") String sorts,
			@RequestParam(required = false) @Parameter(description = "Identifiants des organismes")
				List<Integer> organismeId,
			@RequestParam(required = false) @Parameter(description = "Identifiants des valeurs d'attributs à requeter")
				List<Integer> phaseId ,
			@RequestParam(required = false) @Parameter(description = "Date de début de periode de cycle")
			List<LocalDateTime> dateDeDebut,
			@RequestParam(required = false) @Parameter(description = "Date de fin de periode de cycle")
			List<LocalDateTime> dateDeFin){

		Page<PeriodeCycleDTO> periodeCyclePage = this.periodeCycleQueryServicePort.findAllPageByFilter(
				PageableUtils.translatePageable(Optional.ofNullable(page).orElse(1),
						Optional.ofNullable(size).orElse(50), sorts),
				organismeId,
				phaseId,
				dateDeDebut,
				dateDeFin);
		return PesdtWrapper.partial(periodeCyclePage);
	}


	/**
	 * Récupère une entité periode de cycle par son id
	 *
	 * @param id
	 *            identifiant de l'entité
	 * @return entité
	 */
	@Operation(summary = "Récupère une entité periode de cycle par son id")
	@GetMapping(path = "getById/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<PesdtBody<PeriodeCycleDTO>> findPeriodeCycleById(
			@PathVariable("id") @Parameter(description = "identifiant de l'entité") Long id) {
		return PesdtWrapper.ok(this.periodeCycleQueryServicePort.findById(id));
	}

	/**
	 * Verifie en base les dates de Periode de cycle à partir de l'oranismeId et d'une liste de dates
	 *
	 * @param periodeCycles
	 *            identifiant de l'entité
	 * @return entité
	 */
	@Operation(summary = "verifie une liste d'entités de periode de cycle par son id d'organisme")
	@PostMapping(path = "checkPeriodeCycleByOrganismeId/organismeId/{organismeId}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<PesdtBody<List<PeriodeCycleDTO>>> checkPeriodeCycleBeforeRegistred(
			 @PathVariable("organismeId") @Parameter(description = "Identifiants des organismes" , required = true)
			Integer organismeId,
	@Valid @RequestBody(required = true) @Parameter(description = "entité(s) periode de cycle") List<PeriodeCycleDTO> periodeCycles ){
		return PesdtWrapper.ok(this.periodeCycleQueryServicePort.checkPeriodeCycleBeforeRegistred(organismeId, periodeCycles));
	}

	/**
	 * Verifie en base les dates de Periode de cycle à partir de l'oranismeId et d'une date de debut & fin
	 *
	 * @return entité
	 */
	@Operation(summary = "verifie une periode de cycle par son id d'organisme")
	@PostMapping(path = "checkDateDebutEtDeFin", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<PeriodeCycleCheckedDTO> checkDateDebutEtDeFin(
			@Valid @RequestBody(required = true) @Parameter(description = "entité periode de cycle") PeriodeCycleDTO periodeCycle ){
		return PesdtWrapper.ok(this.periodeCycleQueryServicePort.checkDateDebutEtDeFin(periodeCycle));
	}

}
