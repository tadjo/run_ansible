package fr.gouv.intradef.pesdt.queries;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.gouv.intradef.pesdt.dto.PhaseDTO;
import fr.gouv.intradef.pesdt.ports.api.queries.PhaseQueryServicePort;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class PhaseQueryControllerTest {

    private MockMvc mockMvc;

    @Mock
    private PhaseQueryServicePort phaseQueryServicePort;

    @InjectMocks
    private PhaseQueryController phaseQueryController;

    private ObjectMapper objectMapper = new ObjectMapper();

    private PhaseDTO phaseDTO;

    private PhaseDTO phaseDTO2;

    private List<PhaseDTO> phaseDTOList;

    @Before
    public void setUp()
    {
        MockitoAnnotations.openMocks(this);
        this.mockMvc = MockMvcBuilders.standaloneSetup(phaseQueryController).build();

        phaseDTO = new PhaseDTO();
        phaseDTO.setId(1L);
        phaseDTO.setAbreviation("OPEX");
        phaseDTO.setDescription("OPEX (Opérations Extérieusr) sont les \"interventions des forces militaires françaises en dehors du territoire national\"");
        phaseDTO.setIndexCouleur(1);
        phaseDTO.setVersion(1l);

        phaseDTO2 = new PhaseDTO();
        phaseDTO2.setId(2L);
        phaseDTO2.setAbreviation("RCO");
        phaseDTO2.setDescription("Remise en Condition Operationnel");
        phaseDTO2.setIndexCouleur(2);
        phaseDTO2.setVersion(1l);

        phaseDTOList = new ArrayList<>();
        phaseDTOList.add(phaseDTO);
        phaseDTOList.add(phaseDTO2);
    }



    @Test
    public void findPhaseById() throws Exception {
        BDDMockito.given(this.phaseQueryServicePort.findById(phaseDTO.getId())).willReturn(phaseDTO);

        MockHttpServletRequestBuilder mockRequest = MockMvcRequestBuilders.get("/v1/phase/getPhaseById/1")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON);

        this.mockMvc.perform(mockRequest)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.id",
                        Matchers.is(phaseDTO.getId().intValue())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.abreviation",
                        Matchers.is(phaseDTO.getAbreviation())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.indexCouleur",
                        Matchers.is(phaseDTO.getIndexCouleur())));
    }

    @Test
    public void findPhaseByIndexCouleur() throws Exception {

        BDDMockito.given(this.phaseQueryServicePort.findByIndexCouleur(phaseDTO.getIndexCouleur())).willReturn(phaseDTOList);

        MockHttpServletRequestBuilder mockRequest = MockMvcRequestBuilders.get("/v1/phase/getPhaseByIndexCouleur/1")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON);

        this.mockMvc.perform(mockRequest)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data[0].id",
                        Matchers.is(phaseDTO.getId().intValue())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data[0].abreviation",
                        Matchers.is(phaseDTO.getAbreviation())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data[0].indexCouleur",
                        Matchers.is(phaseDTO.getIndexCouleur())));

    }

    @Test
    public void findAllPhases() throws Exception {
        BDDMockito.given(this.phaseQueryServicePort.findAll()).willReturn(phaseDTOList);

        MockHttpServletRequestBuilder mockRequest = MockMvcRequestBuilders.get("/v1/phase/all")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON);

        this.mockMvc.perform(mockRequest)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data", Matchers.hasSize(2)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data[0].id",
                        Matchers.is(phaseDTO.getId().intValue())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data[0].abreviation",
                        Matchers.is(phaseDTO.getAbreviation())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data[0].indexCouleur",
                        Matchers.is(phaseDTO.getIndexCouleur())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data[1].id",
                        Matchers.is(phaseDTO2.getId().intValue())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data[1].abreviation",
                        Matchers.is(phaseDTO2.getAbreviation())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data[1].indexCouleur",
                        Matchers.is(phaseDTO2.getIndexCouleur())));
        ;

    }


}