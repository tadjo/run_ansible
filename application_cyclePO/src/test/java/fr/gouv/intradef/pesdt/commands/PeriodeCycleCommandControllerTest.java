package fr.gouv.intradef.pesdt.commands;


import com.fasterxml.jackson.databind.ObjectMapper;

import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import fr.gouv.intradef.pesdt.common.api.dto.PesdtBody;
import fr.gouv.intradef.pesdt.dto.PeriodeCycleDTO;
import fr.gouv.intradef.pesdt.ports.api.commands.PeriodeCycleCommandServicePort;
import fr.gouv.intradef.pesdt.ports.spi.commands.PeriodeCycleCommandPersistencePort;
import fr.gouv.intradef.pesdt.service.commands.PeriodeCycleCommandServiceImpl;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.parameters.P;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.time.LocalDateTime;
import java.time.Month;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PeriodeCycleCommandControllerTest {


    private MockMvc mockMvc;


    @Mock
    private PeriodeCycleCommandServicePort periodeCycleCommandServicePort;

    @Mock
    private  PeriodeCycleCommandPersistencePort periodeCycleCommandPersistencePort;

    @InjectMocks
    private PeriodeCycleCommandController periodeCycleCommandController;


    private ObjectMapper objectMapper = new ObjectMapper();

    private PeriodeCycleDTO periodeCycleDTO;

    private PeriodeCycleDTO periodeCycleDTO2;

    private List<PeriodeCycleDTO> periodeCycleDTOList;

    @Before
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        this.mockMvc = MockMvcBuilders.standaloneSetup(periodeCycleCommandController).build();

    }

    @Before
    public void setValue(){
        periodeCycleDTO = new PeriodeCycleDTO();
        periodeCycleDTO.setId(1L);
        periodeCycleDTO.setDateDeDebut(LocalDateTime.of(2020, Month.FEBRUARY, 19, 0, 0));
        periodeCycleDTO.setDateDeFin(LocalDateTime.of(2020, Month.FEBRUARY, 28, 0, 0));
        periodeCycleDTO.setDescription("test periode de cycle 1");
        periodeCycleDTO.setOrganismeId(57);
        periodeCycleDTO.setVersion(1l);

        periodeCycleDTO2 = new PeriodeCycleDTO();
        periodeCycleDTO2.setId(2L);
        periodeCycleDTO2.setDateDeDebut(LocalDateTime.of(2021, Month.FEBRUARY, 19, 0, 0));
        periodeCycleDTO2.setDateDeFin(LocalDateTime.of(2021, Month.FEBRUARY, 28, 0, 0));
        periodeCycleDTO2.setDescription("Remise en Condition Operationnel");
        periodeCycleDTO2.setOrganismeId(57);
        periodeCycleDTO2.setVersion(1l);

        periodeCycleDTOList = new ArrayList<>();
        periodeCycleDTOList.add(periodeCycleDTO);
        periodeCycleDTOList.add(periodeCycleDTO2);

        objectMapper.registerModule(new JavaTimeModule());
    }

    @Test
    public void savePeriodeCycle() throws Exception {
        PeriodeCycleDTO periodeCycleDTO = new PeriodeCycleDTO();
        periodeCycleDTO.setDescription("test periode de cycle 1");
        periodeCycleDTO.setDateDeDebut(LocalDateTime.of(2020, Month.FEBRUARY, 19, 0, 0));
        periodeCycleDTO.setDateDeFin(LocalDateTime.of(2020, Month.FEBRUARY, 28, 0, 0));
        periodeCycleDTO.setOrganismeId(57);
        periodeCycleDTO.setPhaseId(2);

        PeriodeCycleDTO periodeCycleDTOSaved = new PeriodeCycleDTO();
        periodeCycleDTOSaved.setDescription("test periode de cycle 1");
        periodeCycleDTOSaved.setDateDeDebut(LocalDateTime.of(2020, Month.FEBRUARY, 19, 0, 0));
        periodeCycleDTOSaved.setDateDeFin(LocalDateTime.of(2020, Month.FEBRUARY, 28, 0, 0));
        periodeCycleDTOSaved.setOrganismeId(57);
        periodeCycleDTOSaved.setPhaseId(2);


        BDDMockito.given(this.periodeCycleCommandPersistencePort.save(periodeCycleDTO)).willReturn(periodeCycleDTOSaved);

        this.mockMvc.perform(MockMvcRequestBuilders.post("/v1/periode-cycle/createPeriodeCycle")
                .contentType(MediaType.APPLICATION_JSON)
                .content(this.objectMapper.writeValueAsString(periodeCycleDTOSaved)))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath(("$.data.organismeId"),
                        Matchers.is(periodeCycleDTOSaved.getOrganismeId().intValue())))
                .andExpect(MockMvcResultMatchers.jsonPath(("$.data.phaseId"),
                        Matchers.is(periodeCycleDTOSaved.getPhaseId())));

    }

    @Test
    public void updatePeriodeCycle() throws Exception {
        PeriodeCycleDTO periodeCycleDTO = new PeriodeCycleDTO();
        periodeCycleDTO.setDescription("test periode de cycle 1");
        periodeCycleDTO.setDateDeDebut(LocalDateTime.of(2020, Month.FEBRUARY, 19, 0, 0));
        periodeCycleDTO.setDateDeFin(LocalDateTime.of(2020, Month.FEBRUARY, 28, 0, 0));
        periodeCycleDTO.setOrganismeId(57);
        periodeCycleDTO.setPhaseId(2);

        PeriodeCycleDTO periodeCycleDTOUpdated = new PeriodeCycleDTO();
        periodeCycleDTOUpdated.setDescription("test periode de cycle 1");
        periodeCycleDTOUpdated.setDateDeDebut(LocalDateTime.of(2020, Month.FEBRUARY, 19, 0, 0));
        periodeCycleDTOUpdated.setDateDeFin(LocalDateTime.of(2020, Month.FEBRUARY, 28, 0, 0));
        periodeCycleDTOUpdated.setOrganismeId(57);
        periodeCycleDTOUpdated.setPhaseId(2);


        BDDMockito.given(this.periodeCycleCommandPersistencePort.update(periodeCycleDTO)).willReturn(periodeCycleDTOUpdated);

        this.mockMvc.perform(MockMvcRequestBuilders.put("/v1/periode-cycle/updatePeriodeCycle")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(this.objectMapper.writeValueAsString(periodeCycleDTOUpdated)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath(("$.data.organismeId"),
                        Matchers.is(periodeCycleDTOUpdated.getOrganismeId().intValue())))
                .andExpect(MockMvcResultMatchers.jsonPath(("$.data.phaseId"),
                        Matchers.is(periodeCycleDTOUpdated.getPhaseId())));

    }


    @Test
    public void deletePeriodeCycleById() throws Exception {
        PeriodeCycleDTO periodeCycleDTO = new PeriodeCycleDTO();
        periodeCycleDTO.setId(1l);
        periodeCycleDTO.setDescription("test periode de cycle 1");
        periodeCycleDTO.setDateDeDebut(LocalDateTime.of(2020, Month.FEBRUARY, 19, 0, 0));
        periodeCycleDTO.setDateDeFin(LocalDateTime.of(2020, Month.FEBRUARY, 28, 0, 0));
        periodeCycleDTO.setOrganismeId(57);
        periodeCycleDTO.setPhaseId(2);

        PeriodeCycleDTO periodeCycleDTODeleted = new PeriodeCycleDTO();
        periodeCycleDTODeleted.setId(1l);
        periodeCycleDTODeleted.setDescription("test periode de cycle 1");
        periodeCycleDTODeleted.setDateDeDebut(LocalDateTime.of(2020, Month.FEBRUARY, 19, 0, 0));
        periodeCycleDTODeleted.setDateDeFin(LocalDateTime.of(2020, Month.FEBRUARY, 28, 0, 0));
        periodeCycleDTODeleted.setOrganismeId(57);
        periodeCycleDTODeleted.setPhaseId(2);


        BDDMockito.given(this.periodeCycleCommandPersistencePort.deleteById(periodeCycleDTO.getId())).willReturn(periodeCycleDTODeleted);

        this.mockMvc.perform(MockMvcRequestBuilders.delete("/v1/periode-cycle/deletePeriodeCycle/"+periodeCycleDTO.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(this.objectMapper.writeValueAsString(periodeCycleDTODeleted)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath(("$.data.organismeId"),
                        Matchers.is(periodeCycleDTODeleted.getOrganismeId().intValue())))
                .andExpect(MockMvcResultMatchers.jsonPath(("$.data.phaseId"),
                        Matchers.is(periodeCycleDTODeleted.getPhaseId())));

    }

    @Test
    public void updateAllPeriodeCycle_shouldReturnUpdatedPeriodeCycleList() {
        List<PeriodeCycleDTO> periodeCycleList = new ArrayList<>();
        periodeCycleList.add(new PeriodeCycleDTO());
        periodeCycleList.add(new PeriodeCycleDTO());
        List<PeriodeCycleDTO> updatedPeriodeCycleList = new ArrayList<>();
        updatedPeriodeCycleList.add(new PeriodeCycleDTO());
        updatedPeriodeCycleList.add(new PeriodeCycleDTO());
        when(periodeCycleCommandPersistencePort.updateAll(anyList())).thenReturn(updatedPeriodeCycleList);

        ResponseEntity<PesdtBody<List<PeriodeCycleDTO>>> response = periodeCycleCommandController.updateAllPeriodeCycle(periodeCycleList);

        // Assert
        verify(periodeCycleCommandPersistencePort).updateAll(periodeCycleList);
        Assert.assertNotNull(response);
    }

    @Test
    public void deleteAllPeriodeCycle_shouldReturnDeletedPeriodeCycleList() {
        List<Long> ids = Arrays.asList(1L, 2L, 3L);
        List<PeriodeCycleDTO> deletedPeriodeCycleList = new ArrayList<>();
        deletedPeriodeCycleList.add(new PeriodeCycleDTO());
        deletedPeriodeCycleList.add(new PeriodeCycleDTO());
        when(periodeCycleCommandPersistencePort.deleteAllById(ids)).thenReturn(deletedPeriodeCycleList);

        ResponseEntity<PesdtBody<List<PeriodeCycleDTO>>> response = periodeCycleCommandController.deleteAllPeriodeCycle(ids);

        // Assert
        verify(periodeCycleCommandPersistencePort).deleteAllById(ids);
        Assert.assertNotNull(response);
    }

}