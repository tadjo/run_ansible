package fr.gouv.intradef.pesdt.commands;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import fr.gouv.intradef.pesdt.dto.PeriodeCycleDTO;
import fr.gouv.intradef.pesdt.dto.PhaseDTO;
import fr.gouv.intradef.pesdt.ports.api.commands.PeriodeCycleCommandServicePort;
import fr.gouv.intradef.pesdt.ports.api.commands.PhaseCommandServicePort;
import fr.gouv.intradef.pesdt.ports.spi.commands.PeriodeCycleCommandPersistencePort;
import fr.gouv.intradef.pesdt.ports.spi.commands.PhaseCommandPersistencePort;
import fr.gouv.intradef.pesdt.service.commands.PhaseCommandServiceImpl;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.time.LocalDateTime;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class PhaseCommandControllerTest {

    private MockMvc mockMvc;

    @Mock
    private PhaseCommandServicePort phaseCommandServicePort;

    @InjectMocks
    private PhaseCommandController phaseCommandController;


    private ObjectMapper objectMapper = new ObjectMapper();

    private PhaseDTO phaseDTO;

    @Before
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        this.mockMvc = MockMvcBuilders.standaloneSetup(phaseCommandController).build();

    }


    @Before
    public void setValue(){
        phaseDTO = new PhaseDTO();
        phaseDTO.setId(1L);
        phaseDTO.setDescription("test periode de cycle 1");
        phaseDTO.setIndexCouleur(1);
        phaseDTO.setVersion(1l);


        objectMapper.registerModule(new JavaTimeModule());
    }


    @Test
    public void save() throws Exception {

        PhaseDTO phaseDTOReturned = new PhaseDTO();
        phaseDTOReturned.setId(1L);
        phaseDTOReturned.setDescription("test phase 1");
        phaseDTOReturned.setIndexCouleur(1);
        phaseDTOReturned.setVersion(1l);

        BDDMockito.given(this.phaseCommandServicePort.save(phaseDTO)).willReturn(phaseDTOReturned);

        this.mockMvc.perform(MockMvcRequestBuilders.post("/v1/phase/createPhase")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(this.objectMapper.writeValueAsString(phaseDTOReturned)))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void updatePhase() throws Exception {
        PhaseDTO phaseDTOReturned = new PhaseDTO();
        phaseDTOReturned.setId(1L);
        phaseDTOReturned.setDescription("test phase 1");
        phaseDTOReturned.setIndexCouleur(1);
        phaseDTOReturned.setVersion(1l);

        BDDMockito.given(this.phaseCommandServicePort.update(phaseDTO)).willReturn(phaseDTOReturned);

        this.mockMvc.perform(MockMvcRequestBuilders.put("/v1/phase/updatePhase/"+phaseDTO.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(this.objectMapper.writeValueAsString(phaseDTOReturned)))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void deletePhaseeById() throws Exception {
        PhaseDTO phaseDTOReturned = new PhaseDTO();
        phaseDTOReturned.setId(1L);
        phaseDTOReturned.setDescription("test phase 1");
        phaseDTOReturned.setIndexCouleur(1);
        phaseDTOReturned.setVersion(1l);

        BDDMockito.given(this.phaseCommandServicePort.deleteById(phaseDTO.getId())).willReturn(phaseDTOReturned);

        this.mockMvc.perform(MockMvcRequestBuilders.delete("/v1/phase/deletePhaseById/"+phaseDTO.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(this.objectMapper.writeValueAsString(phaseDTOReturned)))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }
}