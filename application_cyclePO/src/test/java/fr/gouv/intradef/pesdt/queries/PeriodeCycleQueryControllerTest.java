package fr.gouv.intradef.pesdt.queries;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.gouv.intradef.pesdt.common.api.dto.PesdtBody;
import fr.gouv.intradef.pesdt.dto.PeriodeCycleCheckedDTO;
import fr.gouv.intradef.pesdt.dto.PeriodeCycleDTO;
import fr.gouv.intradef.pesdt.ports.api.queries.PeriodeCycleQueryServicePort;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.time.LocalDateTime;
import java.time.Month;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class PeriodeCycleQueryControllerTest {

    private MockMvc mockMvc;

    @Mock
    private PeriodeCycleQueryServicePort periodeCycleQueryServicePort;

    @InjectMocks
    private PeriodeCycleQueryController periodeCycleQueryController;


    private ObjectMapper objectMapper = new ObjectMapper();

    private PeriodeCycleDTO periodeCycleDTO;

    private PeriodeCycleDTO periodeCycleDTO2;

    private List<PeriodeCycleDTO> periodeCycleDTOList;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.openMocks(this);
        this.mockMvc = MockMvcBuilders.standaloneSetup(periodeCycleQueryController).build();

        periodeCycleDTO = new PeriodeCycleDTO();
        periodeCycleDTO.setId(1L);
        periodeCycleDTO.setDateDeDebut(LocalDateTime.of(2020, Month.FEBRUARY, 19, 0, 0));
        periodeCycleDTO.setDateDeFin(LocalDateTime.of(2020, Month.FEBRUARY, 28, 0, 0));
        periodeCycleDTO.setDescription("test periode de cycle 1");
        periodeCycleDTO.setOrganismeId(57);
        periodeCycleDTO.setVersion(1l);

        periodeCycleDTO2 = new PeriodeCycleDTO();
        periodeCycleDTO2.setId(2L);
        periodeCycleDTO2.setDateDeDebut(LocalDateTime.of(2021, Month.FEBRUARY, 19, 0, 0));
        periodeCycleDTO2.setDateDeFin(LocalDateTime.of(2021, Month.FEBRUARY, 28, 0, 0));
        periodeCycleDTO2.setDescription("Remise en Condition Operationnel");
        periodeCycleDTO2.setOrganismeId(57);
        periodeCycleDTO2.setVersion(1l);

        periodeCycleDTOList = new ArrayList<>();
        periodeCycleDTOList.add(periodeCycleDTO);
        periodeCycleDTOList.add(periodeCycleDTO2);
    }


    @Test
    public void findPeriodeCycleById() throws Exception{


       BDDMockito.given(this.periodeCycleQueryServicePort.findById(periodeCycleDTO.getId())).willReturn(periodeCycleDTO);

        MockHttpServletRequestBuilder mockRequest = MockMvcRequestBuilders.get("/v1/periode-cycle/getById/1")

                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON);

        this.mockMvc.perform(mockRequest)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.id",
                        Matchers.is(periodeCycleDTO.getId().intValue())))
                .andExpect(MockMvcResultMatchers.jsonPath(("$.data.organismeId"),
                        Matchers.is(periodeCycleDTO.getOrganismeId().intValue())));
    }

    @Test
    public void findAllPeriodeCycleByFilter() throws Exception {
        // Préparez les données de test
        Pageable pageable = PageRequest.of(0, 1);
        Page<PeriodeCycleDTO> periodeCyclePage = new PageImpl<>(periodeCycleDTOList,pageable,1);

        // Configurez le comportement de votre service mock
        BDDMockito.given(this.periodeCycleQueryServicePort.findAllPageByFilter(
                Mockito.any(),
                Mockito.any(),
                Mockito.any(),
                Mockito.any(),
                Mockito.any()
        )).willReturn(periodeCyclePage);

        // Effectuez une requête GET à l'URL appropriée
        MockHttpServletRequestBuilder mockRequest = MockMvcRequestBuilders.get("/v1/periode-cycle/getAllByFilter")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON);

        // Exécutez le test
        this.mockMvc.perform(mockRequest)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data", Matchers.hasSize(2)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data[0].phaseId",
                        Matchers.is(periodeCycleDTO.getPhase())))
                .andExpect(MockMvcResultMatchers.jsonPath(("$.data[0].organismeId"),
                        Matchers.is(periodeCycleDTO.getOrganismeId().intValue())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data[1].phaseId",
                        Matchers.is(periodeCycleDTO2.getPhase())))
                .andExpect(MockMvcResultMatchers.jsonPath(("$.data[1].organismeId"),
                        Matchers.is(periodeCycleDTO2.getOrganismeId().intValue())));
    }

    @Test
    public void checkPeriodeCycleBeforeRegistred_shouldReturnCheckedPeriodeCycleList() {
        // Arrange
        Integer organismeId = 1;
        List<PeriodeCycleDTO> periodeCycleList = new ArrayList<>();
        periodeCycleList.add(new PeriodeCycleDTO());
        when(periodeCycleQueryServicePort.checkPeriodeCycleBeforeRegistred(organismeId, periodeCycleList)).thenReturn(periodeCycleList);

        ResponseEntity<PesdtBody<List<PeriodeCycleDTO>>> response = periodeCycleQueryController.checkPeriodeCycleBeforeRegistred(organismeId, periodeCycleList);

        // Assert
        verify(periodeCycleQueryServicePort).checkPeriodeCycleBeforeRegistred(organismeId, periodeCycleList);
        Assert.assertNotNull(response);
    }

    @Test
    public void checkDateDebutEtDeFin_shouldReturnTrue() {
        PeriodeCycleCheckedDTO periodeCycleChecked = new PeriodeCycleCheckedDTO();
        periodeCycleChecked.setIsExist(true);
        periodeCycleChecked.setPeriodeCycleList(Arrays.asList(periodeCycleDTO));

        when(periodeCycleQueryServicePort.checkDateDebutEtDeFin(periodeCycleDTO)).thenReturn(periodeCycleChecked);

        ResponseEntity<PeriodeCycleCheckedDTO> dateIsNotAvailable = periodeCycleQueryController.checkDateDebutEtDeFin(periodeCycleDTO);

        // Assert
        verify(periodeCycleQueryServicePort).checkDateDebutEtDeFin(periodeCycleDTO);
        Assert.assertEquals(true, dateIsNotAvailable.getBody().getIsExist());
        Assert.assertEquals(periodeCycleDTO.getId(), dateIsNotAvailable.getBody().getPeriodeCycleList().get(0).getId());
    }

}