:: Custom script for custom backend deployement
ECHO OFF

set DIR_NAME=%1

echo Press Enter to process deploy on %DIR_NAME% component

set FACTORY_DIR=C:\Users\Public\factory
set TARGET_DIR=%FACTORY_DIR%\%DIR_NAME%\launcher\target
set TOMCAT_BASE=C:\Program Files\Apache Software Foundation\Tomcat 9.0
set WEBAPPS_DIR=%TOMCAT_BASE%\webapps
set LOCALHOST_DIR=%TOMCAT_BASE%\conf\Catalina\localhost
set EXTERNAL_DIR=%TOMCAT_BASE%\conf\external


echo example base url :  https://gitlab.scalian.com/geraldine/sandrine
set GIT_BASE_URL=%4
set BASE_BUILD_DIR=%5

cd "%FACTORY_DIR%"
dir

if exist %DIR_NAME%\ (
  echo Yes 
) else (
  echo Clone Git repository
  git -c http.sslVerify=false clone %GIT_BASE_URL%/%DIR_NAME%.git
)

cd "%DIR_NAME%"

set gitbranch=%2
echo Checkout: %gitbranch%

git fetch origin "%gitbranch%":"%gitbranch%"
git checkout -f "%gitbranch%"
git pull origin "%gitbranch%"
git branch

echo Press Enter to build war file of %DIR_NAME% component

call mvn clean package

set CONTEXT_NAME=%3

if exist %TARGET_DIR%\ (
  echo Copy war file to Tomcat webapps
  
  if exist %LOCALHOST_DIR%\%CONTEXT_NAME%.xml (
    echo %CONTEXT_NAME%.xml already exist 
  ) else (
    copy "%BASE_BUILD_DIR%\localhost\%CONTEXT_NAME%.xml" "%LOCALHOST_DIR%\%CONTEXT_NAME%.xml"
  )

  if exist %EXTERNAL_DIR%\%CONTEXT_NAME%.yml (
    echo %DIR_NAME%.yml already exist 
  ) else (
    copy "%BASE_BUILD_DIR%\external\%CONTEXT_NAME%.yml" "%EXTERNAL_DIR%\%CONTEXT_NAME%.yml"
  )
  copy "%TARGET_DIR%\launcher-1.0-SNAPSHOT.war" "%WEBAPPS_DIR%\%CONTEXT_NAME%.war"

)

echo End of build