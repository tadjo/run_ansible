:: Custom script for Masia-hexa deployement
ECHO OFF

set FACTORY_DIR=%1
set PIPELINE_DIR=C:\Users\yves.tadjotakianpi\Documents\DevOps\Scripts\Windows
SET NGINX_DIR=C:\nginx

cd "%FACTORY_DIR%"

dir

if exist masia-frondend-cap\ (
  echo Yes 
) else (
  echo No
  git -c http.sslVerify=false clone https://gitlab.scalian.com/geraldine/sandrine/masia-frondend-cap.git
)

cd masia-frondend-cap

set gitbranch=%2
echo Checkout: %gitbranch%

git fetch origin "%gitbranch%":"%gitbranch%"
git checkout "%gitbranch%"
git pull origin "%gitbranch%"
git branch

set basefrontendurl=%3

echo MAJ de la base_url pour Nginx
powershell -Command "(gc deploiement-int.sh) -replace 'http://10.25.159.109:4200', '%basefrontendurl%' | Out-File -encoding ASCII deploiement-int.sh"

copy "%PIPELINE_DIR%\deploiement-int.sh" "%FACTORY_DIR%\masia-frondend-cap\deploiement-pre-prod.sh"

echo mutualiser le port 8005 pour tous les endspoints
powershell -Command "(gc build-cycle-po.sh) -replace '8002', '8005' | Out-File -encoding ASCII build-cycle-po.sh"
powershell -Command "(gc build-materiel.sh) -replace '8004', '8005' | Out-File -encoding ASCII build-materiel.sh"
powershell -Command "(gc build-projet.sh) -replace '8003', '8005' | Out-File -encoding ASCII build-projet.sh"

echo Téléchargement des yaml
.\build-dev.sh
.\build-cycle-po.sh
.\build-projet.sh
.\build-materiel.sh

call npm run deploiement-pre-prod

set SAVEDATE=%DATE:~-4%-%DATE:~3,2%-%DATE:~8,2%
set SAVETIME=%time:~0,2%-%time:~3,2%-%time:~6,2%

cd "%NGINX_DIR%"

if exist html\ (
  echo Rename html dir
  move "%NGINX_DIR%\html" "html_%SAVEDATE%_%SAVETIME%"
  copy "%FACTORY_DIR%\masia-frondend-cap\dist\si-activites-front" "%NGINX_DIR%\html"
) else (
  echo First install
)

echo Reload Nginx
nginx -s reload
