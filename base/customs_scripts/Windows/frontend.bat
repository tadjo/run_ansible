:: Custom script for Masia-hexa deployement
ECHO OFF

set FACTORY_DIR=C:\Users\Public\factory
set PIPELINE_DIR=C:\Users\yves.tadjotakianpi\Documents\DevOps\Scripts\Windows
SET NGINX_DIR=C:\nginx

cd "%FACTORY_DIR%"

dir

if exist masia-frondend-cap\ (
  echo Yes 
) else (
  echo No
  git -c http.sslVerify=false clone https://gitlab.scalian.com/geraldine/sandrine/masia-frondend-cap.git
)

cd masia-frondend-cap

set /p gitbranch= Enter the name of git branch to deploy :
echo Checkout: %gitbranch%

git fetch origin "%gitbranch%":"%gitbranch%"
git checkout "%gitbranch%"
git pull origin "%gitbranch%"
git branch

copy "%PIPELINE_DIR%\deploiement-int-vm109.sh" "%FACTORY_DIR%\masia-frondend-cap\deploiement-pre-prod.sh"

copy "%PIPELINE_DIR%\build-cycle-po.sh" "%FACTORY_DIR%\masia-frondend-cap\build-cycle-po.sh"
copy "%PIPELINE_DIR%\build-materiel.sh" "%FACTORY_DIR%\masia-frondend-cap\build-materiel.sh"
copy "%PIPELINE_DIR%\build-projet.sh" "%FACTORY_DIR%\masia-frondend-cap\build-projet.sh"

PAUSE

call npm run deploiement-pre-prod

set SAVEDATE=%DATE:~-4%-%DATE:~3,2%-%DATE:~8,2%
set SAVETIME=%time:~0,2%-%time:~3,2%-%time:~6,2%

cd "%NGINX_DIR%"

if exist html\ (
  echo Rename html dir
  move "%NGINX_DIR%\html" "html_%SAVEDATE%_%SAVETIME%"
  copy "%FACTORY_DIR%\masia-frondend-cap\dist\si-activites-front" "%NGINX_DIR%\html"
) else (
  echo First install
)

echo Reload Nginx
nginx -s reload

echo Press Enter exit command prompt
PAUSE