echo 'Télechargement du swagger Materiel'
curl http://localhost:8005/materiel/v3/api-docs.yaml --output api-materiel.yaml

echo 'Remplacement du localhost:8005  par localhost:4200 dans le swagger api-materiel téléchargé'
sed -i 's/localhost:8005/localhost:4200/' api-materiel.yaml