# Mise à jour des packages
npm install

# Téléchargement des yaml
#./build-dev.sh && ./build-cycle-po.sh 
#./build-projet.sh && ./build-materiel.sh

# Générer les api
npm run generatelocalAll

# Générer l'exécutable
npm run build

# Copie des ressources vers dist
cp src/resources/*.html dist/si-activites-front

# Remplacement des urls des apis
sed -i 's#http://localhost:4200#http://10.25.159.109:4200#g' dist/si-activites-front/*.js
sed -i 's#http://localhost:4200#http://10.25.159.109:4200#g' dist/si-activites-front/*.js.map

# Suppression des imports de styles
sed -i 's#@import url(https://fonts.googleapis.com/css?family=Roboto:400,500);##g' dist/si-activites-front/index.html
sed -i 's#@import url(https://fonts.googleapis.com/css?family=Roboto:400,500);##g' dist/si-activites-front/styles*.css
