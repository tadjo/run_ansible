:: Custom script for custom backend deployement
ECHO OFF

set /p DIR_NAME= Enter the name of backend component to deploy :

echo Press Enter to process deploy on %DIR_NAME% component
PAUSE

set FACTORY_DIR=C:\Users\Public\factory
set TARGET_DIR=%FACTORY_DIR%\%DIR_NAME%\launcher\target
set TOMCAT_BASE=C:\Program Files\Apache Software Foundation\Tomcat 9.0
set WEBAPPS_DIR=%TOMCAT_BASE%\webapps
set LOCALHOST_DIR=%TOMCAT_BASE%\conf\Catalina\localhost
set EXTERNAL_DIR=%TOMCAT_BASE%\conf\external
set BASE_BUILD_DIR=C:\Users\yves.tadjotakianpi\Documents\DevOps\Scripts\Windows

cd "%FACTORY_DIR%"
dir

if exist %DIR_NAME%\ (
  echo Yes 
) else (
  echo Clone Git repository
  git -c http.sslVerify=false clone https://gitlab.scalian.com/geraldine/sandrine/%DIR_NAME%.git
)

cd "%DIR_NAME%"

set /p gitbranch= Enter the name of git branch to deploy :
echo Checkout: %gitbranch%

git fetch origin "%gitbranch%":"%gitbranch%"
git checkout "%gitbranch%"
git pull origin "%gitbranch%"
git branch

echo Press Enter to build war file of %DIR_NAME% component
PAUSE

call mvn clean package

set /p CONTEXT_NAME= Enter the conext of backend component to deploy :

if exist %TARGET_DIR%\ (
  echo Copy war file to Tomcat webapps
  PAUSE
  
  if exist %LOCALHOST_DIR%\%CONTEXT_NAME%.xml (
    echo %CONTEXT_NAME%.xml already exist 
  ) else (
    copy "%BASE_BUILD_DIR%\localhost\%CONTEXT_NAME%.xml" "%LOCALHOST_DIR%\%CONTEXT_NAME%.xml"
  )

  if exist %EXTERNAL_DIR%\%CONTEXT_NAME%.yml (
    echo %DIR_NAME%.yml already exist 
  ) else (
    copy "%BASE_BUILD_DIR%\external\%CONTEXT_NAME%.yml" "%EXTERNAL_DIR%\%CONTEXT_NAME%.yml"
  )
  PAUSE
  copy "%TARGET_DIR%\launcher-1.0-SNAPSHOT.war" "%WEBAPPS_DIR%\%CONTEXT_NAME%.war"

  echo Wait until tomcat restart
  call startup.bat
  
  timeout /t 5 /nobreak
  echo Now you can test you app state at : http://localhost:8005/manager/html
)

echo Press Enter exit command prompt
PAUSE