echo 'Télechargement du swagger projet'
curl http://localhost:8005/projet/v3/api-docs.yaml --output projet.yaml

echo 'Remplacement du localhost:8005 par localhost:4200 dans le swagger projet téléchargé'
sed -i 's/localhost:8005/localhost:4200/' projet.yaml