:: Custom script for Masia-hexa deployement
ECHO OFF

echo Updating environment variables
setx SPRING_DATABASE_SERVER "fr-par-sand06"
setx SPRING_DATABASE_NAME "bdd-multi-bloc"
setx SPRING_DATABASE_USER "postgres"
setx SPRING_DATABASE_PASSWORD "test"
setx SPRING_CLIENT_URL "http://localhost:4200"
setx TOMCAT_BASE "C:\Program Files\Apache Software Foundation\Tomcat 9.0"

:: Display Env variables
:: echo %SPRING_DATABASE_SERVER%
:: echo %SPRING_DATABASE_NAME%
:: echo %SPRING_DATABASE_USER%
:: echo %SPRING_DATABASE_PASSWORD%
:: echo %SPRING_CLIENT_URL%

echo Update tomcat configs
set TOMCAT_EXTERNAL_DIR=%TOMCAT_BASE%\conf\external
set TOMCAT_CATALINA_LOCALHOST=%TOMCAT_BASE%\conf\Catalina\localhost\socle_hexagonale.xml
copy "hexa.yml" "%TOMCAT_EXTERNAL_DIR%"
copy "hexa.xml" "%TOMCAT_CATALINA_LOCALHOST%"

PAUSE

echo Deploy hexa war on tomcat

cd C:\Users\Public\factory

dir

if exist masia-hexa\ (
  echo Yes 
) else (
  echo No
  git -c http.sslVerify=false clone https://gitlab.scalian.com/geraldine/sandrine/masia-hexa.git
)

cd masia-hexa

set /p gitbranch= Enter the name of git repository to deploy :
echo Checkout: %gitbranch%

git fetch origin "%gitbranch%":"%gitbranch%"
git checkout "%gitbranch%"
git pull origin "%gitbranch%"
git branch

cd launcher

PAUSE

dir

mvn clean install tomcat7:redeploy -DskipTests

PAUSE