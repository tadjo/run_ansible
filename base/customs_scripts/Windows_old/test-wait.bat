ECHO OFF

set /p delay= Enter delay :

echo  Wait for %delay% seconds

timeout /t %delay% /nobreak

PAUSE