:: Custom script for Masia-hexa deployement
ECHO OFF

dir

cd C:\Users\Public\factory

dir

if exist masia-cycle-po\ (
  echo Yes 
) else (
  echo No
  git -c http.sslVerify=false clone https://gitlab.scalian.com/geraldine/sandrine/masia-cycle-po.git
)

cd masia-cycle-po

set /p gitbranch= Enter the name of git repository to deploy :
echo Checkout: %gitbranch%

git fetch origin "%gitbranch%":"%gitbranch%"
git checkout "%gitbranch%"
git pull origin "%gitbranch%"
git branch

PAUSE