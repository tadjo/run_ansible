:: Custom script for custom backend deployement
ECHO OFF

set /p DIR_NAME= Enter the name of backend component to deploy :

echo Press Enter to process deploy on %DIR_NAME% component
PAUSE

set FACTORY_DIR=C:\Users\Public\factory
set DIR_NAME=masia-materiel
set TARGET_DIR=%FACTORY_DIR%\%DIR_NAME%\launcher\target
set TOMCAT_BASE=C:\Program Files\Apache Software Foundation\Tomcat 9.0
set WEBAPPS_DIR=%TOMCAT_BASE%\webapps

cd "%FACTORY_DIR%"

dir

if exist %DIR_NAME%\ (
  echo Yes 
) else (
  echo Clone Git repository
  git -c http.sslVerify=false clone https://gitlab.scalian.com/geraldine/sandrine/%DIR_NAME%.git
)

cd "%DIR_NAME%"

set /p gitbranch= Enter the name of git branch to deploy :
echo Checkout: %gitbranch%

git fetch origin "%gitbranch%":"%gitbranch%"
git checkout "%gitbranch%"
git pull origin "%gitbranch%"
git branch

echo Press Enter to build war file of %DIR_NAME% component
PAUSE

call mvn clean package

if exist %TARGET_DIR%\ (
  echo Copy war file to Tomcat webapps
  copy "%TARGET_DIR%\launcher-1.0-SNAPSHOT.war" "%WEBAPPS_DIR%\%DIR_NAME%.war"
  echo Wait until tomcat restart
  SLEEP 20
  echo Now you can test you app state at : http://localhost:8005/manager/html
)

echo Press Enter close command promp
PAUSE