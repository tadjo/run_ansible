ECHO OFF

set PIPELINE_DIR=C:\Users\yves.tadjotakianpi\Documents\DevOps\Scripts\Windows
set SAVEDATE=%DATE:~-4%-%DATE:~3,2%-%DATE:~8,2%
set SAVETIME=%time:~0,2%-%time:~3,2%-%time:~6,2%
echo %SAVEDATE%
echo %SAVETIME%

move "%PIPELINE_DIR%\test_mv1" "test_mv1_%SAVEDATE%_%SAVETIME%"

PAUSE