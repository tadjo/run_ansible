:: Custom script for Masia-hexa deployement
ECHO OFF

echo Updating environment variables

set /p databaseserver= Enter the name of database server :
echo Database server: %databaseserver%
setx SPRING_DATABASE_SERVER "%databaseserver%"
set /p databasename= Enter the name of git repository to deploy :
echo Database name: %databasename%
setx SPRING_DATABASE_NAME "%databasename%"
set /p databaseuser= Enter the name of git repository to deploy :
echo Database user: %databaseuser%
setx SPRING_DATABASE_USER "%databaseuser%"
set /p databasepassword= Enter the name of git repository to deploy :
echo Database password: %databasepassword%
setx SPRING_DATABASE_PASSWORD "%databasepassword%"
set /p frontendurl= Enter the name of git repository to deploy :
echo Frontend url: %frontendurl%
setx SPRING_CLIENT_URL "%frontendurl%"

setx TOMCAT_BASE "C:\Program Files\Apache Software Foundation\Tomcat 9.0"

:: Display Env variables
:: echo %SPRING_DATABASE_SERVER%
:: echo %SPRING_DATABASE_NAME%
:: echo %SPRING_DATABASE_USER%
:: echo %SPRING_DATABASE_PASSWORD%
:: echo %SPRING_CLIENT_URL%

PAUSE

echo Update tomcat configs
set TOMCAT_EXTERNAL_DIR=%TOMCAT_BASE%\conf\external
set TOMCAT_CATALINA_LOCALHOST=%TOMCAT_BASE%\conf\Catalina\localhost
copy "hexa.yml" "%TOMCAT_EXTERNAL_DIR%"
copy "hexa.xml" "%TOMCAT_CATALINA_LOCALHOST%"

PAUSE

echo Deploy hexa war on tomcat
cd C:\Users\yves.tadjotakianpi\Documents\Factory\masia-hexa\launcher
mvn clean install tomcat7:redeploy -DskipTests

echo Press enter to close command prompt
PAUSE