package fr.gouv.intradef.pesdt.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import java.util.List;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@Schema(description = "Période de cycle verifié en base de donnée")
public class PeriodeCycleCheckedDTO {

    @JsonProperty("isExist")
    @Schema(description = "isExist")
    private Boolean isExist;

    @JsonProperty("periodeCycleList")
    @Schema(description = "periodeCycleList", nullable = false)
    private List<PeriodeCycleDTO> periodeCycleList;
}
