package fr.gouv.intradef.pesdt.ports.spi.commands;
import fr.gouv.intradef.pesdt.dto.PeriodeCycleDTO;

import java.util.List;

public interface PeriodeCycleCommandPersistencePort {



    List<PeriodeCycleDTO> saveAll(List<PeriodeCycleDTO> entities);

    PeriodeCycleDTO save(PeriodeCycleDTO entity);


    List<PeriodeCycleDTO> updateAll(List<PeriodeCycleDTO> entities);

    PeriodeCycleDTO update(PeriodeCycleDTO entities);

    List<PeriodeCycleDTO> deleteAllById(List<Long> ids);

    PeriodeCycleDTO deleteById(Long id);

}
