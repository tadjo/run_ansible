package fr.gouv.intradef.pesdt.service.commands;

import fr.gouv.intradef.pesdt.dto.PeriodeCycleDTO;
import fr.gouv.intradef.pesdt.ports.api.commands.PeriodeCycleCommandServicePort;
import fr.gouv.intradef.pesdt.ports.spi.commands.PeriodeCycleCommandPersistencePort;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class PeriodeCycleCommandServiceImpl implements PeriodeCycleCommandServicePort {

    private PeriodeCycleCommandPersistencePort periodeCycleCommandPersistencePort;

    public PeriodeCycleCommandServiceImpl(PeriodeCycleCommandPersistencePort periodeCycleCommandPersistencePort)
    {
        this.periodeCycleCommandPersistencePort = periodeCycleCommandPersistencePort;
    }

    @Override
    public PeriodeCycleDTO save(PeriodeCycleDTO periodeCycleDTO) {
       return periodeCycleCommandPersistencePort.save(periodeCycleDTO);
    }

    @Override
    public List<PeriodeCycleDTO> saveAll(List<PeriodeCycleDTO> periodeCycleDTOS) {
        return periodeCycleCommandPersistencePort.saveAll(periodeCycleDTOS);
    }

    @Override
    public PeriodeCycleDTO update(PeriodeCycleDTO periodeCycleDTO) {
        return periodeCycleCommandPersistencePort.update(periodeCycleDTO);
    }

    @Override
    public PeriodeCycleDTO deleteById(Long periodeCycleDTO) {
        return periodeCycleCommandPersistencePort.deleteById(periodeCycleDTO);
    }
    @Override
    public List<PeriodeCycleDTO> deleteAllByIds(List<Long> ids) {
        return periodeCycleCommandPersistencePort.deleteAllById(ids);
    }
}
