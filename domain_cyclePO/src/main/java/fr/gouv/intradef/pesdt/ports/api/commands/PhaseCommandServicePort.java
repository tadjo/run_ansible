package fr.gouv.intradef.pesdt.ports.api.commands;

import fr.gouv.intradef.pesdt.common.domain.port.common.ICrudService;
import fr.gouv.intradef.pesdt.dto.PhaseDTO;

import java.util.List;

public interface PhaseCommandServicePort extends ICrudService<PhaseDTO,Long> {

    List<PhaseDTO> deleteAllById(List<Long> ids);

    List<PhaseDTO> updateAll(List<PhaseDTO> entities);
}
