package fr.gouv.intradef.pesdt.service.commands;
import fr.gouv.intradef.pesdt.dto.PhaseDTO;
import fr.gouv.intradef.pesdt.ports.api.commands.PhaseCommandServicePort;
import fr.gouv.intradef.pesdt.ports.spi.commands.PhaseCommandPersistencePort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PhaseCommandServiceImpl implements PhaseCommandServicePort {

    private PhaseCommandPersistencePort phaseCommandPersistencePort;

    public PhaseCommandServiceImpl(PhaseCommandPersistencePort phaseCommandPersistencePort)
    {
        this.phaseCommandPersistencePort = phaseCommandPersistencePort;
    }

    @Override
    public PhaseDTO save(PhaseDTO phase)
    {
        return this.phaseCommandPersistencePort.save(phase);
    }


    @Override
    public List<PhaseDTO> saveAll(List<PhaseDTO> entities)
    {
        return this.phaseCommandPersistencePort.saveAll(entities);
    }

    @Override
    public PhaseDTO update(PhaseDTO phase)
    {
        return this.phaseCommandPersistencePort.update(phase);
    }

    @Override
    public List<PhaseDTO> deleteAllById(List<Long> ids) {
        return this.phaseCommandPersistencePort.deleteAllById(ids);
    }

    @Override
    public PhaseDTO deleteById(Long id)
    {
        return this.phaseCommandPersistencePort.deleteById(id);
    }

    @Override
    public List<PhaseDTO> updateAll(List<PhaseDTO> entities)
    {
        return this.phaseCommandPersistencePort.updateAll(entities);
    }

}
