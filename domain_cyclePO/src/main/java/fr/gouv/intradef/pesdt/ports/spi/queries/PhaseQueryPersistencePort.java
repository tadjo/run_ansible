package fr.gouv.intradef.pesdt.ports.spi.queries;


import fr.gouv.intradef.pesdt.common.domain.port.common.IQueryService;
import fr.gouv.intradef.pesdt.dto.PhaseDTO;

import java.util.List;

public interface PhaseQueryPersistencePort extends IQueryService<PhaseDTO, Long>
{

    List<PhaseDTO> findByIndexCouleur(Integer indexCouleur);
}
