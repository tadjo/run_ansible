package fr.gouv.intradef.pesdt.ports.api.commands;

import fr.gouv.intradef.pesdt.common.domain.port.common.ICrudService;
import fr.gouv.intradef.pesdt.dto.PeriodeCycleDTO;

import java.util.List;

public interface PeriodeCycleCommandServicePort extends ICrudService<PeriodeCycleDTO,Long> {

    List<PeriodeCycleDTO> deleteAllByIds(List<Long> ids);
}
