package fr.gouv.intradef.pesdt.service.queries;


import fr.gouv.intradef.pesdt.common.domain.utils.AssertionUtils;
import fr.gouv.intradef.pesdt.dto.PeriodeCycleCheckedDTO;
import fr.gouv.intradef.pesdt.dto.PeriodeCycleDTO;
import fr.gouv.intradef.pesdt.ports.api.queries.PeriodeCycleQueryServicePort;
import fr.gouv.intradef.pesdt.ports.spi.queries.PeriodeCycleQueryPersistencePort;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


@Service
public class PeriodeCycleQueryServiceImpl implements PeriodeCycleQueryServicePort
{
	private final PeriodeCycleQueryPersistencePort periodeCycleQueryPersistencePort;

	public PeriodeCycleQueryServiceImpl(
			PeriodeCycleQueryPersistencePort periodeCycleQueryPersistencePort)
	{
		this.periodeCycleQueryPersistencePort = periodeCycleQueryPersistencePort;
	}

	@Override
	public List<PeriodeCycleDTO> findAll()
	{
		return periodeCycleQueryPersistencePort.findAll();
	}

	@Override
	public Page<PeriodeCycleDTO> findAllPageByFilter(Pageable translatePageable, List<Integer> organismeId,
														  List<Integer> phaseId,
													 List<LocalDateTime> dateDeDebut,
													 List<LocalDateTime> dateDeFin)
	{
		return periodeCycleQueryPersistencePort.findAllPageByFilter(translatePageable,
																	organismeId,
																	phaseId,
																	dateDeDebut,
																	dateDeFin);
	}

	@Override
	public Page<PeriodeCycleDTO> findAllPage(Pageable page) {

		return periodeCycleQueryPersistencePort.findAllPage(page);
	}

	@Override
	public PeriodeCycleDTO findById(Long id) {
		AssertionUtils.assertNotNull(id);
		return periodeCycleQueryPersistencePort.findById(id);
	}

	@Override
	public List<PeriodeCycleDTO> checkPeriodeCycleBeforeRegistred(Integer organismeId, List<PeriodeCycleDTO> periodeCycleDTOs) {
		AssertionUtils.assertNotNull(organismeId);
		AssertionUtils.assertNotNull(periodeCycleDTOs);

		Set<PeriodeCycleDTO> periodeCycleSetRegistred = new HashSet<>(periodeCycleQueryPersistencePort.findByOrganismeId(organismeId));

		List<PeriodeCycleDTO> periodeCycleSetToReturn = periodeCycleDTOs.stream()
				.filter(newPeriodeCycleDTO -> periodeCycleSetRegistred.stream()
						.anyMatch(periodeCycleRegistred ->
										newPeriodeCycleDTO.getDateDeDebut().isBefore(periodeCycleRegistred.getDateDeFin()) &&
												newPeriodeCycleDTO.getDateDeFin().isAfter(periodeCycleRegistred.getDateDeDebut())
												|| newPeriodeCycleDTO.getDateDeDebut().isEqual(periodeCycleRegistred.getDateDeDebut())
												|| newPeriodeCycleDTO.getDateDeFin().isEqual(periodeCycleRegistred.getDateDeFin())))
				.collect(Collectors.toList());

		return periodeCycleSetToReturn;
	}

	@Override
	public PeriodeCycleCheckedDTO checkDateDebutEtDeFin(PeriodeCycleDTO periodeCycleSended) {
		AssertionUtils.assertNotNull(periodeCycleSended.getOrganismeId());

		boolean dateAlreadyExist = false;

		Set<PeriodeCycleDTO> periodeCycleSetRegistred = new HashSet<>(periodeCycleQueryPersistencePort.findByOrganismeId(periodeCycleSended.getOrganismeId()));

		List<PeriodeCycleDTO> periodeCycleDTOReturned = periodeCycleSetRegistred
				.stream()
					.filter(periodeCycleDTORegistred -> periodeCycleSended.getDateDeDebut().isBefore(periodeCycleDTORegistred.getDateDeFin())
						&& periodeCycleSended.getDateDeFin().isAfter(periodeCycleDTORegistred.getDateDeDebut())
						|| periodeCycleSended.getDateDeDebut().isEqual(periodeCycleDTORegistred.getDateDeDebut())
					|| periodeCycleSended.getDateDeFin().isEqual(periodeCycleDTORegistred.getDateDeFin()))
				.collect(Collectors.toList());

		if (periodeCycleDTOReturned.isEmpty()){
			//si aucune periode de cycle est chevauchée en BDD on ajoute dans la liste la periode de cycle passée en parametre
			periodeCycleDTOReturned.add(periodeCycleSended);
		} else {
			dateAlreadyExist = true;
		}

		PeriodeCycleCheckedDTO periodeCycleChecked = new PeriodeCycleCheckedDTO();
		periodeCycleChecked.setIsExist(dateAlreadyExist);
		periodeCycleChecked.setPeriodeCycleList(periodeCycleDTOReturned);

		return periodeCycleChecked;
	}
}
