package fr.gouv.intradef.pesdt.ports.api.queries;

import fr.gouv.intradef.pesdt.common.domain.port.common.IQueryService;
import fr.gouv.intradef.pesdt.dto.PhaseDTO;

import java.util.List;

public interface PhaseQueryServicePort extends IQueryService<PhaseDTO,Long>
{
     PhaseDTO findById(Long id);

    List<PhaseDTO> findAll();

    List<PhaseDTO> findByIndexCouleur(Integer indexCouleur);
}
