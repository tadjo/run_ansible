package fr.gouv.intradef.pesdt.service.queries;

import fr.gouv.intradef.pesdt.dto.PhaseDTO;
import fr.gouv.intradef.pesdt.ports.api.queries.PhaseQueryServicePort;
import fr.gouv.intradef.pesdt.ports.spi.queries.PhaseQueryPersistencePort;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PhaseQueryServiceImpl implements PhaseQueryServicePort
{
	private final PhaseQueryPersistencePort phaseQueryPersistencePort;

	public PhaseQueryServiceImpl(PhaseQueryPersistencePort phaseQueryPersistencePort)
	{
		this.phaseQueryPersistencePort = phaseQueryPersistencePort;
	}

	@Override
	public PhaseDTO findById(Long id) {
		return phaseQueryPersistencePort.findById(id);
	}

	@Override
	public List<PhaseDTO> findAll() {
		return phaseQueryPersistencePort.findAll();
	}

	@Override
	public Page<PhaseDTO> findAllPage(Pageable page) {
		return phaseQueryPersistencePort.findAllPage(page);
	}

	@Override
	public List<PhaseDTO> findByIndexCouleur(Integer indexCouleur) {
		return phaseQueryPersistencePort.findByIndexCouleur(indexCouleur);
	}

}
