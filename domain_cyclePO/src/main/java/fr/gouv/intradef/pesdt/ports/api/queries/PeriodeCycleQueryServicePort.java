package fr.gouv.intradef.pesdt.ports.api.queries;

import fr.gouv.intradef.pesdt.common.domain.port.common.IQueryService;
import fr.gouv.intradef.pesdt.dto.PeriodeCycleCheckedDTO;
import fr.gouv.intradef.pesdt.dto.PeriodeCycleDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.time.LocalDateTime;
import java.util.List;

public interface PeriodeCycleQueryServicePort extends IQueryService<PeriodeCycleDTO,Long>
{

    Page<PeriodeCycleDTO> findAllPageByFilter(Pageable translatePageable, List<Integer> organismeId,
                                              List<Integer> phaseId,
                                              List<LocalDateTime> dateDeDebut,
                                              List<LocalDateTime> dateDeFin);

    List<PeriodeCycleDTO> checkPeriodeCycleBeforeRegistred(Integer organismeId, List<PeriodeCycleDTO> periodeCycleDTOs);

    PeriodeCycleCheckedDTO checkDateDebutEtDeFin(PeriodeCycleDTO periodeCycleSended);
}
