package fr.gouv.intradef.pesdt.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import fr.gouv.intradef.pesdt.common.api.dto.AbstractCustomDTO;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

/**
 * Phase
 */

@Getter
@Setter
@ToString
@EqualsAndHashCode
@Schema(description = "type de phase")
public class PhaseDTO extends AbstractCustomDTO
{
    @JsonProperty("abreviation")
    @Schema(description = "abreviation", maxLength = 120, nullable = false)
    private String abreviation;

    @JsonProperty("description")
    @Schema(description = "description", maxLength = 255, nullable = true)
    private String description;

    @JsonProperty("indexCouleur")
    @Schema(description = "index couleur", nullable = false)
    private Integer indexCouleur;

}
