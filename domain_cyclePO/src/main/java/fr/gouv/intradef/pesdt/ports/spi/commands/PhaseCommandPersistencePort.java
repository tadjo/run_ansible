package fr.gouv.intradef.pesdt.ports.spi.commands;

import fr.gouv.intradef.pesdt.common.infrastructure.configuration.database.DataSourceType;
import fr.gouv.intradef.pesdt.common.infrastructure.configuration.database.WithDatabase;
import fr.gouv.intradef.pesdt.dto.PhaseDTO;

import java.util.List;

public interface PhaseCommandPersistencePort {

    PhaseDTO save(PhaseDTO entitie);

    List<PhaseDTO> saveAll(List<PhaseDTO> entities);

    PhaseDTO deleteById(Long id);


    List<PhaseDTO> updateAll(List<PhaseDTO> entities);

    List<PhaseDTO> deleteAllById(List<Long> ids);

    PhaseDTO update(PhaseDTO entitie);


}
