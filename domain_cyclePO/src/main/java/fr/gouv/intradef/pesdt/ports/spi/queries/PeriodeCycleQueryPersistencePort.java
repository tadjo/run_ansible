package fr.gouv.intradef.pesdt.ports.spi.queries;


import fr.gouv.intradef.pesdt.common.domain.port.common.IQueryService;
import fr.gouv.intradef.pesdt.dto.PeriodeCycleDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.time.LocalDateTime;
import java.util.List;

public interface PeriodeCycleQueryPersistencePort extends IQueryService<PeriodeCycleDTO, Long>
{

    List<PeriodeCycleDTO> findByOrganismeId(Integer id);

    Page<PeriodeCycleDTO> findAllPageByFilter(Pageable translatePageable, List<Integer> organismeId,
                                              List<Integer> phaseId,
                                              List<LocalDateTime> dateDeDebut,
                                              List<LocalDateTime> dateDeFin);
}
