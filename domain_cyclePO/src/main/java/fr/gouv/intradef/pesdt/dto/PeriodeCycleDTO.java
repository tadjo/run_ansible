package fr.gouv.intradef.pesdt.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import fr.gouv.intradef.pesdt.common.api.dto.AbstractCustomDTO;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.List;

/**
 * Periode de cycle
 */

@Getter
@Setter
@ToString
@EqualsAndHashCode
@Schema(description = "Période de cycle")
public class PeriodeCycleDTO extends AbstractCustomDTO {
    @JsonProperty("dateDeDebut")
    @Schema(description = "date de debut", nullable = true)
    private LocalDateTime dateDeDebut;

    @JsonProperty("dateDeFin")
    @Schema(description = "date de fin", nullable = true)
    private LocalDateTime dateDeFin;

    @JsonProperty("description")
    @Schema(description = "description", maxLength = 255, nullable = true)
    private String description;

    @JsonProperty("organismeId")
    @Schema(description = "organismeId", nullable = false)
    private Integer organismeId;

    @JsonProperty("organismeAbreviation")
    @Schema(description = "abreviation", maxLength = 120, nullable = false)
    private String organisme_abreviation;

    @JsonProperty("phaseId")
    @Schema(description = "phase id", nullable = false)
    private Integer phaseId;

    @JsonProperty("phase")
    @Schema(description = "phase")
    private PhaseDTO phase;
}
