package fr.gouv.intradef.pesdt.dto.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum MessageErreurCyclePOEnum
{
	MESSAGE_ERREUR_FILTRE_CYCLEPO_ENUM("Pas de période de cycle existant avec les filtres sélectionnés");

	private String messageErreur;
}
