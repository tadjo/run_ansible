export M2_HOME={{ maven.dir }}/apache-maven-{{ maven.version }}
export MAVEN_HOME={{ maven.dir }}/apache-maven-{{ maven.version }}
export PATH=${M2_HOME}/bin:${PATH}